using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using APSA.Web.Services;
using APSA.Web;
using APSA.Web.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace APSA.Test
{
    [TestClass]
    public class GlobalTest
    {
        private static Guid globalBusinessKey = Guid.Empty;

        private static readonly HashSet<Guid> newBusinessKeys = new HashSet<Guid>();
        private static readonly HashSet<Guid> newRegistrationIDs = new HashSet<Guid>();

        public static async Task<Guid> CreateNewBusinessKey()
        {
            var req = new BusinessKeyRequest
            {
                AppType = AppType.Ecommerce.ToString(),
                BusinessName = "Rick & Morty Co.",
                BID = 1,
                AssociationID = 1
            };

            Response<BusinessKeyResponse> resp = await BusinessKeyService.Add(req);

            Guid newBusinessKey = resp.ResponseObject.BusinessKey;
            AddBusinessKey(newBusinessKey);

            return newBusinessKey;
        }

        public static async Task<Guid> GetOrCreateBusinessKey()
        {
            if (globalBusinessKey == Guid.Empty)
                globalBusinessKey = await CreateNewBusinessKey();

            return globalBusinessKey;
        }
        public static void ResetBusinessKey()
        {
            if (globalBusinessKey != Guid.Empty)
            {
                RemoveBusinessKey(globalBusinessKey);
                globalBusinessKey = Guid.Empty;
            }
        }
        public static void AddBusinessKey(Guid businesskey)
        {
            newBusinessKeys.Add(businesskey);
        }
        public static void RemoveBusinessKey(Guid businesskey)
        {
            newBusinessKeys.Remove(businesskey);
        }
        public static async Task<Guid> CreateRegistration()
        {
            Guid businessKey = await GetOrCreateBusinessKey();

            var registrationRequest = new ClientRegistrationRecord
            {
                BusinessKey = businessKey,
                AppType = AppType.Ecommerce.ToString(),
                IsSandboxed = true,
                PackingProcessorType = "BinPacking",
                ShippingProcessorType = "XPS",
                PackingCredentials = new PackingCredentials
                {
                    Login = "apps@corebridge.net",
                    APIKey = "37264533ce4710a8ae8ad8a500cd5606"
                },
                ShippingCredentials = new ShippingCredentials
                {
                    Login = "11700349",
                    APIKey = "Basic v0YAAOgliVQsc1Ja"
                },
                UnitOfLength = "in",
                UnitOfWeight = "lb",
                Containers = new List<ShippingContainer>
                {
                    new ShippingContainer
                    {
                        Name = "18x18x12",
                        NameLabel = "18x18x12",
                        Height = 12,
                        Width = 18,
                        Depth = 18,
                        ContainerWeight = (float)0.1,
                        MaxWeight = 500
                    },
                    new ShippingContainer
                    {
                        Name = "10x10x10",
                        NameLabel = "18x18x12",
                        Height = 10,
                        Width = 10,
                        Depth = 10,
                        ContainerWeight = (float)0.1,
                        MaxWeight = 250
                    },
                },
                Currency = "USD",
                Name = "Thrift Store"
            };

            Response<RegistrationResponse> response = await RegistrationService.Add(registrationRequest);

            Guid registrationID = response.ResponseObject.RegistrationID;
            newRegistrationIDs.Add(registrationID);
            return registrationID;
        }

        public static void AddRegistration(Guid registrationId)
        {
            newRegistrationIDs.Add(registrationId);
        }
        public static TestServer Server { get; private set; }

        [AssemblyInitialize]
        public static void Init(TestContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            // This will run APSA.Web
            Server = new TestServer(new WebHostBuilder()
                                        .ConfigureAppConfiguration(c => c.AddUserSecrets<Startup>())
                                        .UseStartup<Startup>());

            // Just in case LoggingService and DatabaseService needed to be configured.
            //IConfigurationBuilder configurationBuilder = new ConfigurationBuilder().AddUserSecrets<Startup>();
            //var config = configurationBuilder.Build();
            //string connString = config.GetConnectionString("APSA");
            //LoggingService.Init(Serilog.Events.LogEventLevel.Verbose, connString);
            //DatabaseService.Init(connString);
        }

        [AssemblyCleanup]
        public static async Task CleanUp()
        {
            foreach (Guid id in newRegistrationIDs)
            {
                await RegistrationService.Delete(id);
                await RegistrationService.DeleteArchive(id);
            }
            newRegistrationIDs.Clear();

            foreach (Guid key in newBusinessKeys)
            {
                await BusinessKeyService.Delete(key);
            }
            newBusinessKeys.Clear();
            globalBusinessKey = Guid.Empty;
        }
    }
}
