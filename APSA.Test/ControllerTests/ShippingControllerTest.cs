﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using APSA.Test.ControllerTests;
using APSA.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace APSA.Test
{
    [TestClass]
    [TestCategory("Controller/All")]
    [TestCategory("All/Shipping")]
    [TestCategory("Controller/Shipping")]
    public class ShippingControllerTest
    {
        private readonly PackShipRequest _packShipRequest = new PackShipRequest
        {
            ID = "1",
            OrderID = 10061,
            Name = "Watattops",
            Items = new List<ShippingItem>
                {
                    new ShippingItem
                    {
                        ID = "1",
                        Name = "Venum Predator Gloves Yellow Green Black 16oz",
                        Quantity = 2,
                        Height = 16,
                        Width = 7,
                        Depth = 5,
                        Weight = (float) 0.05,
                        DeclaredValue = (float) 100.00,
                        InsuredValue = (float) 100.00
                    },
                    new ShippingItem
                    {
                        ID = "1",
                        Name = "Venum Predator Mouthguard Yellow Green Black",
                        Quantity = 1,
                        Height = 3,
                        Width = 4,
                        Depth = 1,
                        Weight = 3,
                        DeclaredValue = (float) 90.00,
                        InsuredValue = (float) 90.00
                    },
                    new ShippingItem
                    {
                        ID = "1",
                        Name = "Venum Predator Handwraps Yellow Green",
                        Quantity = 2,
                        Height = 5,
                        Width = 5,
                        Depth = 5,
                        Weight = 1,
                        DeclaredValue = (float) 40.00,
                        InsuredValue = (float) 40.00
                    }
                },
            ShipmentInfo = new ShippingTransitInfo
            {
                ShipmentDate = DateTime.Now.AddMonths(3),
                ShipmentReference = "123456",
                Description = "Shipment desciption",
                SignatureOption = "Signature",
                CommercialInvoice = new object(),
                BillTo = new BillTo
                {
                    name = "Watattops",
                    account_number = "ACC-001",
                    country = "US",
                    zip = "99801"
                },
                ShipFrom = new Person
                {
                    Name = "Rick Sanchez",
                    Company = "Rick and Morty Co.",
                    Address = new Address
                    {
                        Street = "Earth",
                        Street2 = "C137",
                        Region = "Pacific",
                        PostalCode = "99801",
                        City = "Juneau",
                        Country = "US",      
                    },
                    Email = "rick@earthc137.com",
                    Phone = "1234567891",
                    State = "Alaska"
                },
                ShipTo = new Person
                {
                    Name = "Morty Smith",
                    Company = "Rick and Morty Co.",
                    Address = new Address
                    {
                        Street = "Earth",
                        Street2 = "C137",
                        Region = "Pacific",
                        PostalCode = "99801",
                        City = "Juneau",
                        Country = "US"
                    },
                    Email = "morty@earthc137.com",
                    Phone = "1234567891",
                    
                    State = "Alaska"
                }
            },
            Service = new CarrierService
            {
                Carrier = "usps",
                Service = "usps_priority",
                CarrierPackageCode = "usps_custom_package"
            },
            Services = new List<CarrierService>
                {
                    new CarrierService
                    {
                        Carrier = "usps",
                        Service = "usps_priority",
                        CarrierPackageCode = "usps_custom_package"
                    },
                    new CarrierService
                    {
                        Carrier = "usps",
                        Service = "usps_express",
                        CarrierPackageCode = "usps_custom_package"
                    }
                }
        };

        [TestMethod]
        public async Task QuoteShipment()
        {
            Guid registrationID = await GlobalTest.CreateRegistration();

            HttpResponseMessage response = await ControllerTestHelper.POST("/apsa/ship/quote", _packShipRequest, registrationID);
            Assert.IsTrue(response.IsSuccessStatusCode, "Quote Shipment did not return success.");

            Response<ShippingQuote> shippingQuoteResponse = JsonConvert.DeserializeObject<Response<ShippingQuote>>(await response.Content.ReadAsStringAsync());
            Assert.IsNotNull(shippingQuoteResponse, "No ResponseObject returned");
            await GlobalTest.CleanUp();
        }

        /* Will always fail test because of the XPS account from the shipping credentials can't book shipment. */
        //[TestMethod]
        //public async Task BookShipment()
        //{
        //    Guid registrationID = await GlobalTest.CreateRegistration();

        //    HttpResponseMessage response = await ControllerTestHelper.POST("/apsa/ship", _packShipRequest, registrationID);
        //    Assert.IsTrue(response.IsSuccessStatusCode, "Book Shipment did not return success.");

        //    Response<ShippingPlan> shippingPlanResponse = JsonConvert.DeserializeObject<Response<ShippingPlan>>(await response.Content.ReadAsStringAsync());
        //    Assert.IsNotNull(shippingPlanResponse, "No ResponseObject returned");
        //}

        /* Test Method for Void Shipment here */

        [TestMethod]
        public async Task GetCarriers()
        {
            Guid registrationID = await GlobalTest.CreateRegistration();

            HttpResponseMessage response = await ControllerTestHelper.GET("/apsa/ship/carrier", registrationID);
            Assert.IsTrue(response.IsSuccessStatusCode, "Get Carriers did not return success.");
            await GlobalTest.CleanUp();
        }

        [TestMethod]
        public async Task GetQuotingOptions()
        {
            Guid registrationID = await GlobalTest.CreateRegistration();

            HttpResponseMessage response = await ControllerTestHelper.GET("/apsa/ship/service", registrationID);
            Assert.IsTrue(response.IsSuccessStatusCode, "Get Quoting Options did not return success.");
            await GlobalTest.CleanUp();
        }
    }
}
