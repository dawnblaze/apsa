﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using APSA.Test.ControllerTests;
using APSA.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace APSA.Test
{
    [TestClass]
    [TestCategory("Controller/All")]
    [TestCategory("All/Packing")]
    [TestCategory("Controller/Packing")]
    public class PackingControllerTest
    {
        //[TestMethod]
        //public async Task DeleteExpiredRequestedImages()
        //{
        //    Guid registrationID = await GlobalTest.CreateRegistration();
        //    Registration registration = await RegistrationService.FindRegistration(registrationID);
        //    Assert.IsNotNull(registration, "New registration failed.");

        //    HttpResponseMessage response = await ControllerTestHelper.DELETE("/apsa/pack/deleteoldrequestedimages");
        //    Assert.IsTrue(response.IsSuccessStatusCode, "Failed to delete expired  requested images.");
        //}

        [TestMethod]
        public async Task ConfirmPacking()
        {
            PackShipRequest packShipRequest = new PackShipRequest
            {
                ID = "1",
                OrderID = 10061,
                Name = "Watattops",
                Items = new List<ShippingItem>
                {
                    new ShippingItem
                    {
                        ID = "1",
                        Name = "Venum Predator Gloves Yellow Green Black 16oz",
                        Quantity = 2,
                        Height = 16,
                        Width = 7,
                        Depth = 5,
                        Weight = (float) 0.05,
                        DeclaredValue = (float) 100.00,
                        InsuredValue = (float) 100.00
                    },
                    new ShippingItem
                    {
                        ID = "1",
                        Name = "Venum Predator Mouthguard Yellow Green Black",
                        Quantity = 1,
                        Height = 3,
                        Width = 4,
                        Depth = 1,
                        Weight = 3,
                        DeclaredValue = (float) 90.00,
                        InsuredValue = (float) 90.00
                    },
                    new ShippingItem
                    {
                        ID = "1",
                        Name = "Venum Predator Handwraps Yellow Green",
                        Quantity = 2,
                        Height = 5,
                        Width = 5,
                        Depth = 5,
                        Weight = 1,
                        DeclaredValue = (float) 40.00,
                        InsuredValue = (float) 40.00
                    }
                },
                ShipmentInfo = new ShippingTransitInfo
                {
                    ShipmentDate = DateTime.Now.AddDays(3),
                    ShipmentReference = "123456",
                    Description = "Shipment desciption",
                    SignatureOption = "NO_SIGNATURE_REQUIRED",
                    CommercialInvoice = new object(),
                    BillTo = new BillTo
                    {
                        name = "Watattops",
                        account_number = "ACC-001",
                        country = "US",
                        zip = "99801"
                    },
                    ShipFrom = new Person
                    {
                        Name = "Rick Sanchez",
                        Company = "Rick and Morty Co.",
                        Address = new Address
                        {
                            Street = "Earth",
                            Street2 = "C137",
                            Region = "Pacific",
                            PostalCode = "99801",
                            Country = "US"
                        },
                        Email = "rick@earthc137.com",
                        Phone = "1234567891"
                    },
                    ShipTo = new Person
                    {
                        Name = "Morty Smith",
                        Company = "Rick and Morty Co.",
                        Address = new Address
                        {
                            Street = "Earth",
                            Street2 = "C137",
                            Region = "Pacific",
                            PostalCode = "99801",
                            Country = "US"
                        },
                        Email = "morty@earthc137.com",
                        Phone = "1234567891"
                    }
                },
                Service = new CarrierService
                {
                    Carrier = "usps",
                    Service = "usps_priority",
                    CarrierPackageCode = "usps_custom_package"
                },
                Services = new List<CarrierService>
                {
                    new CarrierService
                    {
                        Carrier = "usps",
                        Service = "usps_priority",
                        CarrierPackageCode = "usps_custom_package"
                    },
                    new CarrierService
                    {
                        Carrier = "usps",
                        Service = "usps_express",
                        CarrierPackageCode = "usps_custom_package"
                    }
                }
            };

            Guid registrationID = await GlobalTest.CreateRegistration();

            HttpResponseMessage response = await ControllerTestHelper.POST("/apsa/pack", packShipRequest, registrationID);
            Assert.IsTrue(response.IsSuccessStatusCode, "Packing confirmation did not return success.");
            await GlobalTest.CleanUp();
        }
    }
}
