﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace APSA.Test.ControllerTests
{
    public class ControllerTestHelper
    {
        public static async Task<HttpResponseMessage> GET(string endpoint, Guid? registrationId = null)
        {
            using (HttpClient client = GlobalTest.Server.CreateClient())
            {
                if (registrationId.HasValue)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Registration", registrationId.Value.ToString());

                return await client.GetAsync(endpoint);
            }
        }
        public static async Task<HttpResponseMessage> POST(string endpoint, object body, Guid? registrationId = null)
        {
            using (HttpClient client = GlobalTest.Server.CreateClient())
            {
                if (registrationId.HasValue)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Registration", registrationId.Value.ToString());

                return await client.PostAsync(endpoint, new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json"));
            }
        }
        public static async Task<HttpResponseMessage> PUT(string endpoint, object body, Guid? registrationId = null)
        {
            using (HttpClient client = GlobalTest.Server.CreateClient())
            {
                if (registrationId.HasValue)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Registration", registrationId.Value.ToString());

                return await client.PutAsync(endpoint, new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json"));
            }
        }
        public static async Task<HttpResponseMessage> DELETE(string endpoint, Guid? registrationId = null)
        {
            using (HttpClient client = GlobalTest.Server.CreateClient())
            {
                if (registrationId.HasValue)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Registration", registrationId.Value.ToString());

                return await client.DeleteAsync(endpoint);
            }
        }
    }
}
