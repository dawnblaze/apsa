﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using APSA.Test.ControllerTests;
using APSA.Web.Models;
using APSA.Web.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace APSA.Test
{
    [TestClass]
    [TestCategory("All/BusinessKey")]
    [TestCategory("Controller/BusinessKey")]
    public class BusinessKeyControllerTest
    {
        [TestMethod]
        public async Task AddNewBusinessKey()
        {
            BusinessKeyRequest businessKeyRequest = new BusinessKeyRequest
            {
                AppType = AppType.Ecommerce.ToString(),
                BID = 1,
                BusinessName = "Redneos",
                AssociationID = 1
            };

            HttpResponseMessage response = await ControllerTestHelper.POST("/apsa/businesskey", businessKeyRequest);
            Assert.IsTrue(response.IsSuccessStatusCode, "New Business Request did not return success.");

            BusinessKeyResponse businessKeyResponse = JsonConvert.DeserializeObject<BusinessKeyResponse>(await response.Content.ReadAsStringAsync());
            Assert.IsNotNull(businessKeyResponse, "No ResponseObject returned");
            Assert.AreNotEqual(Guid.Empty, businessKeyResponse.BusinessKey, "New Business Key is an Empty Guid");

            GlobalTest.AddBusinessKey(businessKeyResponse.BusinessKey);

            short? bid = await BusinessKeyService.GetBusinessID(businessKeyResponse.BusinessKey);
            Assert.IsNotNull(bid, "New Business Key not found.");
        }

        [TestMethod]
        public async Task AddNewBusinessKeyWithoutAppType()
        {
            BusinessKeyRequest businessKeyRequest = new BusinessKeyRequest
            {
                BusinessName = "Rick & Morty Co."
            };

            HttpResponseMessage resp = await ControllerTestHelper.POST("/apsa/businesskey", businessKeyRequest);
            Assert.AreEqual(HttpStatusCode.BadRequest, resp.StatusCode, "New Business Request did not fail properly.");
        }

        [TestMethod]
        public async Task AddNewBusinessKeyWithInvalidAppType()
        {
            BusinessKeyRequest businessKeyRequest = new BusinessKeyRequest
            {
                AppType = "Riggity",
                BusinessName = "Rick & Morty Co."
            };

            HttpResponseMessage resp = await ControllerTestHelper.POST("/apsa/businesskey", businessKeyRequest);
            Assert.AreEqual(HttpStatusCode.BadRequest, resp.StatusCode, "New Business Request did not fail properly.");
        }

        [TestMethod]
        public async Task AddNewBusinessKeyWithoutCompanyName()
        {
            BusinessKeyRequest businessKeyRequest = new BusinessKeyRequest
            {
                AppType = AppType.Ecommerce.ToString()
            };

            HttpResponseMessage resp = await ControllerTestHelper.POST("/apsa/businesskey", businessKeyRequest);
            Assert.AreEqual(HttpStatusCode.BadRequest, resp.StatusCode, "New Business Request did not fail properly.");
        }

        [TestMethod]
        public async Task DeleteBusinessKey()
        {
            Guid businessKey = await GlobalTest.CreateNewBusinessKey();

            HttpResponseMessage response = await ControllerTestHelper.DELETE($"/apsa/businesskey/{businessKey}");
            Assert.IsTrue(response.IsSuccessStatusCode, "Business Request did not delete successfully.");

            GlobalTest.RemoveBusinessKey(businessKey);
            short? bid = await BusinessKeyService.GetBusinessID(businessKey);
            Assert.IsNull(bid, "Business Key not actually deleted");
        }
    }
}
