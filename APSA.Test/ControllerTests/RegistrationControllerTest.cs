﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using APSA.Test.ControllerTests;
using APSA.Web.Models;
using APSA.Web.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace APSA.Test
{
    [TestClass]
    [TestCategory("Controller/All")]
    [TestCategory("All/Registration")]
    [TestCategory("Controller/Registration")]
    public class RegistrationControllerTest
    {
        [TestMethod]
        public async Task AddNewRegistration()
        {
            Guid businessKey = await GlobalTest.GetOrCreateBusinessKey();

            var registrationRequest = new ClientRegistrationRecord
            {
                BusinessKey = businessKey,
                AppType = AppType.Ecommerce.ToString(),
                IsSandboxed = true,
                PackingProcessorType = "BinPacking",
                ShippingProcessorType = "XPS",
                PackingCredentials = new PackingCredentials
                {
                    Login = "apps@corebridge.net",
                    APIKey = "37264533ce4710a8ae8ad8a500cd5606"
                },
                ShippingCredentials = new ShippingCredentials
                {
                    Login = "11700349",
                    APIKey = "Basic v0YAAOgliVQsc1Ja"
                },
                UnitOfLength = "m",
                UnitOfWeight = "kg",
                Containers = new List<ShippingContainer>
                {
                    new ShippingContainer
                    {
                        Name = "18x18x12",
                        NameLabel = "18x18x12",
                        Height = 12,
                        Width = 18,
                        Depth = 18,
                        ContainerWeight = (float)0.1,
                        MaxWeight = 500
                    },
                    new ShippingContainer
                    {
                        Name = "10x10x10",
                        NameLabel = "18x18x12",
                        Height = 10,
                        Width = 10,
                        Depth = 10,
                        ContainerWeight = (float)0.1,
                        MaxWeight = 250
                    },
                },
                Currency = "USD",
                Name = "Thrift Store"
            };

            HttpResponseMessage response = await ControllerTestHelper.POST("/apsa/registration", registrationRequest);
            Assert.IsTrue(response.IsSuccessStatusCode, "New Registration Request did not return success.");

            RegistrationResponse registrationResponse = JsonConvert.DeserializeObject<RegistrationResponse>(await response.Content.ReadAsStringAsync());
            Assert.IsNotNull(registrationResponse, "No ResponseObject returned");
            Assert.AreNotEqual(Guid.Empty, registrationResponse.RegistrationID, "New Registration ID is an Empty Guid");

            GlobalTest.AddRegistration(registrationResponse.RegistrationID);

            Registration registration = await RegistrationService.FindRegistration(registrationResponse.RegistrationID);

            Assert.IsNotNull(registration, "New Registration not found");
        }

        [TestMethod]
        public async Task DeleteRegistration()
        {
            Guid registrationID = await GlobalTest.CreateRegistration();

            HttpResponseMessage resp = await ControllerTestHelper.DELETE($"/apsa/registration/{registrationID}");
            Assert.IsTrue(resp.IsSuccessStatusCode, "New Registration Request did not delete successfully.");

            Registration reg = await RegistrationService.FindRegistration(registrationID);
            Assert.IsNull(reg, "Registration not actually deleted");
        }

        [TestMethod]
        public async Task CleanRegistration()
        {
            Guid businessKey = await GlobalTest.GetOrCreateBusinessKey();
            Guid registrationID = await GlobalTest.CreateRegistration();

            var registrationRequest = new ClientRegistrationRecord
            {
                BusinessKey = businessKey,
                AppType = AppType.Ecommerce.ToString(),
                IsSandboxed = true,
                PackingProcessorType = "BinPacking",
                ShippingProcessorType = "XPS",
                PackingCredentials = new PackingCredentials
                {
                    Login = "apps@corebridge.net",
                    APIKey = "37264533ce4710a8ae8ad8a500cd5606"
                },
                ShippingCredentials = new ShippingCredentials
                {
                    Login = "11700349",
                    APIKey = "Basic v0YAAOgliVQsc1Ja"
                },
                UnitOfLength = "m",
                UnitOfWeight = "kg",
                Containers = new List<ShippingContainer>
                {
                    new ShippingContainer
                    {
                        Name = "18x18x12",
                        NameLabel = "18x18x12",
                        Height = 12,
                        Width = 18,
                        Depth = 18,
                        ContainerWeight = (float)0.1,
                        MaxWeight = 500
                    },
                    new ShippingContainer
                    {
                        Name = "10x10x10",
                        NameLabel = "18x18x12",
                        Height = 10,
                        Width = 10,
                        Depth = 10,
                        ContainerWeight = (float)0.1,
                        MaxWeight = 250
                    },
                },
                Currency = "USD",
                ExpirationDT = new DateTime(2018, 01, 01),
                Name = "Thrift Store"
            };

            Response<RegistrationResponse> updatedRegistrationResponse = await RegistrationService.Update(registrationID, registrationRequest);
            Assert.AreEqual(ResponseState.Success, updatedRegistrationResponse.State, "Update date of registration to expire failed.");

            HttpResponseMessage response = await ControllerTestHelper.POST("/apsa/registration/clean", null);
            Assert.IsTrue(response.IsSuccessStatusCode, "Clean was not successful.");

            Registration registration = await RegistrationService.FindRegistration(registrationID);
            Assert.IsNull(registration, "Expired registration still exists.");
        }

        [TestMethod]
        public async Task LookupRegistration()
        {
            Guid registrationID = await GlobalTest.CreateRegistration();

            HttpResponseMessage resp = await ControllerTestHelper.GET($"/apsa/registration/{registrationID}");

            Assert.IsTrue(resp.IsSuccessStatusCode, "Registration Request did not return success.");
        }

        [TestMethod]
        public async Task LookupAndRenewRegistration()
        {
            Guid businessKey = await GlobalTest.GetOrCreateBusinessKey();
            Guid registrationID = await GlobalTest.CreateRegistration();

            HttpResponseMessage response = await ControllerTestHelper.GET($"/apsa/registration/{registrationID}");
            Assert.IsTrue(response.IsSuccessStatusCode, "Registration Request did not return success.");

            var registrationRequest = new ClientRegistrationRecord
            {
                BusinessKey = businessKey,
                AppType = AppType.Ecommerce.ToString(),
                IsSandboxed = true,
                PackingProcessorType = "BinPacking",
                ShippingProcessorType = "XPS",
                PackingCredentials = new PackingCredentials
                {
                    Login = "apps@corebridge.net",
                    APIKey = "37264533ce4710a8ae8ad8a500cd5606"
                },
                ShippingCredentials = new ShippingCredentials
                {
                    Login = "11700349",
                    APIKey = "Basic v0YAAOgliVQsc1Ja"
                },
                UnitOfLength = "m",
                UnitOfWeight = "kg",
                Containers = new List<ShippingContainer>
                {
                    new ShippingContainer
                    {
                        Name = "18x18x12",
                        NameLabel = "18x18x12",
                        Height = 12,
                        Width = 18,
                        Depth = 18,
                        ContainerWeight = (float)0.1,
                        MaxWeight = 500
                    },
                    new ShippingContainer
                    {
                        Name = "10x10x10",
                        NameLabel = "18x18x12",
                        Height = 10,
                        Width = 10,
                        Depth = 10,
                        ContainerWeight = (float)0.1,
                        MaxWeight = 250
                    },
                },
                Currency = "USD",
                ExpirationDT = new DateTime(1993, 01, 01),
                Name = "Thrift Store"
            };

            Response<RegistrationResponse> updatedRegistrationResponse = await RegistrationService.Update(registrationID, registrationRequest);
            Assert.AreEqual(ResponseState.Success, updatedRegistrationResponse.State, "Update date of registration to expire failed.");

            string url = $"/apsa/registration/{registrationID}?renew=true";

            response = await ControllerTestHelper.GET(url);
            Assert.IsTrue(response.IsSuccessStatusCode, "Registration Request did not return success.");

            Registration registration = await RegistrationService.FindRegistration(registrationID);
            Assert.IsTrue(registration.ExpirationDT > DateTime.UtcNow.AddHours(-2), "Expiration date not updated after lookup and renew.");
        }

        [TestMethod]
        public async Task LookupExpiredRegistration()
        {
            Guid businessKey = await GlobalTest.GetOrCreateBusinessKey();
            Guid registrationID = await GlobalTest.CreateRegistration();

            var registrationRequest = new ClientRegistrationRecord
            {
                BusinessKey = businessKey,
                AppType = AppType.Ecommerce.ToString(),
                IsSandboxed = true,
                PackingProcessorType = "BinPacking",
                ShippingProcessorType = "XPS",
                PackingCredentials = new PackingCredentials
                {
                    Login = "apps@corebridge.net",
                    APIKey = "37264533ce4710a8ae8ad8a500cd5606"
                },
                ShippingCredentials = new ShippingCredentials
                {
                    Login = "11700349",
                    APIKey = "Basic v0YAAOgliVQsc1Ja"
                },
                UnitOfLength = "m",
                UnitOfWeight = "kg",
                Containers = new List<ShippingContainer>
                {
                    new ShippingContainer
                    {
                        Name = "18x18x12",
                        NameLabel = "18x18x12",
                        Height = 12,
                        Width = 18,
                        Depth = 18,
                        ContainerWeight = (float)0.1,
                        MaxWeight = 500
                    },
                    new ShippingContainer
                    {
                        Name = "10x10x10",
                        NameLabel = "18x18x12",
                        Height = 10,
                        Width = 10,
                        Depth = 10,
                        ContainerWeight = (float)0.1,
                        MaxWeight = 250
                    }
                },
                Currency = "USD",
                ExpirationDT = new DateTime(1993, 01, 01),
                Name = "Thrift Store"
            };

            Response<RegistrationResponse> updatedRegistrationResponse = await RegistrationService.Update(registrationID, registrationRequest);
            Assert.AreEqual(ResponseState.Success, updatedRegistrationResponse.State, "Update date of registration to expire failed.");

            HttpResponseMessage response = await ControllerTestHelper.GET($"/apsa/registration/{registrationID}");
            Assert.IsTrue(response.IsSuccessStatusCode, "Registration Request failed.");

            Dictionary<string, string> registrationResponse = JsonConvert.DeserializeObject<Dictionary<string, string>>(await response.Content.ReadAsStringAsync());
            Assert.AreEqual("false", registrationResponse["response"], "Registration Request did not fail.");
        }

    }
}
