﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using APSA.Test.ControllerTests;
using APSA.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace APSA.Test
{
    [TestClass]
    [TestCategory("Controller/All")]
    [TestCategory("All/PackAndShip")]
    [TestCategory("Controller/PackAndShip")]
    public class PackAndShipControllerTest
    {
        [TestMethod]
        public async Task PackAndShipWithValidData()
        {
            PackShipRequest packShipRequest = new PackShipRequest
            {
                Service = new CarrierService
                {
                    Carrier = "ups",
                    Service = "ups_ground",
                    CarrierPackageCode = "ups_custom_package"
                },
                ID = "1",
                OrderID = 10061,
                Name = "Watattops",
                Items = new List<ShippingItem>
                {
                    new ShippingItem
                    {
                        ID = "1",
                        Name = "Venum Predator Gloves Yellow Green Black 16oz",
                        Quantity = 2,
                        Height = 16,
                        Width = 7,
                        Depth = 5,
                        Weight = (float) 0.05,
                        DeclaredValue = (float) 100.00,
                        InsuredValue = (float) 100.00
                    }
                },
                ShipmentInfo = new ShippingTransitInfo
                {
                    ShipmentDate = DateTime.Now.AddDays(3),
                    ShipmentReference = "123456",
                    Description = "Shipment desciption",
                    SignatureOption = "NO_SIGNATURE_REQUIRED",
                    CommercialInvoice = new object(),
                    BillTo = new BillTo
                    {
                        name = "Watattops",
                        account_number = "ACC-001",
                        country = "US",
                        zip = "99801"
                    },
                    ShipFrom = new Person
                    {
                        Name = "Rick Sanchez",
                        Company = "COREBRIDGE DEVELOPMENT",
                        Address = new Address
                        {
                            Street = "US-201b Capital St",
                            Street2 = "",
                            Region = "North America",
                            PostalCode = "04330",
                            Country = "US",
                            City = "Augusta"
                        },
                        State = "ME",
                        Email = "rick@earthc137.com",
                        Phone = "8015042351"
                    },
                    ShipTo = new Person
                    {
                        Name = "Morty Smith",
                        Company = "COREBRIDGE DEVELOPMENT",
                        Address = new Address
                        {
                            Street = "US-201b Capital St",
                            Street2 = "",
                            Region = "North America",
                            PostalCode = "04330",
                            Country = "US",
                            City = "Augusta"
                        },
                        State = "ME",
                        Email = "morty@earthc137.com",
                        Phone = "8015042351"
                    }
                },
                Services = new List<CarrierService>
                {
                    new CarrierService
                    {
                        Carrier = "usps",
                        Service = "usps_priority",
                        CarrierPackageCode = "usps_custom_package"
                    },
                    new CarrierService
                    {
                        Carrier = "usps",
                        Service = "usps_express",
                        CarrierPackageCode = "usps_custom_package"
                    }
                }
            };

            Guid registrationID = await GlobalTest.CreateRegistration();

            HttpResponseMessage response = await ControllerTestHelper.POST("/apsa/packandship", packShipRequest, registrationID);
            // 200 = Success Request
            Assert.AreEqual((int)response.StatusCode, 200, "Packing confirmation did not return success.");
            await GlobalTest.CleanUp();
        }

        [TestMethod]
        public async Task PackAndShipWithInvalidData()
        {
            PackShipRequest packShipRequest = new PackShipRequest
            {
                ID = "1",
                OrderID = 10061,
                Name = "Watattops",
                Items = new List<ShippingItem>
                {
                    new ShippingItem
                    {
                        ID = "1",
                        Name = "Venum Predator Gloves Yellow Green Black 16oz",
                        Quantity = 2,
                        Height = 0, // Height can't BE 0
                        Width = 0, // Width can't BE 0
                        Depth = 0, // Depth can't BE 0
                        Weight = (float) 0.05,
                        DeclaredValue = (float) 100.00,
                        InsuredValue = (float) 100.00
                    }
                },
                ShipmentInfo = new ShippingTransitInfo
                {
                    ShipmentDate = DateTime.Now.AddMonths(3),
                    ShipmentReference = "123456",
                    Description = "Shipment desciption",
                    SignatureOption = "Signature",
                    CommercialInvoice = new object(),
                    BillTo = new BillTo
                    {
                        name = "Watattops",
                        account_number = "ACC-001",
                        country = "US",
                        zip = "6000" // invalid zip code
                    },
                    ShipFrom = new Person
                    {
                        Name = "Rick Sanchez",
                        Company = "Rick and Morty Co.",
                        Address = new Address
                        {
                            Street = "Earth",
                            Street2 = "C137",
                            Region = "Pacific",
                            PostalCode = "6000", // invalid zip code
                            Country = "US"
                        },
                        Email = "rick@earthc137.com",
                        Phone = "1234567891"
                    },
                    ShipTo = new Person
                    {
                        Name = "Morty Smith",
                        Company = "Rick and Morty Co.",
                        Address = new Address
                        {
                            Street = "Earth",
                            Street2 = "C137",
                            Region = "Pacific",
                            PostalCode = "6000",
                            Country = "US"
                        },
                        Email = "morty@earthc137.com",
                        Phone = "1234567891"
                    }
                },
                Service = new CarrierService
                {
                    Carrier = "usps",
                    Service = "usps_priority",
                    CarrierPackageCode = "INVALID_PACKAGE_CODE"
                },
                Services = new List<CarrierService>
                {
                    new CarrierService
                    {
                        Carrier = "usps",
                        Service = "usps_priority",
                        CarrierPackageCode = "INVALID_PACKAGE_CODE"
                    },
                    new CarrierService
                    {
                        Carrier = "usps",
                        Service = "usps_express",
                        CarrierPackageCode = "INVALID_PACKAGE_CODE"
                    }
                }
            };

            Guid registrationID = await GlobalTest.CreateRegistration();

            HttpResponseMessage response = await ControllerTestHelper.POST("/apsa/packandship", packShipRequest, registrationID);
            // 204 = NoContent/NoResponse
            Assert.AreEqual((int)response.StatusCode, 204, "Failed to get the shipping rate due to invalid data.");
            await GlobalTest.CleanUp();
        }
    }
}
