rm -rf publish-output;
dotnet publish --self-contained true --runtime linux-x64 -o publish-output;
docker build -t corebridge/end-apsa:latest .