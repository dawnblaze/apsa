﻿using System;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using APSA.Web.Models;
using APSA.Web.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.Net.Http.Headers;

namespace APSA.Web.Authentication
{
    internal class APSAAuthHandler : AuthenticationHandler<APSAAuthOptions>
    {
        public APSAAuthHandler(IOptionsMonitor<APSAAuthOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.TryGetValue(HeaderNames.Authorization, out StringValues authorization))
            {
#if LOCAL
                Registration registration = new Registration
                {
                    AppType = AppType.Endor,
                    BID = -99,
                    Currency = "USD",
                    ExpirationDT = DateTime.Now.AddYears(1),
                    ProcessorType = Processor.CardConnect,
                    RegistrationID = Guid.Empty,
                    IsSandboxed = true,
                    TimeZone = Models.TimeZone.Central,
                };

                AuthenticationTicket ticket = new AuthenticationTicket(
                    APSAAuthHelper.CreateUser(registration),
                    APSAAuthOptions.DefaultScheme
                    );

                return AuthenticateResult.Success(ticket);
#else
                return AuthenticateResult.Fail("Cannot read authorization header.");
#endif
            }

            string[] authParts = authorization.ToString().Split(' ', StringSplitOptions.RemoveEmptyEntries);

            if (authParts.Length < 2)
                return AuthenticateResult.Fail("Invalid authentication.");

            string authType = authParts[0].ToLower();
            string authString = authParts[1];

            if (authType == "registration")
            {
                if (!Guid.TryParse(authString, out Guid registrationID))
                    return AuthenticateResult.Fail("Cannot read registration id.");

                Registration registration = await RegistrationService.FindRegistration(registrationID);

                if (registration == null)
                    return AuthenticateResult.Fail("Cannot find registration.");

                if (registration.ExpirationDT < DateTime.UtcNow)
                    return AuthenticateResult.Fail("Registration has expired.");

                AuthenticationTicket ticket = new AuthenticationTicket(
                    APSAAuthHelper.CreateUser(registration),
                    APSAAuthOptions.DefaultScheme
                    );

                return AuthenticateResult.Success(ticket);
            }

            else
                return AuthenticateResult.Fail("Invalid authentication type.");
        }
    }
}
