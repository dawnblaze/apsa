﻿using System;
using Microsoft.AspNetCore.Authentication;

namespace APSA.Web.Authentication
{
    internal static class APSAAuthExtensions
    {
        internal static AuthenticationBuilder AddCustomAuth(this AuthenticationBuilder builder, Action<APSAAuthOptions> configureOptions)
        {
            return builder.AddScheme<APSAAuthOptions, APSAAuthHandler>(APSAAuthOptions.DefaultScheme, APSAAuthOptions.SchemeDisplayName, configureOptions);
        }
    }
}
