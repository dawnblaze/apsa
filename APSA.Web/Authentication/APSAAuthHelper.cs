﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using APSA.Web.Models;
using Newtonsoft.Json;

namespace APSA.Web.Authentication
{
    internal static class APSAAuthHelper
    {
        private static void AddClaim(ClaimsIdentity identity, string type, string value)
        {
            if (value != null)
                identity.AddClaim(new Claim(type, value));
        }
        private static void AddClaim<T>(ClaimsIdentity identity, string type, T value)
            where T : struct
        {
            AddClaim(identity, type, value.ToString());
        }
        private static string ReadClaim(ClaimsIdentity identity, string claimName, string defaultValue)
        {
            Claim claim = identity.Claims.FirstOrDefault(t => t.Type == claimName);

            if (claim == null)
                return defaultValue;

            return claim.Value;
        }
        private static T ReadClaim<T>(ClaimsIdentity identity, string claimName, T defaultValue)
            where T : struct
        {
            Claim claim = identity.Claims.FirstOrDefault(t => t.Type == claimName);

            if (claim == null)
                return defaultValue;

            TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));

            return (T)converter.ConvertFromString(null, CultureInfo.InvariantCulture, claim.Value);
        }

        public static Registration CreateRegistration(ClaimsPrincipal User)
        {
            ClaimsIdentity identity = (ClaimsIdentity)(User.Identity);

            string authType = ReadClaim(identity, "AuthenticationType", "");

            if (authType != "Registration")
                return null;

            var result = new Registration
            {
                RegistrationID = ReadClaim(identity, "RegistrationID", Guid.Empty),
                BID = ReadClaim(identity, "BID", (short)-1),
                ExpirationDT = ReadClaim(identity, "ExpirationDT", DateTime.UtcNow.AddDays(1)),
                IsSandboxed = ReadClaim(identity, "IsSandboxed", false),
                ShippingCredentials = new ShippingCredentials(),
                PackingCredentials = new PackingCredentials(),
                Name = ReadClaim(identity, "Name", ""),
                Currency = ReadClaim(identity, "Currency", "USD"),
                UnitOfWeight = ReadClaim(identity, "UnitOfWeight", "kg"),
                UnitOfLength = ReadClaim(identity, "UnitOfLength", "cm"),
                ShippingContainers = ReadClaim(identity, "ShippingContainers", "[]")
            };

            result.ShippingCredentials.Login = ReadClaim(identity, "ShippingCredentials.Login", "");
            result.ShippingCredentials.APIKey = ReadClaim(identity, "ShippingCredentials.APIKey", "");
            result.PackingCredentials.Login = ReadClaim(identity, "PackingCredentials.Login", "");
            result.PackingCredentials.APIKey = ReadClaim(identity, "PackingCredentials.APIKey", "");

            string s = ReadClaim(identity, "AppType", "");
            if (Enum.TryParse(s, true, out AppType appType))
                result.AppType = appType;

            s = ReadClaim(identity, "ShippingProcessor", "");
            if (Enum.TryParse(s, true, out ShippingProcessorType shippingProcessorType))
                result.ShippingProcessorType = shippingProcessorType;

            s = ReadClaim(identity, "PackingProcessor", "");
            if (Enum.TryParse(s, true, out PackingProcessorType packingProcessorType))
                result.PackingProcessorType = packingProcessorType;

            return result;
        }
        public static ClaimsPrincipal CreateUser(Registration registration)
        {
            ClaimsIdentity identity = new ClaimsIdentity("APSA Authentication");
            AddClaim(identity, "AuthenticationType", "Registration");
            AddClaim(identity, "RegistrationID", registration.RegistrationID);
            AddClaim(identity, "BID", registration.BID);
            AddClaim(identity, "ExpirationDT", registration.ExpirationDT);
            AddClaim(identity, "AppType", registration.AppType.Name());
            AddClaim(identity, "IsSandboxed", registration.IsSandboxed);
            AddClaim(identity, "ShippingProcessor", registration.ShippingProcessorType.ToString());
            AddClaim(identity, "ShippingCredentials.Login", registration.ShippingCredentials.Login);
            AddClaim(identity, "ShippingCredentials.APIKey", registration.ShippingCredentials.APIKey);
            AddClaim(identity, "PackingProcessor", registration.PackingProcessorType.ToString());
            AddClaim(identity, "PackingCredentials.Login", registration.PackingCredentials.Login);
            AddClaim(identity, "PackingCredentials.APIKey", registration.PackingCredentials.APIKey);
            AddClaim(identity, "Name", registration.Name);
            AddClaim(identity, "Currency", registration.Currency);
            AddClaim(identity, "UnitOfWeight", registration.UnitOfWeight.Trim());
            AddClaim(identity, "UnitOfLength", registration.UnitOfLength.Trim());
            AddClaim(identity, "ShippingContainers", registration.ShippingContainers);

            List<ClaimsIdentity> identities = new List<ClaimsIdentity>
            {
                identity
            };

            return new ClaimsPrincipal(identities);
        }
    }
}
