﻿using Microsoft.AspNetCore.Authentication;

namespace APSA.Web.Authentication
{
    internal class APSAAuthOptions : AuthenticationSchemeOptions
    {
        internal const string DefaultScheme = "APSA";
        internal const string SchemeDisplayName = "APSA Scheme";
    }
}
