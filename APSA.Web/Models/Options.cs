﻿namespace APSA.Web.Models
{
    /// <summary>
    /// Custom object used for options for either the PackingServer or the ShippingService.
    /// </summary>
    public class Options
    {
        /// <summary>
        /// Used by Bin-Packing:
        /// The background color for generated images.The value is supplied as an RGB value in Hex (e.g., "1A1A6B")
        /// </summary>
        public string BackgroundColor { get; set; }
        /// <summary>
        /// Used by Bin-Packing:
        /// The color of dashed container lines that are displayed as semi-transparent so the image is not not obstructed.
        /// </summary>
        public string ContainerTransparentBorderColor { get; set; }
        /// <summary>
        /// Used by Bin-Packing:
        /// The color of solid borders lines for containers.
        /// </summary>
        public string ContainerSolidBorderColor { get; set; }
        /// <summary>
        /// Used by Bin-Packing:
        /// The fill color for containers.
        /// </summary>
        public string ContainerFillColor { get; set; }
        /// <summary>
        /// Used by Bin-Packing:
        /// The default border color for items in images.
        /// The value is supplied as an RGB value in Hex(e.g., "1A1A6B")
        /// </summary>
        public string ItemBorderColor { get; set; }
        /// <summary>
        /// Used by Bin-Packing:
        /// The border color of the current(last) item in the Step-by-Step images.
        /// </summary>
        public string ItemStepByStepBorderColor { get; set; }
        /// <summary>
        /// Used by Bin-Packing:
        /// The fill color of the current(last) item in the Step-by-Step images.
        /// </summary>
        public string ItemStepByStepFillColor { get; set; }
    }
}