namespace APSA.Web.Models
{
    public class BusinessKey
    {
        public System.Guid BusinessID {get; set;}
        public string businessKey {get; set;}
        public bool IsActive {get; set;}
        public string AppType {get; set;}
        public string BusinessName {get; set;}
        public string AssociationID {get; set;}    

    }
}