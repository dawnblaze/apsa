﻿using System;
using System.Collections.Generic;

namespace APSA.Web.Models
{
    public class ClientRegistration
    {
        /// <summary>
        /// The BusinessKey associated with this client.
        /// </summary>
        public Guid BusinessKey { get; set; }
        /// <summary>
        /// Identifies the type of Application requesting a Key.
        /// </summary>
        public AppType AppType { get; set; }
        /// <summary>
        /// If set to Sandboxed, no permanent transactions or changes are made.
        /// </summary>
        public bool IsSandboxed { get; set; }
        /// <summary>
        /// The packing application.
        /// </summary>
        public string PackingProcessorType { get; set; }
        /// <summary>
        /// Custom object used for packing credential information.
        /// </summary>
        public PackingCredentials PackingCredentials { get; set; }
        /// <summary>
        /// The packing application.
        /// </summary>
        public string ShippingProcessorType { get; set; }
        /// <summary>
        /// Custom object used for shipping credential information.
        /// </summary>
        public ShippingCredentials ShippingCredentials { get; set; }
        /// <summary>
        /// Unit of measuring length for containers and items. Defaults to mm.
        /// </summary>
        public string UnitOfLength { get; set; }
        /// <summary>
        /// Unit of measuring weight for containers and items. Defaults to kg.
        /// </summary>
        public string UnitOfWeight { get; set; }
        /// <summary>
        /// A list of the available containers that can be used.
        /// </summary>
        public IList<ShippingContainer> Containers { get; set; }
        /// <summary>
        /// The currency for all transactions.  The currency must be the currency codes specified in CurrencyType Enum.
        /// USD
        /// CAD
        /// AUD
        /// This currency is used for all transactions within the registration.
        /// If multiple currency support is needed for the same terminal, two separate registrations must be obtained.
        /// </summary>
        public string Currency { get; set; }
        /// <summary>
        /// Date/Time the registration expires.
        /// </summary>
        public DateTime ExpirationDT { get; set; }
    }
}
