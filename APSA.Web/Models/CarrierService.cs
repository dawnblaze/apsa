﻿namespace APSA.Web.Models
{
    /// <summary>
    /// The CarrierService Record stores a specific carrier and service. 
    /// This record is used to indicate what service is begin quotes or booked.
    /// </summary>
    public class CarrierService
    {
        /// <summary>
        /// The (system) name for the carrier.
        /// </summary>
        public string Carrier { get; set; }
        /// <summary>
        /// The (system) name for the service.
        /// </summary>
        public string Service { get; set; }

        public string CarrierPackageCode { get; set; }
    }
}
