﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Models
{
    /// <summary>
    /// The PackedItem Record descends from the ShippingItem record. 
    /// It contains the same shipping information and information determined by the packing process. 
    /// The PackedItem record are used to describe was it packed in a container.
    /// </summary>
    public class PackedItem : ShippingItem
    {
        public string ItemImageURL { get; set; }
    }
}
