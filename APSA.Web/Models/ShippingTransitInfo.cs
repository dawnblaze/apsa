﻿using System;

namespace APSA.Web.Models
{
    /// <summary>
    /// The shipping transit information includes the address and billing information associate with the shipment.
    /// </summary>
    public class ShippingTransitInfo
    {
        /// <summary>
        /// The date the shipment was sent.
        /// </summary>
        public DateTime ShipmentDate { get; set; }
        /// <summary>
        /// A reference / tracking number for the shipment
        /// </summary>
        public string ShipmentReference { get; set; }
        /// <summary>
        /// A description for the shipment.
        /// </summary>
        public string Description { get; set; }
        public string SignatureOption { get; set; }
        public object CommercialInvoice { get; set; }
        /// <summary>
        /// party / account / country / zip
        /// </summary>
        public BillTo BillTo { get; set; }
        /// <summary>
        /// The person and address of the shipment sender.
        /// </summary>
        public Person ShipFrom { get; set; }
        /// <summary>
        /// The person and address of the shipment recipient.
        /// </summary>
        public Person ShipTo { get; set; }
    }

    public class BillTo
    {
        public string name { get; set; }
        public string account_number { get; set; }
        public string country { get; set; }
        public string zip { get; set; }
    }
}
