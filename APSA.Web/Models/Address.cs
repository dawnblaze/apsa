﻿namespace APSA.Web.Models
{
    public class Address
    {
        public string Street { get; set; }
        /// <summary>
        /// The second line of the street address.
        /// </summary>
        public string Street2 { get; set; }
        /// <summary>
        /// The state, province, etc.
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// The zip or postal code.
        /// </summary>
        public string PostalCode { get; set; }
        /// <summary>
        /// The two digit ISO country code of the address (e.g., US, CA, AU, etc.)
        /// </summary>
        public string Country { get; set; }

        public string City {get; set;}
    }
}
