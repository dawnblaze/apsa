﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Models
{
    public class CarrierServiceQuote : CarrierService
    {
        /// <summary>
        /// The shipping-only price
        /// </summary>
        public float SubTotalPrice { get; set; }
    }
}
