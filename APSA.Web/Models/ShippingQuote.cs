﻿using System.Collections.Generic;
using APSA.Web.Libraries.XPS.Models.Responses;

namespace APSA.Web.Models
{
    /// <summary>
    /// A ShippingQuote record contains a quoted price for the shipment. 
    /// The ShippingQuote Record descends from the PackingPlan record and adds pricing information for the selected service.
    /// </summary>
    public class ShippingQuote : PackingPlan
    {
        /// <summary>
        /// The declared value of the ItemGroup, equal to the SUM() of the Declared Value of all Containers in the Group.
        /// </summary>
        public int DeclaredValue { get; set; }
        /// <summary>
        /// The insured value of the ItemGroup, equal to the SUM() of the Insured Value of all Containers in the Group.
        /// </summary>
        public int InsuredValue { get; set; }
        /// <summary>
        /// An array of quoted services, one for each carrier service quoted. 
        /// If the service could not be quoted, the price for that service is NULL.
        /// </summary>
        public List<CarrierServiceQuote> Quote { get; set; }
        /// <summary>
        /// An array of objects containing the {description, price} for any surcharges.
        /// </summary>
        public List<Surcharge> Surcharges { get; set; }
        /// <summary>
        /// The net price, including surcharges.
        /// </summary>
        public float Price { get; set; }
        /// <summary>
        /// The zone used for pricing calculations.
        /// </summary>
        public int Zone { get; set; }
        /// <summary>
        /// A boolean value indicating if the quoting process was not able to provide a quote on the container.
        /// </summary>
        public bool HasShippingErrors { get; set; }
        /// <summary>
        /// A text string containing any error messages associated with the booking of shipping.
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
