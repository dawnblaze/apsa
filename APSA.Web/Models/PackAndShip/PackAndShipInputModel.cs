﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Models.PackAndShip
{
    public class PackAndShipInputModel
    {
        public PackAndShipInputModel(Registration registration, Models.PackShipRequest packShipRequestRecord, bool allowVertical, bool includeItemImage, bool includeStepByStepImage, bool includeGroupImage)
        {

            Registration = registration;
            PackShipRequestRecord = packShipRequestRecord;
            AllowVertical = allowVertical;
            IncludeItemImage = includeItemImage;
            IncludeStepByStepImage = includeStepByStepImage;
            IncludeGroupImage = includeGroupImage;
            //This default value is currently based on what is hardcoded when PackingService.MultiBin... is called in PackingController.
            PackingType = 2; 
        }

        public Registration Registration { get; set; }
        public Models.PackShipRequest PackShipRequestRecord { get; set; }
        public bool AllowVertical { get; set; }
        public bool IncludeItemImage { get; set; }
        public bool IncludeStepByStepImage { get; set; }
        public bool IncludeGroupImage { get; set; }
        public int PackingType { get; set; }
    }
}
