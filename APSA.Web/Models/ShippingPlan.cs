﻿using System;
using System.Collections.Generic;
using APSA.Web.Libraries.XPS.Models.Responses;

namespace APSA.Web.Models
{
    public class ShippingPlan : PackingPlan
    {
        /// <summary>
        /// The declared value of the ItemGroup, equal to the SUM() of the Declared Value of all Containers in the Group.
        /// </summary>
        public float DeclaredValue { get; set; }
        /// <summary>
        /// The insured value of the ItemGroup, equal to the SUM() of the Insured Value of all Containers in the Group.
        /// </summary>
        public float InsuredValue { get; set; }
        /// <summary>
        /// The shipping-only price.
        /// This value is NULL if 
        /// </summary>
        public float? SubTotalPrice { get; set; }
        /// <summary>
        /// An array of objects containing the {description, price} for any surcharges.
        /// </summary>
        public List<Surcharge> Surcharges { get; set; }
        /// <summary>
        /// The net price, including surcharges.
        /// </summary>
        public float Price { get; set; }
        /// <summary>
        /// The zone used for pricing calculations.
        /// </summary>
        public int Zone { get; set; }
        /// <summary>
        /// A boolean value indicating if the shipping process was not able to quote.
        /// </summary>
        public bool HasShippingErrors { get; set; }
        /// <summary>
        /// A text string containing any error messages associated with the booking of shipping.
        /// </summary>
        public string ErrorMessage { get; set; }
        /// <summary>
        /// A referenceID for this packing configuration. 
        /// This can be used to track the shipping, cancel the 
        /// </summary>
        public Guid ReferenceID { get; set; }
        /// <summary>
        /// Booking Number from XPS
        /// </summary>
        public string ReferenceNumber { get; set; }
        /// <summary>
        /// Carrier Tracking Number
        /// </summary>
        public string TrackingNumber { get; set; }
        /// <summary>
        /// Prepaid Balance with Shipping Service, if supported.
        /// </summary>
        public float PrePaidBalance { get; set; }
    }
}
