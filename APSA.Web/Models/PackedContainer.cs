﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Models
{
    /// <summary>
    /// The PackedContainer Record descends from the ShippingContainer record. 
    /// It contains the same basic container information as well as the information about what is packed inside of it. 
    /// The PackedContainer record is used to describe the shipped contents of a container.
    /// </summary>
    public class PackedContainer : ShippingContainer
    {
        /// <summary>
        /// The loaded weight of the container, equal to the SUM( Items.Weight * Items.Quantity ) + ContainerWeight
        /// </summary>
        public float Weight {
            get
            {
                var y = Items.Select(x => x.Weight * x.Quantity);
                // Get the total weight of all PackedItems
                float itemsWeight = Items.Sum(i => (i.Weight * i.Quantity));
                //foreach (var packedItem in Items)
                //{
                //    itemsWeight += (packedItem.Weight * packedItem.Quantity);
                //}

                // Add the total weight of all PackedItems to the ContainerWeight
                return itemsWeight + ContainerWeight;
            }
        }
        /// <summary>
        /// The space of the container that is actually filled with items, expressed in cubic UnitOfLength.
        /// </summary>
        public float FilledSpace { get; set; }
        public List<PackedItem> Items { get; set; }

        /// <summary>
        /// The declared price for this container, which is computed as the sum of the declared value for all items in the container.
        /// </summary>
        public float DeclaredValue
        {
            get
            {
                // Get the total declared value of all PackedItems
                //float itemsDeclaredValue = 0;
                //foreach (var packedItem in Items)
                //{
                //    itemsDeclaredValue += packedItem.DeclaredValue;
                //}
                
                return Items.Sum(i => (i.DeclaredValue * i.Quantity));
            }
        }
        /// <summary>
        /// The insured value of this container, which is computed as the sum of the insured value for all items in the container.
        /// </summary>
        public float InsuredValue {
            get
            {
                // Get the total declared value of all PackedItems
                //float itemsInsuredValue = 0;
                //foreach (var packedItem in Items)
                //{
                //    itemsInsuredValue += packedItem.InsuredValue;
                //}

                return Items.Sum(i => (i.InsuredValue * i.Quantity));
            }
        }
    }
}
