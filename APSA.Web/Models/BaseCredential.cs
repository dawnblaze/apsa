﻿namespace APSA.Web.Models
{
    public class BaseCredential
    {
        /// <summary>
        /// Login for service provider
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// API access key for application
        /// </summary>
        public string APIKey { get; set; }
    }
}