﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Models
{
    /// <summary>
    /// The PackingPlan describes the packed configuration of a shipment. 
    /// The PackingPlan Record descends from the ShippingRequest record.
    /// </summary>
    public class PackingPlan : PackShipRequest
    {
        public bool HasPackingErrors { get; set; }
        public List<PackedContainer> Containers { get; set; }
        public List<ShippingItem> UnpackedItems { get; set; }
        public string StepByStepImageURL { get; set; }
        public string GroupImageURL { get; set; }
    }
}
