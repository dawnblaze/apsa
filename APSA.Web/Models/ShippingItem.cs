﻿namespace APSA.Web.Models
{
    /// <summary>
    /// The ShippingItem record contains the information for a single type of object that needs to be shipped to a single address. 
    /// It normally corresponds to a line items to be shipped.
    /// </summary>
    public class ShippingItem
    {
        /// <summary>
        /// The ID of the line item for this item.  This is usually the Line Item number, but may be any unique reference.
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// The name of the item being packed.  This is usually the product or material name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The quantity of items being shipped. All items must be identical in the dimensions supplied. 
        /// Defaults to 1 if not supplied.
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// The height of the item being packed. The UnitOfLength specified in the registration is assumed.
        /// </summary>
        public float Height { get; set; }
        /// <summary>
        /// The width of the item being packed. The UnitOfLength specified in the registration is assumed.
        /// </summary>
        public float Width { get; set; }
        /// <summary>
        /// The depth of the item being packed. The UnitOfLength specified in the registration is assumed.
        /// </summary>
        public float Depth { get; set; }
        /// <summary>
        /// The weight of one item. The UnitOfWeight specified in the registration is assumed.
        /// If not supplied, the weight is assumed to be 0.00 and not weight constraints are considered.
        /// </summary>
        public float Weight { get; set; }
        /// <summary>
        /// The declared price for one of these items.  This is normally the Unit Price.  
        /// If not supplied, the declared value is assumed to be 0.00.
        /// </summary>
        public float DeclaredValue { get; set; }
        /// <summary>
        /// The insured amount for one of these items.  This is usually the same as declared value.
        /// If not supplied, the item shipment is not insured.
        /// </summary>
        public float InsuredValue { get; set; }
    }
}
