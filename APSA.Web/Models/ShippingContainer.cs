﻿namespace APSA.Web.Models
{
    public class ShippingContainer
    {
        public string Name { get; set; }
        public string NameLabel { get; set; }
        public float Height { get; set; }
        public float Width { get; set; }
        public float Depth { get; set; }
        public float ContainerWeight { get; set; }
        public float MaxWeight { get; set; }
    }
}