﻿namespace APSA.Web.Models
{
    /// <summary>
    /// Delete Response class
    /// </summary>
    public class DeleteResponse
    {
        /// <summary>
        /// If the item is not found.
        /// </summary>
        public bool NotFound { get; set; }
    }
}
