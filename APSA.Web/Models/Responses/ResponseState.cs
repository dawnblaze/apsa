﻿namespace APSA.Web.Models
{
    /// <summary>
    /// The state of the Response
    /// </summary>
    public enum ResponseState
    {
        /// <summary>
        /// Success
        /// </summary>
        Success,
        /// <summary>
        /// Error
        /// </summary>
        Error,
        /// <summary>
        /// Exception
        /// </summary>
        Exception,
    }
}
