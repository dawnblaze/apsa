﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Models.Responses
{
    public class DeleteExpiredRequestedImagesResponse
    {
        private List<string> _blobsFoundList = new List<string>();
        private List<string> _deletedBlobs = new List<string>();

        public int NumDaysOldSetting { set; get; }

        public int BlobsFoundCount
        {

            get
            {

                return this.BlobsFound.Count();

            }
        }


        public List<string> BlobsFound {
            
            get
            {
                return this._blobsFoundList;
            }
        }

        
        public int DeletedBlobsCount
        {
            get
            {
                return this._deletedBlobs.Count();
            }
        }

        public List<string> DeletedBlobs
        {
            
           get
           {
                return this._deletedBlobs;
           }
        }

        public string ErrorMessage { set; get; }
    }
}
