﻿using System;
using System.ComponentModel;
using System.Reflection;
using APSA.Web.Services;

namespace APSA.Web.Models
{
    /// <summary>
    /// Base Response class
    /// </summary>
    public class Response
    {
        /// <summary>
        /// Specifies the state of the response
        /// </summary>
        public ResponseState State { get; set; }
        /// <summary>
        /// Specifies whether the response state is success or not
        /// </summary>
        public bool WasSuccess
        {
            get
            {
                return (State == ResponseState.Success);
            }
        }
        /// <summary>
        /// Specifies the outcome message
        /// </summary>
        public ResponseMessage Message { get; set; }
        /// <summary>
        /// Returns the message text of a <see cref="ResponseMessage"/>.
        /// </summary>
        public string MessageText
        {
            get
            {
                if (Message == ResponseMessage.MiscError)
                    return ErrorText;

                FieldInfo fi = Message.GetType().GetField(Message.ToString());

                DescriptionAttribute[] attributes =
                    (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes != null && attributes.Length > 0)
                    return attributes[0].Description;

                return Message.ToString();
            }
        }
        /// <summary>
        /// When the response is a failure without an exception or prefined error, this is the error message
        /// </summary>
        public string ErrorText { get; set; }
        /// <summary>
        /// When the response is a failure, this is the exception
        /// </summary>
        public Exception Exception { get; set; }
        /// <summary>
        /// Creates a success response of the specified type.
        /// </summary>
        public static Response Success()
        {
            return new Response
            {
                State = ResponseState.Success,
                Message = ResponseMessage.Success,
                ErrorText = null,
                Exception = null
            };
        }
        /// <summary>
        /// Creates a validation error response of the specified type.
        /// </summary>
        /// <param name="message">Error message explaining the failure</param>
        /// <returns></returns>
        public static Response Error(ResponseMessage message)
        {
            return new Response
            {
                State = ResponseState.Error,
                Message = message,
                ErrorText = null,
                Exception = null
            };
        }
        /// <summary>
        /// Creates a validation error response of the specified type.
        /// </summary>
        /// <param name="errorText">Error message explaining the failure</param>
        /// <returns></returns>
        public static Response Error(string errorText)
        {
            return new Response
            {
                State = ResponseState.Error,
                Message = ResponseMessage.MiscError,
                ErrorText = errorText,
                Exception = null
            };
        }
        /// <summary>
        /// Creates a Exception of the specified type.
        /// </summary>
        /// <param name="exception">Exception that caused the failure</param>
        /// <returns></returns>
        public static Response Error(Exception exception)
        {
            return new Response
            {
                State = ResponseState.Exception,
                Message = ResponseMessage.Exception,
                ErrorText = null,
                Exception = exception
            };
        }
        /// <summary>
        /// Adds Debug messages to the log concerning the State of the Response
        /// </summary>
        /// <param name="category">Category in the log</param>
        /// <param name="method">The Method generated this response</param>
        /// <param name="bid">Business ID of the log entry</param>
        public void LogResult(string category, string method, short? bid = null)
        {
            switch (State)
            {
                case ResponseState.Success:
                    LoggingService.Debug(category, $"{method} Successful", bID: bid);
                    break;
                case ResponseState.Error:
                    LoggingService.Debug(category, $"{method} Failed", notes: MessageText, bID: bid);
                    break;
                case ResponseState.Exception:
                    LoggingService.Debug(category, $"{method} Failed", exception: Exception, bID: bid);
                    break;
            }
        }
    }
    /// <summary>
    /// Response class with ResponseObject
    /// </summary>
    public class Response<T> : Response
    {
        /// <summary>
        /// The object that contains response values.
        /// </summary>
        public T ResponseObject { get; set; }
        /// <summary>
        /// Creates a success response of the specified type.
        /// </summary>
        public static Response<T> Success(T responseObject)
        {
            return new Response<T>
            {
                State = ResponseState.Success,
                Message = ResponseMessage.Success,
                Exception = null,
                ErrorText = null,
                ResponseObject = responseObject
            };
        }
        /// <summary>
        /// Creates a validation error response of the specified type.
        /// </summary>
        /// <param name="message">Error message explaining the failure</param>
        /// <returns></returns>
        public new static Response<T> Error(ResponseMessage message)
        {
            return new Response<T>
            {
                State = ResponseState.Error,
                Message = message,
            };
        }
        /// <summary>
        /// Creates a validation error response of the specified type.
        /// </summary>
        /// <param name="errortText">Error message explaining the failure</param>
        /// <returns></returns>
        public new static Response<T> Error(string errortText)
        {
            return new Response<T>
            {
                State = ResponseState.Error,
                Message = ResponseMessage.MiscError,
                ErrorText = errortText,
            };
        }
        /// <summary>
        /// Creates a exception error response of the specified type.
        /// </summary>
        /// <param name="exception">Exception that caused the failure</param>
        /// <returns></returns>
        public new static Response<T> Error(Exception exception)
        {
            return new Response<T>
            {
                State = ResponseState.Exception,
                Message = ResponseMessage.Exception,
                Exception = exception,
            };
        }
        /// <summary>
        /// Adds Debug messages to the log concerning the State of the Response
        /// </summary>
        /// <param name="category">Category in the log</param>
        /// <param name="method">The Method generated this response</param>
        /// <param name="bid">Business ID of the log entry</param>
        public new void LogResult(string category, string method, short? bid = null)
        {
            switch (State)
            {
                case ResponseState.Success:
                    LoggingService.Debug(category, $"{method} Successful", notes: ResponseObject, bID: bid);
                    break;
                case ResponseState.Error:
                    LoggingService.Debug(category, $"{method} Failed", notes: MessageText, bID: bid);
                    break;
                case ResponseState.Exception:
                    LoggingService.Debug(category, $"{method} Failed", exception: Exception, bID: bid);
                    break;
            }
        }

        public static implicit operator Response<T>(Response<DeleteResponse> v)
        {
            throw new NotImplementedException();
        }
    }
}
