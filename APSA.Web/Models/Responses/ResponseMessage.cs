﻿using System.ComponentModel;

namespace APSA.Web.Models
{
    /// <summary>
    /// Possible reponse messages
    /// </summary>
    public enum ResponseMessage
    {
        /// <summary>
        /// Success
        /// </summary>
        [Description("Success")]
        Success,
        /// <summary>
        /// Exception
        /// </summary>
        [Description("Exception")]
        Exception,
        /// <summary>
        /// Timeout
        /// </summary>
        [Description("Timeout")]
        Timeout,
        /// <summary>
        /// CancelledByUser
        /// </summary>
        [Description("Cancelled By User")]
        CancelledByUser,
        /// <summary>
        /// MiscError
        /// </summary>
        MiscError,
        /// <summary>
        /// NotImplemented
        /// </summary>
        [Description("Request not implemented")]
        NotImplemented,
        /// <summary>
        /// BusinessKeyRequired
        /// </summary>
        [Description("Request input is missing or invalid")]
        InvalidRequest,
        /// <summary>
        /// BusinessKeyNotFound
        /// </summary>
        [Description("Business key not found")]
        BusinessKeyNotFound,
        /// <summary>
        /// RegistrationNotFound
        /// </summary>
        [Description("Registration not found")]
        RegistrationNotFound,
        /// <summary>
        /// RegistrationHasExpired
        /// </summary>
        [Description("Registration has expired")]
        RegistrationHasExpired,
        /// <summary>
        /// InvalidSessionKey
        /// </summary>
        [Description("Invalid session key")]
        InvalidSessionKey,
        /// <summary>
        /// InvalidAppType
        /// </summary>
        [Description("Invalid application type")]
        InvalidAppType,
        /// <summary>
        /// BusinessIDRequired
        /// </summary>
        [Description("Business ID is required")]
        BIDRequired,
        /// <summary>
        /// BusinessNameRequired
        /// </summary>
        [Description("Business name is required")]
        BusinessNameRequired,
        /// <summary>
        /// CurrencyRequired
        /// </summary>
        [Description("Currency is required")]
        CurrencyRequired,

        [Description("Service is required")]
        ServiceRequired,

        [Description("Error. Unable to pack items.")]
        PackingError,

        [Description("Unable to retrieve carriers.")]
        NoXPSCarriers
    }
}
