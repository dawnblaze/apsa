﻿using System.Collections.Generic;

namespace APSA.Web.Models
{
    /// <summary>
    /// The ShippingRequest record contains a collection of items to be shipped to a single address. 
    /// It normally corresponds to a single shipment of an order.
    /// </summary>
    public class PackShipRequest
    {
        /// <summary>
        /// The ID of the shipping item group.
        /// This is usually the Shipment ID. 
        /// If the entire order is being shipped together, the order number may be used.
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// The order id for the order. This is used for reporting and support.
        /// </summary>
        public int OrderID { get; set; }
        /// <summary>
        /// The "friendly" name of the shipping item group. (e.g. "Order 2727 Shipment 1")
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The items being shipped in the group.
        /// </summary>
        public List<ShippingItem> Items { get; set; }
        /// <summary>
        /// The Shipping Information. Required for quoting and booking requests, but not for packing endpoints.
        /// </summary>
        public ShippingTransitInfo ShipmentInfo { get; set; }
        /// <summary>
        /// The carrier service to be quoted or booked with.
        /// This field is not used for Packing requests and required for the Booking endpoints. 
        /// Either the Service or Services property may be specified for Quoting endpoints.
        /// </summary>
        public CarrierService Service { get; set; }
        /// <summary>
        /// An array of carrier service to be quoted.
        /// This field can only be used for Quoting endpoints. For booking endpoints, the single Service property must be used.
        /// </summary>
        public List<CarrierService> Services { get; set; }
    }
}
