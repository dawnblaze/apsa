﻿namespace APSA.Web.Models
{
    /// <summary>
    /// Business Key request class used when adding a new business key registration
    /// </summary>
    public class BusinessKeyRequest
    {
        /// <summary>
        /// Application the created the registration
        /// </summary>
        public string AppType { get; set; }
        /// <summary>
        /// The company name of the business
        /// </summary>
        public string BusinessName { get; set; }
        /// <summary>
        /// The association id of the business
        /// </summary>
        public int? AssociationID { get; set; }
        /// <summary>
        /// The business id of the business
        /// </summary>
        public int BID { get; set; }
    }
}
