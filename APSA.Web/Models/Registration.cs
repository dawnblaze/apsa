﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace APSA.Web.Models
{
    /// <summary>
    /// Registration model.
    /// </summary>
    public class Registration
    {
        /// <summary>
        /// Registration ID
        /// </summary>
        public Guid RegistrationID { get; set; }
        /// <summary>
        /// ID of the Business
        /// </summary>
        public short BID { get; set; }
        /// <summary>
        /// Date/Time the registration expires
        /// </summary>
        public DateTime ExpirationDT { get; set; }
        /// <summary>
        /// Application the created the registration
        /// </summary>
        public AppType AppType { get; set; }
        /// <summary>
        /// If true, the sandbox credentials are used
        /// </summary>
        public bool IsSandboxed { get; set; }
        /// <summary>
        /// The shipping processor to use
        /// </summary>
        public ShippingProcessorType ShippingProcessorType { get; set; }
        /// <summary>
        /// The packing processor to use
        /// </summary>
        public PackingProcessorType PackingProcessorType { get; set; }
        /// <summary>
        /// Custom object used for packing credential information.
        /// </summary>
        public PackingCredentials PackingCredentials { get; set; }
        /// <summary>
        /// Custom object used for shipping credential information.
        /// </summary>
        public ShippingCredentials ShippingCredentials { get; set; }
        /// <summary>
        /// List of credentials as an XML string
        /// </summary>
        [JsonIgnore]
        public string Credentials
        {
            get
            {
                if (PackingCredentials == null && ShippingCredentials == null)
                    return null;

                var elements = new List<XElement>();

                var packingCredentialsElement = new List<XElement>();
                var shippingCredentialsElement = new List<XElement>();

                if (PackingCredentials != null)
                {
                    var packingLogin = new XElement("Login", PackingCredentials.Login);
                    var packingAPIKey = new XElement("APIKey", PackingCredentials.APIKey);
                    packingCredentialsElement.Add(packingLogin);
                    packingCredentialsElement.Add(packingAPIKey);
                }

                if (ShippingCredentials != null)
                {
                    var shippingLogin = new XElement("Login", ShippingCredentials.Login);
                    var shippingAPIKey = new XElement("APIKey", ShippingCredentials.APIKey);
                    shippingCredentialsElement.Add(shippingLogin);
                    shippingCredentialsElement.Add(shippingAPIKey);
                }

                elements.Add(new XElement("PackingCredentials", packingCredentialsElement));
                elements.Add(new XElement("ShippingCredentials", shippingCredentialsElement));
                
                return string.Join("", elements);
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    PackingCredentials = null;
                    ShippingCredentials = null;
                }
                else
                {
                    XElement rootElement = XElement.Parse($"<root>{value}</root>");

                    PackingCredentials = new PackingCredentials();
                    ShippingCredentials = new ShippingCredentials();
                    
                    PackingCredentials.Login = rootElement.Elements("PackingCredentials").Elements("Login").ElementAt(0).Value;
                    PackingCredentials.APIKey = rootElement.Elements("PackingCredentials").Elements("APIKey").ElementAt(0).Value;
                    ShippingCredentials.Login = rootElement.Elements("ShippingCredentials").Elements("Login").ElementAt(0).Value;
                    ShippingCredentials.APIKey = rootElement.Elements("ShippingCredentials").Elements("APIKey").ElementAt(0).Value;
                }
            }
        }
        /// <summary>
        /// The friendly name of the registration
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Currency code of the payments
        /// </summary>
        public string Currency { get; set; }
        /// <summary>
        /// Unit of weight used for shipping
        /// </summary>
        public string UnitOfWeight { get; set; }
        /// <summary>
        /// Unit of length used for shipping
        /// </summary>
        public string UnitOfLength { get; set; }

        //Expecting ShippingContainers in JSON string format.
        public string ShippingContainers {get; set;}

        public IList<ShippingContainer> ShippingContainersAsList {

            get {
                
                if(!string.IsNullOrEmpty(ShippingContainers)){

                    return JsonConvert.DeserializeObject<IList<ShippingContainer>>(ShippingContainers);
                }

                return default(IList<ShippingContainer>); 

            }

        }

    }
    /// <summary>
    /// Archived Registration model.
    /// </summary>
    public class ArchivedRegistration : Registration
    {
        /// <summary>
        /// Date/Time the registration was archived
        /// </summary>
        public DateTime ArchivedDT { get; set; }
    }
}
