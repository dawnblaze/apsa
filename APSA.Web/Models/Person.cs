﻿namespace APSA.Web.Models
{
    /// <summary>
    /// A Person Record in Shipping represents a sender of a receiver of a shipment.
    /// </summary>
    public class Person
    {
        /// <summary>
        /// The person's name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The company name.
        /// </summary>
        public string Company { get; set; }
        /// <summary>
        /// The address to be shipped to or from.
        /// </summary>
        public Address Address { get; set; }
        /// <summary>
        /// The email address of the person.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// The phone number of the person, without any spaces or punctuation (e.g. "8885551212")
        /// </summary>
        public string Phone { get; set; }
        
        /// <summary>
        /// The state where the person is from.
        /// </summary>
        public string State { get; set; }
    }
}
