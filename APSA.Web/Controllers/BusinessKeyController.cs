using System;
using Microsoft.AspNetCore.Mvc;
using APSA.Web.Models;
using APSA.Web.Services;
using System.Threading.Tasks;

namespace APSA.Web.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Controller for Businesskey endpoints
    /// </summary>       
    [Route("apsa/businesskey")]
    public class BusinessKeyController : Controller
    {
        /// <summary>
        /// Adds Business Key
        /// </summary>
        /// <param name="input"> BusinessKeyRequest Object </param>
        /// <returns> TBuinessKeyResponse response </returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] BusinessKeyRequest input)
        {

            Response<BusinessKeyResponse> resp = await BusinessKeyService.Add(input);
            switch (resp.State)
            {
                case ResponseState.Exception:
                    return StatusCode(500, resp.Exception);
                case ResponseState.Error:
                    return BadRequest(resp.MessageText);
                default:
                    return Ok(resp.ResponseObject);
            }
        }

        /// <summary>
        /// deletes/removes Business Key
        /// </summary>
        /// <param name="id"> BusinessKey key(guid) </param>
        /// <returns> TBuinessKeyResponse response </returns>

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            Response<DeleteResponse> resp = await BusinessKeyService.Delete(id);
            
            switch (resp.State)
            {
                case ResponseState.Exception:
                    return StatusCode(500, resp.Exception);
                case ResponseState.Error:
                    return BadRequest(resp.MessageText);
                default:
                    return Ok(resp.ResponseObject);

            }
        }

    }

}
