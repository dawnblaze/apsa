﻿using System;
using APSA.Web.Authentication;
using APSA.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace APSA.Web.Controllers
{
    public class APSAController : Controller
    {
        private Registration _registration;

        public Registration Registration
        {
            get
            {
                if (_registration != null) return _registration;

                //See if calls are coming from within an iframe with a "reg" querystring for persisting the session...
                string headerReferer = Request.Headers["Referer"];
                if (!string.IsNullOrEmpty(headerReferer))
                {
                    try
                    {
                        Uri myUri = new Uri(headerReferer);
                        string reg = System.Web.HttpUtility.ParseQueryString(myUri.Query).Get("reg");
                        if (!string.IsNullOrEmpty(reg))
                        {
                            byte[] data = Convert.FromBase64String(reg);
                            string base64DecodedRegistration = System.Text.Encoding.UTF8.GetString(data);

                            Registration decodedRegObject = Newtonsoft.Json.JsonConvert.DeserializeObject<Registration>(base64DecodedRegistration);
                            if (decodedRegObject != null)
                                _registration = decodedRegObject;
                        }
                    }
                    catch
                    {
                        //do nothing...
                    }
                }

                return _registration ?? (_registration = APSAAuthHelper.CreateRegistration(User));
            }
        }
    }
}
