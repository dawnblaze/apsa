﻿using System;
using System.Threading.Tasks;
using APSA.Web.Authentication;
using APSA.Web.Models;
using APSA.Web.Repositories;
using APSA.Web.Services;
using APSA.Web.Models.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace APSA.Web.Controllers
{
    [Route("apsa/pack")]
    [Authorize(AuthenticationSchemes = APSAAuthOptions.DefaultScheme)]
    public class PackingController : APSAController
    {
        private readonly IRequestedImagesRepository _requestedImagesRepository;

        public PackingController(IRequestedImagesRepository requestedImagesRepository)
        {
            _requestedImagesRepository = requestedImagesRepository;
        }

        [HttpDelete]
        [Route("deleteoldrequestedimages")]
        public async Task<IActionResult> DeleteExpiredRequestedImages()
        {
            DeleteExpiredRequestedImagesResponse response = new DeleteExpiredRequestedImagesResponse();

            try
            {
                if(!Equals(_requestedImagesRepository, null))
                {
                    response = await _requestedImagesRepository.DeleteExpiredImagesAsync();
                }


            }catch(Exception ex)
            {
                response.ErrorMessage += ex.Message;
               
            }
             
            //Temporary return type. Will use more detailed response soon.
            return Ok(response);

        }

        [HttpPost]
        public async Task<IActionResult> Post(
            [FromBody] PackShipRequest packShipRequest
            , [FromQuery] bool allowVertical = true
            , [FromQuery] bool includeItemImage = false
            , [FromQuery] bool includeStepByStepImage = false
            , [FromQuery] bool includeGroupImage = false
            )
        {
            Response<PackingPlan> resp = await PackingService.MultiBinPacking(Registration, packShipRequest, allowVertical, includeItemImage, includeStepByStepImage, includeGroupImage, 2);

            switch (resp.State)
            {
                case ResponseState.Exception:
                    return StatusCode(500, resp.Exception);
                case ResponseState.Error:
                    return BadRequest(resp.MessageText);
                default:
                    return Ok(resp.ResponseObject);
            }
        }
    }
}
