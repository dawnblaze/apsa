﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APSA.Web.Models;
using APSA.Web.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace APSA.Web.Controllers
{
    [Produces("application/json")]
    [Route("apsa/Diagnostics")]
    public class DiagnosticsController : Controller
    {
        private IDiagnosticsService _diagnosticsService;

        public DiagnosticsController(IDiagnosticsService diagnosticsService)
        {
            this._diagnosticsService = diagnosticsService;
        }

        [HttpGet]
        [Route("productversiondetails")]
        public ActionResult GetProductVersionDetails()
        {

            DiagnosticsResponse response = new DiagnosticsResponse();

            response.BuildVersion = this._diagnosticsService.GetBuildVersion();

            return Ok(response);


        }
    }
}