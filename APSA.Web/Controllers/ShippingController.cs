﻿using System.Collections.Generic;
using System.Threading.Tasks;
using APSA.Web.Authentication;
using APSA.Web.Libraries.XPS.Models.Responses;
using APSA.Web.Models;
using APSA.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace APSA.Web.Controllers
{
    [Route("apsa/ship")]
    [Authorize(AuthenticationSchemes = APSAAuthOptions.DefaultScheme)]
    public class ShippingController : APSAController
    {
        /// <summary>
        /// Request a quote on shipping.
        /// </summary>
        /// <param name="packShipRequest"></param>
        /// <returns></returns>
        [Route("quote")]
        [HttpPost]
        public async Task<IActionResult> QuoteShipment([FromBody] PackShipRequest packShipRequest)
        {
            Response<ShippingQuote> resp = await ShippingService.QuoteShipment(Registration, packShipRequest);

            switch (resp.State)
            {
                case ResponseState.Exception:
                    return StatusCode(500, resp.Exception);
                case ResponseState.Error:
                    return BadRequest(resp.MessageText);
                default:
                    return Ok(resp.ResponseObject);
            }
        }

        /// <summary>
        /// Book a shipment with an external carrier.
        /// </summary>
        /// <param name="packShipRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> BookShipment([FromBody] PackShipRequest packShipRequest)
        {
            Response<ShippingPlan> resp = await ShippingService.BookShipment(Registration, packShipRequest);

            switch (resp.State)
            {
                case ResponseState.Exception:
                    return StatusCode(500, resp.Exception);
                case ResponseState.Error:
                    return BadRequest(resp.MessageText);
                default:
                    return Ok(resp.ResponseObject);
            }
        }

        /// <summary>
        /// HTTP DELETE Method -
        /// Attempts to void/cancel the referenced Shipment
        /// </summary>
        /// <param name="referenceId">Reference ID</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> VoidShipment(int referenceId)
        {
            Response<DeleteResponse> resp = await ShippingService.VoidShipment(Registration, referenceId);

            switch (resp.State)
            {
                case ResponseState.Exception:
                    return StatusCode(500, resp.Exception);
                case ResponseState.Error:
                    {
                        if (resp.Message == ResponseMessage.MiscError)
                            return NotFound(resp.MessageText);
                        return BadRequest(resp.MessageText);
                    }
                default:
                    return Ok(resp.ResponseObject);
            }
        }

        /// <summary>
        /// Attemps to adjust a shipment with an external carrier
        /// </summary>
        /// <param name="refid">Contains a collection of items to be shipped</param>
        /// <param name="packShipRequest">Contains a collection of items to be shipped</param>
        /// <returns></returns>
        [HttpPut]
        [Route("{refid}")]
        public async Task<IActionResult> UpdateShipment(int refid, [FromBody] PackShipRequest packShipRequest)
        {
            Response<ShippingPlan> resp = await ShippingService.UpdateShipment(Registration, refid, packShipRequest);

            switch (resp.State)
            {
                case ResponseState.Exception:
                    return StatusCode(500, resp.Exception);
                case ResponseState.Error:
                    return NotFound(resp.MessageText);
                default:
                    return Ok(resp.ResponseObject);
            }
        }


        /// <summary>
        /// Retrieves a list of Carriers available.
        /// If IncludeServices = true, includes the list of services and containers for each carrier.
        /// </summary>
        /// <param name="includeServices"></param>
        /// <returns></returns>

        [Route("carrier")]
        [HttpGet]
        public async Task<IActionResult> GetCarriers([FromQuery] bool includeServices)
        {
            Response<List<XPSCarrier>> resp = await ShippingService.GetCarriers(Registration, includeServices);

            switch (resp.State)
            {
                case ResponseState.Exception:
                    return StatusCode(500, resp.Exception);
                case ResponseState.Error:
                    return BadRequest(resp.MessageText);
                default:
                    return Ok(resp.ResponseObject);
            }
        }

        /// <summary>
        /// Retrieves a list of Services for each carrier.
        /// </summary>
        /// <returns></returns>
        [Route("service")]
        [HttpGet]
        public async Task<IActionResult> GetQuotingOptions()
        {
            Response<List<XPSService>> resp = await ShippingService.GetServices(Registration);

            switch (resp.State)
            {
                case ResponseState.Exception:
                    return StatusCode(500, resp.Exception);
                case ResponseState.Error:
                    return BadRequest(resp.MessageText);
                default:
                    return Ok(resp.ResponseObject);

            }
        }
    }
}