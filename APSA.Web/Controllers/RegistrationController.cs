﻿using System;
using System.Threading.Tasks;
using APSA.Web.Models;
using APSA.Web.Services;
using Microsoft.AspNetCore.Mvc;

namespace APSA.Web.Controllers
{
    /// <inheritdoc />
    /// <summary>
    /// Controller for registration endpoints
    /// </summary>
    [Route("apsa/registration")]
    public class RegistrationController : Controller
    {
        /// <summary>
        /// HTTP GET Method - 
        /// Checks if a registration exists and is not expired.
        /// </summary>
        /// <param name="id">The ID of the Registration</param>
        /// <param name="renew">When true, if a valid registration if found, it will be renewed.</param>
        /// <returns>
        /// OK
        /// {
        ///     "response": true/false
        /// }
        /// </returns>
        /// <example>
        /// GET ../apsa/registration/12345678-1234-1234-1234-123456789012?renew=true
        /// </example>
        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(Guid id, [FromQuery] bool? renew = null)
        {
            Response resp = await RegistrationService.Lookup(id, renew.GetValueOrDefault(false));

            switch (resp.State)
            {
                case ResponseState.Exception:
                    return StatusCode(500, resp.Exception);
                default:
                    return Ok(new { response = resp.WasSuccess });
            }
        }

        /// <summary>
        /// HTTP POST Method - 
        /// Adds a new registration.
        /// </summary>
        /// <param name="input">Registration Record object containing information for the new registration</param>
        /// <returns>
        /// If successful:
        /// OK
        /// {
        ///     "registrationid": "{xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}"
        /// }
        /// 
        /// If failed:
        /// BadRequest
        /// </returns>
        /// <example>
        /// POST ../apsa/registration
        /// </example>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ClientRegistrationRecord input)
        {
            Response<RegistrationResponse> resp = await RegistrationService.Add(input);

            switch (resp.State)
            {
                case ResponseState.Exception:
                    return StatusCode(500, resp.Exception);
                case ResponseState.Error:
                    return BadRequest(resp.MessageText);
                default:
                    return Ok(resp.ResponseObject);
            }
        }

        /// <summary>
        /// Endpoint for updating registration information
        /// </summary>
        /// <param name="id">Registration ID of the existing registration information that is to be updated</param>
        /// <param name="input">Registration Information that will replace the existing registration information</param>
        /// <returns>
        /// OK
        /// {
        ///     "response": true/false
        /// }
        /// </returns>
        /// <example>
        /// GET ../apsa/registration/12345678-1234-1234-1234-123456789012
        /// </example>
        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] ClientRegistrationRecord input)
        {
            Response<RegistrationResponse> resp = await RegistrationService.Update(id, input);

            switch (resp.State)
            {
                case ResponseState.Exception:
                    return StatusCode(500, resp.Exception);
                case ResponseState.Error:
                    return BadRequest(resp.MessageText);
                default:
                    return Ok(resp.ResponseObject);
            }
        }

        /// <summary>
        /// HTTP DELETE Method - 
        /// Deletes a registration.
        /// </summary>
        /// <param name="id">Registration ID to be deleted</param>
        /// <returns>
        /// OK
        /// </returns>
        /// <example>
        /// DELETE ../apsa/registration/12345678-1234-1234-1234-123456789012
        /// </example>
        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            Response resp = await RegistrationService.Delete(id);

            switch (resp.State)
            {
                case ResponseState.Exception:
                    return StatusCode(500, resp.Exception);
                case ResponseState.Error:
                    return BadRequest(resp.MessageText);
                default:
                    return Ok();
            }
        }

        /// <summary>
        /// HTTP POST Method - 
        /// Cleans up any expired registrations.
        /// </summary>
        /// <returns>
        /// OK
        /// </returns>
        /// <example>
        /// POST ../ape/registration/clean
        /// </example>
        [HttpPost]
        [Route("clean")]
        public async Task<IActionResult> Clean()
        {
            Response resp = await RegistrationService.Clean();

            switch (resp.State)
            {
                case ResponseState.Exception:
                    return StatusCode(500, resp.Exception);
                case ResponseState.Error:
                    return BadRequest(resp.MessageText);
                default:
                    return Ok();
            }
        }

    }
}
