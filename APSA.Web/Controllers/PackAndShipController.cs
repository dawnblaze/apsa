﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using APSA.Web.Authentication;
using APSA.Web.Models;
using APSA.Web.Models.PackAndShip;
using APSA.Web.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace APSA.Web.Controllers
{
    [Produces("application/json")]
    [Route("apsa/packandship")]
    [Authorize(AuthenticationSchemes = APSAAuthOptions.DefaultScheme)]
    public class PackAndShipController : APSAController
    {
        private IPackAndShipService _packAndShipService;

        public PackAndShipController(IPackAndShipService packAndShipService)
        {
            _packAndShipService = packAndShipService;
        }

        [HttpPost]
        [Route("quote")]
        public async Task<IActionResult> Quote([FromBody] Models.PackShipRequest packShipRequestRecord
            , [FromQuery] bool allowVertical = true
            , [FromQuery] bool includeItemImage = false
            , [FromQuery] bool includeStepByStepImage = false
            , [FromQuery] bool includeGroupImage = false)
        {
            try
            {


                //The result must be of type ShippingQuote.

                Response<ShippingQuote> result = await this._packAndShipService.GetShippingQuoteAsync(new PackAndShipInputModel(Registration, packShipRequestRecord, allowVertical, includeItemImage, includeStepByStepImage, includeGroupImage));

                switch (result.State)
                {
                    case ResponseState.Exception:
                        return StatusCode(500, result.Exception);
                    case ResponseState.Error:
                        return BadRequest(result.MessageText);
                    default:
                        return Ok(result.ResponseObject);
                }

            }
            catch(Exception ex)
            {

                return StatusCode(500, ex);

            }

            

        }

        [HttpPost]
        public async Task<IActionResult> OnPost([FromBody] Models.PackShipRequest packShipRequestRecord
            , [FromQuery] bool allowVertical = true
            , [FromQuery] bool includeItemImage = false
            , [FromQuery] bool includeStepByStepImage = false
            , [FromQuery] bool includeGroupImage = false)
        {

            var result = new ShippingPlan(); //new ShippingPlan();
            Response<ShippingPlan> resp;

            try
            {
                result = await this._packAndShipService.GetShippingPlanAsync(new PackAndShipInputModel(Registration, packShipRequestRecord, allowVertical, includeItemImage, includeStepByStepImage, includeGroupImage));

                resp = new Response<ShippingPlan>() { ResponseObject = result };


                switch (resp.State)
                {
                    case ResponseState.Exception:
                        return StatusCode(500, resp.Exception);
                    case ResponseState.Error:
                        return BadRequest(resp.MessageText);
                    default:
                        return Ok(resp.ResponseObject);
                }

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
            
            

        }
        
    }
}