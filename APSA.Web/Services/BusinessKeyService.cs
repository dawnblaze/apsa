﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using APSA.Web.Models;

namespace APSA.Web.Services
{
    public class BusinessKeyService
    {
        private class SQL
        {
            public const string InsertBusiness = @"
INSERT INTO [Business.Data]
(
    [BusinessKey]
   ,[IsActive]
   ,[AppType]
   ,[BusinessName]
   ,[AssociationID]
   ,[BusinessID]
)
VALUES
(
    @BusinessKey
  , @IsActive
  , @AppType
  , @BusinessName
   ,@AssociationID
   ,@BusinessID
)
";
            public const string DeleteBusiness = @"
DELETE FROM [Business.Data]
WHERE BusinessKey = @BusinessKey
";
            public const string LoadBusinessID = @"
SELECT ID
FROM [Business.Data]
WHERE BusinessKey = @BusinessKey
";
        }

        /// <summary>
        /// Adds a new business key registration.
        /// </summary>
        /// <param name="input">BusinessKeyRequest object containing information for the new regitration</param>
        /// <returns>
        /// <see cref="Response{TBusinessKeyResponse}"/>
        /// {
        ///   bool Success,
        ///   string ErrorMessage,
        ///   Guid RegistrationID
        /// }
        /// </returns>
        public static async Task<Response<BusinessKeyResponse>> Add(BusinessKeyRequest input)
        {
            LoggingService.Debug("BusinessKeyService", "Add", notes: input);

            Response<BusinessKeyResponse> result;

            try
            {
                if (input == null)
                    result = Response<BusinessKeyResponse>.Error(ResponseMessage.InvalidRequest);

                else if (!AppTypeHelper.TryFromName(input.AppType, out AppType appType))
                    result = Response<BusinessKeyResponse>.Error(ResponseMessage.InvalidAppType);

                else if (string.IsNullOrWhiteSpace(input.BusinessName))
                    result = Response<BusinessKeyResponse>.Error(ResponseMessage.BusinessNameRequired);

                else if (input.BID <= 0)
                    result = Response<BusinessKeyResponse>.Error(ResponseMessage.BIDRequired);

                else
                {
                    Guid businessKey = Guid.NewGuid();

                    object mapping = new
                    {
                        BusinessKey = businessKey,
                        IsActive = true,
                        AppType = appType,
                        input.BusinessName,
                        input.AssociationID,
                        BusinessID = input.BID
                    };

                    DatabaseResult sqlResult = await DatabaseService.ExecQuery(-1, SQL.InsertBusiness, mapping);

                    result = sqlResult.Success
                        ? Response<BusinessKeyResponse>.Success(new BusinessKeyResponse {BusinessKey = businessKey})
                        : Response<BusinessKeyResponse>.Error(sqlResult.Exception);
                }
            }
            catch (Exception err)
            {
                result = Response<BusinessKeyResponse>.Error(err);
            }

            result.LogResult("BusinessKeyService", "Add");

            return result;
        }

        /// <summary>
        /// Deletes a business key
        /// </summary>
        /// <param name="key">The key of the business</param>
        public static async Task<Response<DeleteResponse>> Delete(Guid key)
        {
            LoggingService.Debug("BusinessKeyService", "Delete", notes: new { key });
            Response<DeleteResponse> result;

            try
            {
                object mapping = new
                {
                    BusinessKey = key
                };

                DatabaseResult sqlResult = await DatabaseService.ExecQuery(-1, SQL.DeleteBusiness, mapping);

                result = sqlResult.Success
                    ? Response<DeleteResponse>.Success(new DeleteResponse {NotFound = (sqlResult.RowsAffected == 0)})
                    : Response<DeleteResponse>.Error(sqlResult.Exception);
            }
            catch (Exception err)
            {
                result = Response<DeleteResponse>.Error(err);
            }

            result.LogResult("BusinessKeyService", "Delete");

            return result;
        }
        /// <summary>
        /// Gets a Business's ID from its Key
        /// </summary>
        /// <param name="key">The key of the business</param>
        public static async Task<short?> GetBusinessID(Guid key)
        {
            LoggingService.Debug("BusinessKeyService", "GetBusinessID", notes: new { key });
            short? result;

            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>
                {
                    {"BusinessKey", key }
                };

                result = await DatabaseService.Load<short?>(SQL.LoadBusinessID, parameters);
            }

            catch (Exception err)
            {
                LoggingService.Error("BusinessKeyService", "GetBusinessID", exception: err);
                result = null;
            }

            if (result.HasValue)
                LoggingService.Debug("BusinessKeyService", "GetBusinessID Successful", notes: new { value = result.Value });
            else
                LoggingService.Debug("BusinessKeyService", "GetBusinessID Failed");

            return result;
        }
    }
}
