﻿using APSA.Web.Properties;
using APSA.Web.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace APSA.Web.Services
{
    public class DiagnosticsService : IDiagnosticsService
    {
        public DiagnosticsService() { }

        public string GetBuildVersion()
        {
            string buildVersion = string.Empty;

            Assembly assembly = typeof(AssemblyInfo).Assembly;

            var buildVersionAttribute = assembly.GetCustomAttributes(typeof(BuildVersionAttribute), false).FirstOrDefault();

            if (buildVersionAttribute is BuildVersionAttribute)
            {
                var attrib = buildVersionAttribute as BuildVersionAttribute;
                buildVersion = attrib.VersionString;
            }

            return buildVersion;
        }
    }
}
