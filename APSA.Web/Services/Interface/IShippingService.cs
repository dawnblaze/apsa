﻿using APSA.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Services.Interface
{
    public interface IShippingService
    {
        Task<Response<ShippingQuote>> QuoteShipment(Registration registration, PackingPlan packingPlan);
    }
}
