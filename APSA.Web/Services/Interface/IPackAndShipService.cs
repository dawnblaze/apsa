﻿using APSA.Web.Models;
using APSA.Web.Models.PackAndShip;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace APSA.Web.Services.Interface
{
    public interface IPackAndShipService
    {
        
        Task<Response<ShippingQuote>> GetShippingQuoteAsync(PackAndShipInputModel packAndShipInputModel);
        Task<Models.ShippingPlan> GetShippingPlanAsync(PackAndShipInputModel packAndShipInputModel);
    }
}
