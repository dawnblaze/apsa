﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Services.Interface
{
    public interface IDiagnosticsService
    {

        string GetBuildVersion();
    }
}
