﻿using System;
using Newtonsoft.Json;
using Serilog;
using Serilog.Sinks.MSSqlServer;
using Serilog.Events;
using Serilog.Core.Enrichers;
using System.Collections.ObjectModel;
using Serilog.Models;
using Serilog.Core;
using Microsoft.Extensions.Configuration;
using System.IO;
using Serilog.Debugging;

namespace APSA.Web.Services
{
    /// <summary>
    /// Service for adding entries to the log
    /// </summary>
    public static class LoggingService
    {
        private class APSAEnricher : ILogEventEnricher
        {
            public void Enrich(LogEvent logEvent, ILogEventPropertyFactory lepf)
            {
                logEvent.AddPropertyIfAbsent(
                  lepf.CreateProperty("TimeStamp", logEvent.Timestamp.UtcDateTime));
                logEvent.AddPropertyIfAbsent(
                  lepf.CreateProperty("Level", (short)logEvent.Level));
            }
        }

        private static ILogger log;

        /// <summary>
        /// Initializes SerilLog configuration
        /// </summary>
        /// <param name="level">Logging Level</param>
        /// <param name="connString">Log's Connection string</param>
        /// <param name="debugLog">Whether you are debugging the logger</param>
        public static void Init(LogEventLevel level, string connString, bool debugLog = false)
        {
            if (debugLog)
            {
                string logPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SerilLog");
                if (!Directory.Exists(logPath))
                    Directory.CreateDirectory(logPath);

                StreamWriter logLog = File.CreateText(Path.Combine(logPath, $"SerilogDebug -{DateTime.Now.Ticks}.txt"));
                SelfLog.Enable(logLog);
            }

            ColumnOptions columnOptions = new ColumnOptions();
            columnOptions.Store.Clear();
            columnOptions.Store.Add(StandardColumn.Message);
            columnOptions.Store.Add(StandardColumn.TimeStamp);
            columnOptions.Store.Add(StandardColumn.Exception);

            columnOptions.AdditionalDataColumns = new Collection<DataColumn>
            {
                new DataColumn() { ColumnName = "BID", DataType = typeof(short) },
                new DataColumn() { ColumnName = "Level", DataType = typeof(short) },
                new DataColumn() { ColumnName = "Category", DataType = typeof(string), MaxLength = 25 },
                new DataColumn() { ColumnName = "Notes", DataType = typeof(string) },
            };

            columnOptions.Properties.ExcludeAdditionalProperties = true;

            log = new LoggerConfiguration()
                    .MinimumLevel.Is(level)
                    .Enrich.With(new APSAEnricher())
                    .WriteTo.MSSqlServer(connString, "Log.Data", columnOptions: columnOptions)
                    .CreateLogger();

            Log.Logger = log;
        }
        /// <summary>
        /// Initializes SerilLog configuration
        /// </summary>
        /// <param name="configuration">Application Configuration Settings</param>
        public static void Init(IConfiguration configuration)
        {
            string debugSerilLog = configuration["DebugSerilLog"];
            if (!bool.TryParse(debugSerilLog, out bool doDebug))
                doDebug = false;

            if (!Enum.TryParse<LogEventLevel>(configuration["LoggingLevel"], true, out LogEventLevel level))
                level = LogEventLevel.Information;

            string connString = configuration.GetConnectionString("APSA");

            if (!string.IsNullOrWhiteSpace(connString))
                Init(level, connString, doDebug);
        }
        /// <summary>
        /// Adds an entry to the Log
        /// </summary>
        /// <param name="level">The level of the entry.</param>
        /// <param name="category">Short category name of the entry</param>
        /// <param name="message">Log entry message</param>
        /// <param name="bID">Optional Business ID</param>
        /// <param name="notesObj">Optional Notes to be logged</param>
        /// <param name="exception">Optional Exception to be logged</param>
        private static void AddToLog(LogEventLevel level, string category, string message, short? bID, object notesObj, Exception exception)
        {
            if (log == null)
                return;

            string notes;
            if (notesObj == null)
                notes = null;
            else if (notesObj is string)
                notes = (string)notesObj;
            else
                notes = JsonConvert.SerializeObject(notesObj);

            ILogger logger = log.ForContext(new[] {
              new PropertyEnricher("BID", bID ?? -1),
              new PropertyEnricher("Category", category),
              new PropertyEnricher("Notes", notes),
            });

            logger.Write(level, exception, message);
        }

        /// <summary>
        /// Adds a fatal error entry to the Log
        /// </summary>
        /// <param name="category">Short category name of the entry</param>
        /// <param name="message">Log entry message</param>
        /// <param name="bID">Optional Business ID</param>
        /// <param name="notes">Optional Notes to be logged</param>
        /// <param name="exception">Optional Exception to be logged</param>
        public static void Fatal(string category, string message, short? bID = null, object notes = null, Exception exception = null)
        {
            AddToLog(LogEventLevel.Fatal, category, message, bID, notes, exception);
        }
        /// <summary>
        /// Adds an error entry to the Log
        /// </summary>
        /// <param name="category">Short category name of the entry</param>
        /// <param name="message">Log entry message</param>
        /// <param name="bID">Optional Business ID</param>
        /// <param name="notes">Optional Notes to be logged</param>
        /// <param name="exception">Optional Exception to be logged</param>
        public static void Error(string category, string message, short? bID = null, object notes = null, Exception exception = null)
        {
            AddToLog(LogEventLevel.Error, category, message, bID, notes, exception);
        }
        /// <summary>
        /// Adds a warning entry to the Log
        /// </summary>
        /// <param name="category">Short category name of the entry</param>
        /// <param name="message">Log entry message</param>
        /// <param name="bID">Optional Business ID</param>
        /// <param name="notes">Optional Notes to be logged</param>
        /// <param name="exception">Optional Exception to be logged</param>
        public static void Warning(string category, string message, short? bID = null, object notes = null, Exception exception = null)
        {
            AddToLog(LogEventLevel.Warning, category, message, bID, notes, exception);
        }
        /// <summary>
        /// Adds an information entry to the Log
        /// </summary>
        /// <param name="category">Short category name of the entry</param>
        /// <param name="message">Log entry message</param>
        /// <param name="bID">Optional Business ID</param>
        /// <param name="notes">Optional Notes to be logged</param>
        /// <param name="exception">Optional Exception to be logged</param>
        public static void Information(string category, string message, short? bID = null, object notes = null, Exception exception = null)
        {
            AddToLog(LogEventLevel.Information, category, message, bID, notes, exception);
        }
        /// <summary>
        /// Adds a debug entry to the Log
        /// </summary>
        /// <param name="category">Short category name of the entry</param>
        /// <param name="message">Log entry message</param>
        /// <param name="bID">Optional Business ID</param>
        /// <param name="notes">Optional Notes to be logged</param>
        /// <param name="exception">Optional Exception to be logged</param>
        public static void Debug(string category, string message, short? bID = null, object notes = null, Exception exception = null)
        {
            AddToLog(LogEventLevel.Debug, category, message, bID, notes, exception);
        }
        /// <summary>
        /// Adds a verbose entry to the Log
        /// </summary>
        /// <param name="category">Short category name of the entry</param>
        /// <param name="message">Log entry message</param>
        /// <param name="bID">Optional Business ID</param>
        /// <param name="notes">Optional Notes to be logged</param>
        /// <param name="exception">Optional Exception to be logged</param>
        public static void Verbose(string category, string message, short? bID = null, object notes = null, Exception exception = null)
        {
            AddToLog(LogEventLevel.Verbose, category, message, bID, notes, exception);
        }
    }
}
