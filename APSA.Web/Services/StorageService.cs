﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Extensions.Configuration;
using System.Net;
using System.IO;

namespace APSA.Web.Services
{
    /// <summary>
    /// Service for accessing the azure blob storage
    /// </summary>
    public static class StorageService
    {
        private static readonly string ContainerName = "apsa";
        private static string BaseFilePath => "images";
        private static string TempFilePath => $"{BaseFilePath}/temp";
        private static CloudStorageAccount storageAccount = null;
        private static CloudBlobContainer APSAContainer;

        /// <summary>
        /// Initializes Storage Account
        /// </summary>
        /// <param name="configuration">Application Configuration Settings</param>
        public static void Init(IConfiguration configuration)
        {
            string connectionString = configuration.GetConnectionString("AzureStorageConnection");
            storageAccount = CloudStorageAccount.Parse(connectionString);
            APSAContainer = GetContainer(GetClient());
        }

        /// <summary>
        /// Initializes a Azure Blob Storage client
        /// </summary>
        public static CloudBlobClient GetClient()
        {
            return new CloudBlobClient(storageAccount.BlobStorageUri, storageAccount.Credentials);
        }

        /// <summary>
        /// Get the APSA blob container
        /// </summary>
        /// <param name="client">CloudBlobClient</param>
        public static CloudBlobContainer GetContainer(CloudBlobClient client)
        {
            return client.GetContainerReference(ContainerName);
        }

        /// <summary>
        /// Get the relative path to the request images
        /// </summary>
        /// <param name="date">Current date in server local time.</param>
        /// <param name="guid">Represents a randomly generated GUID</param>
        /// <param name="imageName">Represents the name of the image</param>
        /// <returns>relative path to the request images</returns>
        private static string GetRequestedImageLocation(string date, string guid, string imageName)
        {
            return $"{TempFilePath}/{date}/{guid}/{imageName}";
        }

        /// <summary>
        /// Saves image to azure blob storage
        /// </summary>
        /// <param name="date">The current date in server local time</param>
        /// <param name="guid">Represents a randomly generated GUID</param>
        /// <param name="imageName">Represents the name of the image</param>
        /// <param name="data">Byte array representing the contents of the file</param>
        /// <returns>Path of the stored requested image</returns>
        public static string SaveImageToBlobStorage(string date, string guid, string imageName, string imageURL)
        {
            try
            {
                string requestImageFileLocation = GetRequestedImageLocation(date, guid, imageName);
                CloudBlockBlob cloudBlockBlobRef = APSAContainer.GetBlockBlobReference(requestImageFileLocation);

                using (var client = new WebClient())
                {
                    // Download data.
                    byte[] data = client.DownloadData(imageURL);
                    using (var stream = new MemoryStream(data))
                    {
                        //Upload product asset file to blob storage
                        cloudBlockBlobRef.UploadFromStreamAsync(stream);
                    }
                }

                return cloudBlockBlobRef.Uri.AbsoluteUri;
            }
            catch
            {
                return imageURL;
            }
        }
    }
}