﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using APSA.Web.Models;
using Newtonsoft.Json;

namespace APSA.Web.Services
{
    /// <summary>
    /// Service the handles the adding, removing, and looking up registrations
    /// </summary>
    public class RegistrationService
    {
        private class SQL
        {
            public class StoredProc
            {
                public const string RenewRegistration = "[Registration.Renew]";
                public const string DeleteRegistration = "[Registration.Delete]";
                public const string CleanRegistration = "[Registration.Clean]";
            }

            public const string InsertRegistration = @"
INSERT INTO [Registration.Data]
(
    [RegistrationID]
   ,[BID]
   ,[ExpirationDT]
   ,[AppType]
   ,[IsSandboxed]
   ,[ShippingProcessorType]
   ,[PackingProcessorType]
   ,[Credentials]
   ,[Name]
   ,[Currency]
   ,[UnitOfWeight]
   ,[UnitOfLength]
   ,[ShippingContainers]
)
VALUES
(
    @RegistrationID
   ,@BID
   ,@ExpirationDT
   ,@AppType
   ,@IsSandboxed
   ,@ShippingProcessorType
   ,@PackingProcessorType
   ,@Credentials
   ,@Name
   ,@Currency
   ,@UnitOfWeight
   ,@UnitOfLength
   ,@ShippingContainers
)
";
            public const string LookupRegistration = @"
SELECT 
    RegistrationID
   ,BID
   ,ExpirationDT
   ,AppType
   ,IsSandboxed
   ,ShippingProcessorType
   ,PackingProcessorType
   ,Credentials
   ,Name
   ,Currency
   ,UnitOfWeight
   ,UnitOfLength
   ,ShippingContainers
FROM [Registration.Data]
WHERE RegistrationID = @RegistrationID
";
            public const string UpdateRegistration = @"
UPDATE [dbo].[Registration.Data]
   SET [BID] = @BID
      ,[ExpirationDT] = @ExpirationDT
      ,[AppType] = @AppType
      ,[IsSandboxed] = @IsSandboxed
      ,[ShippingProcessorType] = @ShippingProcessorType
      ,[PackingProcessorType] = @PackingProcessorType
      ,[Credentials] = @Credentials
      ,[Name] = @Name
      ,[Currency] = @Currency
      ,[UnitOfWeight] = @UnitOfWeight
      ,[UnitOfLength] = @UnitOfLength
      ,[ShippingContainers] = @ShippingContainers
WHERE RegistrationID = @RegistrationID
";
            public const string DeleteArchivedRegistration = @"
DELETE 
FROM   [Registration.Archive]
WHERE  RegistrationID = @RegistrationID
";
        }

        /// <summary>
        /// Looks up a registration by ID.
        /// </summary>
        /// <param name="id">The ID of the Registration</param>
        /// <param name="renew">When true, if a valid registration if found, it will be renewed.</param>
        /// <returns>
        /// <see cref="Response"/>
        /// {
        ///   bool Success,
        ///   string ErrorMessage
        /// }
        /// </returns>
        public static async Task<Response> Lookup(Guid id, bool renew)
        {
            LoggingService.Debug("RegistrationService", "Lookup", notes: new { id, renew });

            Response result;

            try
            {
                Registration reg = await FindRegistration(id);

                if (reg == null)
                    result = Response.Error(ResponseMessage.RegistrationNotFound);
                else if (renew)
                {
                    var expDate = DateTime.UtcNow.AddDays(1);
                    var parameters = new Dictionary<string, object>
                    {
                        { "RegistrationID", id },
                        { "ExpirationDT", expDate },
                    };

                    await DatabaseService.ExecStoredProc(reg.BID, SQL.StoredProc.RenewRegistration, parameters);
                    result = Response.Success();
                }
                else // if (reg.ExpirationDT < DateTime.UtcNow)
                    result = Response.Error(ResponseMessage.RegistrationHasExpired);
                
            }
            catch (Exception err)
            {
                result = Response.Error(err);
            }

            result.LogResult("RegistrationService", "Lookup");
            return result;
        }

        /// <summary>
        /// Finds a registration by ID.
        /// </summary>
        /// <param name="id">The ID of the Registration</param>
        /// <returns>
        /// <see cref="Registration"/>
        /// </returns>
        public static async Task<Registration> FindRegistration(Guid id)
        {
            LoggingService.Debug("RegistrationService", "FindRegistration", notes: new { id });

            Registration result;

            try
            {

                var parameters = new Dictionary<string, object>
                {
                    {"RegistrationID", id }
                };
                result = await DatabaseService.Load<Registration>(SQL.LookupRegistration, parameters);
            }
            catch (Exception err)
            {
                LoggingService.Error("RegistrationService", "FindRegistration", exception: err);
                result = null;
            }

            if (result == null)
                LoggingService.Debug("RegistrationService", "FindRegistration Failed", notes: new { id });
            else
                LoggingService.Debug("RegistrationService", "FindRegistration Successful", notes: new { id });

            return result;
        }

        /// <summary>
        /// Adds a new registration.
        /// </summary>
        /// <param name="input">ClientRegistrationRecord object containing information for the new registration</param>
        /// <returns>
        /// <see cref="RegistrationResponse"/>
        /// {
        ///   bool Success,
        ///   string ErrorMessage,
        ///   Guid RegistrationID
        /// }
        /// </returns>
        public static async Task<Response<RegistrationResponse>> Add(ClientRegistrationRecord input)
        {
            LoggingService.Debug("RegistrationService", "Add", notes: new { input });

            Response<RegistrationResponse> result;

            if (input == null)
                result = Response<RegistrationResponse>.Error(ResponseMessage.InvalidRequest);
            else
            {
                try
                {
                    short? bid = await new BusinessKeyRepository().GetBusinessID(input.BusinessKey);
                    if (!bid.HasValue)
                        result = Response<RegistrationResponse>.Error(ResponseMessage.BusinessKeyNotFound);

                    else if (!AppTypeHelper.TryFromName(input.AppType, out AppType appType))
                        result = Response<RegistrationResponse>.Error(ResponseMessage.InvalidAppType);

                    else if (string.IsNullOrWhiteSpace(input.Currency))
                        result = Response<RegistrationResponse>.Error(ResponseMessage.CurrencyRequired);
                    else
                    {
                        var registrationId = Guid.NewGuid();
                        var expDate = DateTime.UtcNow.AddDays(1);

                        var registration = new Registration
                        {
                            RegistrationID = registrationId,
                            BID = bid.Value,
                            ExpirationDT = expDate,
                            AppType = appType,
                            IsSandboxed = input.IsSandboxed,
                            PackingProcessorType = (PackingProcessorType)Enum.Parse(typeof(PackingProcessorType),
                                input.PackingProcessorType),
                            ShippingProcessorType = (ShippingProcessorType) Enum.Parse(typeof(ShippingProcessorType),
                                input.ShippingProcessorType),
                            Name = input.Name,
                            Currency = input.Currency,
                            PackingCredentials = input.PackingCredentials,
                            ShippingCredentials = input.ShippingCredentials,
                            UnitOfLength = input.UnitOfLength,
                            UnitOfWeight = input.UnitOfWeight,
                            ShippingContainers = JsonConvert.SerializeObject(input.Containers)
                            
                        };

                        DatabaseResult sqlResult =
                            await DatabaseService.ExecQuery(bid.Value, SQL.InsertRegistration, registration);

                        result = sqlResult.Success
                            ? Response<RegistrationResponse>.Success(
                                new RegistrationResponse {RegistrationID = registrationId})
                            : Response<RegistrationResponse>.Error(sqlResult.Exception);
                    }
                }
                catch (Exception err)
                {
                    result = Response<RegistrationResponse>.Error(err);
                    
                }
            }

            result.LogResult("RegistrationService", "Add");
            return result;
        }

        /// <summary>
        /// Updates an existing registration.
        /// </summary>
        /// <param name="registrationId">Registration ID of the existing registration information</param>
        /// <param name="input">ClientRegistrationRecord object containing information of an existing registration</param>
        /// <returns>
        /// <see cref="RegistrationResponse"/>
        /// {
        ///   bool Success,
        ///   string ErrorMessage,
        ///   Guid RegistrationID
        /// }
        /// </returns>
        public static async Task<Response<RegistrationResponse>> Update(Guid registrationId, ClientRegistrationRecord input)
        {
            LoggingService.Debug("RegistrationService", "Update", notes: new { input });

            Response<RegistrationResponse> result;

            if (input == null)
                result = Response<RegistrationResponse>.Error(ResponseMessage.InvalidRequest);
            else
            {
                try
                {
                    short? bid = await new BusinessKeyRepository().GetBusinessID(input.BusinessKey);
                    if (!bid.HasValue)
                        result = Response<RegistrationResponse>.Error(ResponseMessage.BusinessKeyNotFound);

                    else if (!AppTypeHelper.TryFromName(input.AppType, out AppType appType))
                        result = Response<RegistrationResponse>.Error(ResponseMessage.InvalidAppType);

                    else if (string.IsNullOrWhiteSpace(input.Currency))
                        result = Response<RegistrationResponse>.Error(ResponseMessage.CurrencyRequired);
                    else
                    {
                        var expDate = input.ExpirationDT;

                        var registration = new Registration
                        {
                            RegistrationID = registrationId,
                            BID = bid.Value,
                            ExpirationDT = expDate,
                            AppType = appType,
                            IsSandboxed = input.IsSandboxed,
                            PackingProcessorType = (PackingProcessorType)Enum.Parse(typeof(PackingProcessorType),
                                input.PackingProcessorType),
                            ShippingProcessorType = (ShippingProcessorType) Enum.Parse(typeof(ShippingProcessorType),
                                input.ShippingProcessorType),
                            Name = input.Name,
                            Currency = input.Currency,
                            PackingCredentials = input.PackingCredentials,
                            ShippingCredentials = input.ShippingCredentials,
                            UnitOfWeight = input.UnitOfWeight,
                            UnitOfLength = input.UnitOfLength,
                            ShippingContainers = JsonConvert.SerializeObject(input.Containers)
                        };

                        DatabaseResult sqlResult =
                            await DatabaseService.ExecQuery(bid.Value, SQL.UpdateRegistration, registration);

                        result = sqlResult.Success
                            ? Response<RegistrationResponse>.Success(
                                new RegistrationResponse {RegistrationID = registrationId})
                            : Response<RegistrationResponse>.Error(sqlResult.Exception);
                    }
                }
                catch (Exception err)
                {
                    result = Response<RegistrationResponse>.Error(err);
                }
            }

            result.LogResult("RegistrationService", "Update");
            return result;
        }
        /// <summary>
        /// Deletes a registration
        /// </summary>
        /// <param name="id">The ID of the Registration</param>
        public static async Task<Response> Delete(Guid id)
        {
            LoggingService.Debug("RegistrationService", "Delete", notes: new { id });

            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                { "RegistrationID", id },
            };
            DatabaseResult sqlResult = await DatabaseService.ExecStoredProc(-1, SQL.StoredProc.DeleteRegistration, parameters);

            var result = sqlResult.Success ? Response.Success() : Response.Error(sqlResult.Exception);

            result.LogResult("RegistrationService", "Delete");

            return result;
        }
        /// <summary>
        /// Deletes a registration from the archive
        /// </summary>
        /// <param name="id">The ID of the Registration</param>
        public static async Task<Response> DeleteArchive(Guid id)
        {
            LoggingService.Debug("RegistrationService", "DeleteArchive", notes: new { id });

            Dictionary<string, object> parameters = new Dictionary<string, object>
            {
                { "RegistrationID", id },
            };
            DatabaseResult sqlResult = await DatabaseService.ExecQuery(-1, SQL.DeleteArchivedRegistration, parameters);

            var result = sqlResult.Success ? Response.Success() : Response.Error(sqlResult.Exception);

            result.LogResult("RegistrationService", "DeleteArchive");

            return result;
        }
        /// <summary>
        /// Removes all expired a registration
        /// </summary>
        public static async Task<Response> Clean()
        {
            LoggingService.Debug("RegistrationService", "Clean");

            DatabaseResult sqlResult = await DatabaseService.ExecStoredProc(-1, SQL.StoredProc.CleanRegistration);

            var result = sqlResult.Success ? Response.Success() : Response.Error(sqlResult.Exception);

            result.LogResult("RegistrationService", "Clean");

            return result;
        }
    }
}
