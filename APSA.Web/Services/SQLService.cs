﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Services
{
    public class SQLService
    {
        private static string _contentRootPath;

        /// <summary>
        /// Sets the content root path.
        /// </summary>
        /// <param name="contentRootPath">Root path of the project.</param>
        public static void SetContentRootPath(string contentRootPath)
        {
            _contentRootPath = contentRootPath;
        }

        public class SQLMigrationResults
        {
            public bool Success { get; set; }
            public Exception Error { get; set; }
        }

        public static async Task<SQLMigrationResults> RunMigration()
        {
            string path = Path.Combine(_contentRootPath, "SQL");

            if (!Directory.Exists(path))
                return new SQLMigrationResults
                {
                    Success = false,
                    Error = new Exception("No SQL files found.")
                };

            IEnumerable<string> sqlFiles = Directory.GetFiles(path, "*.SQL", SearchOption.AllDirectories).OrderBy(f => f);

            bool success = true;
            List<Exception> errors = new List<Exception>();

            foreach (string sqlFile in sqlFiles)
            {
                try
                {
                    DatabaseResult databaseResult = await DatabaseService.ExecQueryFromFile(-1, sqlFile, null);

                    if (databaseResult.Success) continue;
                    success = false;
                    errors.Add(databaseResult.Exception);
                }
                catch (Exception err)
                {
                    success = false;
                    errors.Add(err);
                }
            }

            if (success)
                return new SQLMigrationResults
                {
                    Success = true,
                    Error = null
                };
            var error = new AggregateException("Error recreating SQL Objects", errors);
            LoggingService.Error("SQL", "Error recreating SQL Objects", exception: error);

            return new SQLMigrationResults
            {
                Success = true,
                Error = error
            };
        }
    }
}
