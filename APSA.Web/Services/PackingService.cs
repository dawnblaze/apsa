﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using APSA.Web.Libraries.BinPacking;
using APSA.Web.Models;
using Newtonsoft.Json;

namespace APSA.Web.Services
{
    public class PackingService
    {
       

        public static async Task<Response<PackingPlan>> MultiBinPacking(
            Registration registration
            , PackShipRequest packShipRequest
            , bool allowVertical
            , bool includeItemImage
            , bool includeStepByStepImage
            , bool includeGroupImage
            , int packingType)
        {
            LoggingService.Debug("PackingService", "MultiBinPacking",
                notes: new
                {
                    registration,
                    packShipRequest,
                    allowVertical,
                    includeItemImage,
                    includeStepByStepImage,
                    includeGroupImage,
                    packingType
                });

            Response<PackingPlan> result;

            if (packShipRequest == null)
                result = Response<PackingPlan>.Error(ResponseMessage.InvalidRequest);
            else
            {
                try
                {
                    var packingClient = new BinPackingRestClient();

                    var shippingContainers = (List<ShippingContainer>)registration?.ShippingContainersAsList;

                    PackingPlan packingPlan = await packingClient.BinPacking(registration.PackingCredentials, shippingContainers, packShipRequest, allowVertical, includeItemImage,
                        includeStepByStepImage, includeGroupImage, packingType);

                    string guid = Guid.NewGuid().ToString();
                    string date = DateTime.Now.ToString("yyMMdd");

                    if (packingPlan.StepByStepImageURL != null) {
                        packingPlan.StepByStepImageURL = SaveRequestedImage(date, guid, packingPlan.StepByStepImageURL);
                    }

                    if (packingPlan.GroupImageURL != null) {
                        packingPlan.GroupImageURL = SaveRequestedImage(date, guid, packingPlan.GroupImageURL);
                    }

                    foreach (var container in packingPlan.Containers)
                    {
                        container.Items.ForEach(item =>
                        {
                            if (item.ItemImageURL != "")
                            {
                                item.ItemImageURL = SaveRequestedImage(date, guid, item.ItemImageURL);
                            }
                        });
                    }

                    result = Response<PackingPlan>.Success(packingPlan);

                }
                catch (Exception err)
                {
                    result = Response<PackingPlan>.Error(err);

                }
            }

            result.LogResult("PackingService", "MultiBinPacking");
            return result;
        }

        public static string SaveRequestedImage(string date, string guid, string imageUrl)
        {
            string[] imageNameSplit = imageUrl.Split('/');
            string imageName = imageNameSplit[imageNameSplit.Length - 1];

            string absoluteUri = StorageService.SaveImageToBlobStorage(date, guid, imageName, imageUrl);
            return absoluteUri;
        }
    }
}
