﻿using APSA.Web.Models;
using APSA.Web.Models.PackAndShip;
using APSA.Web.Services.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace APSA.Web.Services
{
    public class PackAndShipService : IPackAndShipService
    {
        private IShippingService _shippingService;

        //Expecting shippingService instance to be injected via IOC
        public PackAndShipService(IShippingService shippingService)
        {
            _shippingService = shippingService;
        }

        public async Task<Response<ShippingQuote>> GetShippingQuoteAsync(PackAndShipInputModel packAndShipInputModel)
        {
            Registration registration = packAndShipInputModel.Registration;
            Models.PackShipRequest packShipRequest = packAndShipInputModel.PackShipRequestRecord;
            bool allowVertical = packAndShipInputModel.AllowVertical;
            bool includeItemImage = packAndShipInputModel.IncludeItemImage;
            bool includeStepByStepImage = packAndShipInputModel.IncludeStepByStepImage;
            bool includeGroupImage = packAndShipInputModel.IncludeGroupImage;
            int packingType = packAndShipInputModel.PackingType;


            Response<ShippingQuote> shippingQuote = new Response<ShippingQuote>();

            //Do not proceed if registration object is null.
             if(registration != null)
            {
                
                Response<PackingPlan> packingPlanResponse = await PackingService.MultiBinPacking(registration, packShipRequest, allowVertical, includeItemImage, includeStepByStepImage, includeGroupImage, packingType);

                if(!packingPlanResponse.WasSuccess)
                {
                    var shippingQuoteErrorResponse = new Response<ShippingQuote>
                    {
                        State = packingPlanResponse.State,
                        Message = packingPlanResponse.Message,
                        Exception = packingPlanResponse.Exception,
                        ErrorText = packingPlanResponse.ErrorText,
                        ResponseObject = packingPlanResponse.ResponseObject as ShippingQuote
                    };

                    return shippingQuoteErrorResponse;
                }

                PackingPlan packingPlan = packingPlanResponse.ResponseObject;

                if (packingPlan != null && !packingPlan.HasPackingErrors)
                {
                    
                    packingPlan.ShipmentInfo = packShipRequest.ShipmentInfo;

                    
                    //Response<ShippingQuote> shippingQuoteResponse = await ShippingService.QuoteShipment(registration, packingPlan);
                    
                    //_shippingService instance expected be injected via IOC.
                    Response<ShippingQuote> shippingQuoteResponse = await this._shippingService.QuoteShipment(registration, packingPlan);

                    shippingQuote = shippingQuoteResponse;
                }
                else
                {
                    //Return Error
                    return Response<ShippingQuote>.Error(ResponseMessage.PackingError);
                }

            }

            

            return shippingQuote;
            
        }




        public async Task<ShippingPlan> GetShippingPlanAsync(PackAndShipInputModel packAndShipInputModel)
        {
            Registration registration = packAndShipInputModel.Registration;
            Models.PackShipRequest packShipRequest = packAndShipInputModel.PackShipRequestRecord;
            bool allowVertical = packAndShipInputModel.AllowVertical;
            bool includeItemImage = packAndShipInputModel.IncludeItemImage;
            bool includeStepByStepImage = packAndShipInputModel.IncludeStepByStepImage;
            bool includeGroupImage = packAndShipInputModel.IncludeGroupImage;
            int packingType = packAndShipInputModel.PackingType;

            ShippingPlan shippingPlan = new ShippingPlan();

            //Do not proceed if registration object is null.
            if (registration != null)
            {
                Response<PackingPlan> packingPlanResponse = await PackingService.MultiBinPacking(registration, packShipRequest, allowVertical, includeItemImage, includeStepByStepImage, includeGroupImage, packingType);

                PackingPlan packingPlan = packingPlanResponse.ResponseObject;

                if (packingPlan != null && !packingPlan.HasPackingErrors)
                {
                    packingPlan.ShipmentInfo = packShipRequest.ShipmentInfo;

                    Response<ShippingPlan> bookShipmentResponse = await ShippingService.BookShipment(registration, packingPlan);
                    shippingPlan = bookShipmentResponse.ResponseObject;
                }
                else
                {
                    //Return only details from Packing Plan.

                    shippingPlan = packingPlan as ShippingPlan;

                }
            }
            
            return shippingPlan;
        }
    }


}
