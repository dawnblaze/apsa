﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using APSA.Web.Libraries.XPS;
using APSA.Web.Libraries.XPS.Interface;
using APSA.Web.Libraries.XPS.Models.Responses;
using APSA.Web.Models;
using APSA.Web.Services.Interface;

namespace APSA.Web.Services
{
    public class ShippingService : IShippingService
    {
        private IXPSClient _xpsClient;

        public ShippingService(IXPSClient xpsClient)
        {
            _xpsClient = xpsClient;
        }

        public static async Task<Response<ShippingQuote>> QuoteShipment(Registration registration, PackShipRequest packShipRequest)
        {
            LoggingService.Debug("ShippingService", "QuoteShipment", notes: new { registration, packShipRequest });

            Response<ShippingQuote> result;

            if (packShipRequest == null)
                result = Response<ShippingQuote>.Error(ResponseMessage.InvalidRequest);
            else
            {
                try
                {
                    if (packShipRequest.Service == null)
                        result = Response<ShippingQuote>.Error(ResponseMessage.ServiceRequired);
                    else
                    {
                        var shippingClient = new XPSClient();

                        ShippingQuote shippingQuote = await shippingClient.QuoteShipment(packShipRequest, registration);

                        //ShippingQuote shippingQuote = await shippingClient.QuoteShipment(packShipRequest, shippingCredentials);

                        result = !shippingQuote.HasShippingErrors
                            ? Response<ShippingQuote>.Success(
                                shippingQuote)
                            : Response<ShippingQuote>.Error(shippingQuote.ErrorMessage);
                    }
                }
                catch (Exception err)
                {
                    result = Response<ShippingQuote>.Error(err);
                }
            }

            result.LogResult("ShippingService", "QuoteShipment");
            return result;
        }

        public static async Task<Response<ShippingPlan>> BookShipment(Registration registration, PackShipRequest packShipRequest)
        {
            LoggingService.Debug("ShippingService", "BookShipment", notes: new { registration, packShipRequest });

            Response<ShippingPlan> result;

            if (packShipRequest == null)
                result = Response<ShippingPlan>.Error(ResponseMessage.InvalidRequest);
            else
            {
                try
                {
                    if (packShipRequest.Service == null)
                        result = Response<ShippingPlan>.Error(ResponseMessage.ServiceRequired);
                    else
                    {
                        var shippingClient = new XPSClient();

                        ShippingPlan shippingPlan = await shippingClient.BookShipment(packShipRequest, registration);

                        if (!shippingPlan.HasShippingErrors)
                        {
                            await VoidShipment(registration, Int32.Parse(shippingPlan.ReferenceNumber));
                            result = Response<ShippingPlan>.Success(shippingPlan);
                        }
                        else
                            result = Response<ShippingPlan>.Error(shippingPlan.ErrorMessage);
                    }
                }
                catch (Exception err)
                {
                    result = Response<ShippingPlan>.Error(err);
                }
            }

            result.LogResult("ShippingService", "BookShipment");
            return result;
        }

        public static async Task<Response<DeleteResponse>> VoidShipment(Registration registration, int referenceID)
        {
            LoggingService.Debug("ShippingService", "VoidShipment", notes: new { registration, referenceID });

            Response<DeleteResponse> result;

            if (referenceID < 0)
                result = Response<DeleteResponse>.Error(ResponseMessage.InvalidRequest);
            else
            {
                try
                {
                    var shippingClient = new XPSClient();

                    result = await shippingClient.VoidShipment(referenceID, registration.ShippingCredentials);
                }
                catch (Exception err)
                {
                    result = Response<DeleteResponse>.Error(err);
                }
            }

            result.LogResult("ShippingService", "VoidShipment");
            return result;
        }

        public static async Task<Response<ShippingPlan>> UpdateShipment(Registration registration, int referenceId, PackShipRequest packShipRequest)
        {
            LoggingService.Debug("ShippingService", "UpdateShipment", notes: new { packShipRequest });

            Response<ShippingPlan> result;

            if (packShipRequest == null || referenceId < 0)
                result = Response<ShippingPlan>.Error(ResponseMessage.InvalidRequest);
            else
            {
                try
                {
                    // Void shipment then book it again
                    Response<DeleteResponse> voidShipment = await VoidShipment(registration, referenceId);
                    if (voidShipment.State == ResponseState.Success) result = await BookShipment(registration, packShipRequest);
                    else result = Response<ShippingPlan>.Error(voidShipment.MessageText);
                }
                catch (Exception err)
                {
                    result = Response<ShippingPlan>.Error(err);
                }
            }

            result.LogResult("ShippingService", "BookShipment");
            return result;
        }


        public static async Task<Response<List<XPSCarrier>>> GetCarriers(Registration registration, bool includeServices)
        {
            LoggingService.Debug("ShippingService", "GetCarriers", notes: new { registration, includeServices });

            Response<List<XPSCarrier>> result;

            try
            {
                var shippingClient = new XPSClient();

                List<XPSCarrier> carriers = await shippingClient.GetCarriers(includeServices, registration.ShippingCredentials);

                result = Response<List<XPSCarrier>>.Success(carriers);
            }
            catch (Exception err)
            {
                result = Response<List<XPSCarrier>>.Error(err);
            }

            result.LogResult("ShippingService", "GetCarriers");
            return result;
        }

        public static async Task<Response<List<XPSService>>> GetServices(Registration registration)
        {
            LoggingService.Debug("ShippingService", "GetServices", notes: new { registration });

            Response<List<XPSService>> result;

            try
            {
                var shippingClient = new XPSClient();

                List<XPSService> carriers = await shippingClient.GetServices(registration.ShippingCredentials);

                result = Response<List<XPSService>>.Success(carriers);
            }
            catch (Exception err)
            {
                result = Response<List<XPSService>>.Error(err);
            }

            result.LogResult("ShippingService", "GetServices");
            return result;
        }

        //Instance method. Implements IShippingService interface.
        public async Task<Response<ShippingQuote>> QuoteShipment(Registration registration, PackingPlan packingPlan)
        {
            LoggingService.Debug("ShippingService instance", "QuoteShipment", notes: new { registration, packingPlan });

            Response<ShippingQuote> result;

            if (packingPlan == null)
                result = Response<ShippingQuote>.Error(ResponseMessage.InvalidRequest);
            else
            {
                try
                {
                    if (packingPlan.Service == null)
                        result = Response<ShippingQuote>.Error(ResponseMessage.ServiceRequired);
                    else
                    {
                        
                        //Need to modify shippingClient to accept packingplan
                        ShippingQuote shippingQuote = await this._xpsClient.QuoteShipment(packingPlan, registration);

                        //ShippingQuote shippingQuote = await shippingClient.QuoteShipment(packShipRequest, shippingCredentials);

                        result = !shippingQuote.HasShippingErrors
                            ? Response<ShippingQuote>.Success(
                                shippingQuote)
                            : Response<ShippingQuote>.Error(shippingQuote.ErrorMessage);
                    }
                }
                catch (Exception err)
                {
                    result = Response<ShippingQuote>.Error(err);
                }
            }

            result.LogResult("ShippingService instance", "QuoteShipment");
            return result;
        }
    }
}
