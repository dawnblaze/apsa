﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace APSA.Web.Services
{
    /// <summary>
    /// Result of the execution of a database query.
    /// </summary>
    public class DatabaseResult
    {
        /// <summary>
        /// Indicates if the executions was a success or failure.
        /// </summary>
        public bool Success { get; set; }
        /// <summary>
        /// When the execution is a failure, this is the exception
        /// </summary>
        public Exception Exception { get; set; }
        /// <summary>
        /// The number of row affected
        /// </summary>
        public int RowsAffected { get; set; }
    }

    /// <summary>
    /// Service for handling database query execution
    /// </summary>
    public class DatabaseService
    {
        private static string connectionString;
        /// <summary>
        /// Initializes database service settings
        /// </summary>
        /// <param name="configuration">Application Configuration Settings</param>
        public static void Init(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("APSA");
        }
        /// <summary>
        /// Initializes database service settings with a simple connection string
        /// </summary>
        /// <param name="connString">Application Configuration Settings</param>
        public static void Init(string connString)
        {
            connectionString = connString;
        }

        /// <summary>
        /// Executes a query on the database
        /// </summary>
        /// <param name="bID">The ID of the Business the query is pertaining to for logging.</param>
        /// <param name="sql">The SQL query to be executed.</param>
        /// <param name="model">The object passed into the query as the parameters.</param>
        /// <returns>DatabaseResult</returns>
        public static async Task<DatabaseResult> ExecQuery(short bID, string sql, object model)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlTransaction trans = conn.BeginTransaction();

                try
                {
                    bool success = true;
                    string badSql = null;
                    Exception error = null;
                    int rows = 0;

                    try
                    {
                        rows = await conn.ExecuteAsync(sql, model, trans);
                    }
                    catch (Exception err)
                    {
                        badSql = sql;
                        success = false;
                        error = err;
                    }

                    if (!success)
                    {
                        trans.Rollback();
                        LoggingService.Error("SQL", "Error executing query.", exception: error, notes: badSql, bID: bID);
                        return new DatabaseResult
                        {
                            Success = false,
                            Exception = error
                        };
                    }


                    trans.Commit();
                    return new DatabaseResult { Success = true, RowsAffected = rows };
                }

                finally
                {
                    conn.Close();
                }
            }
        }
        /// <summary>
        /// Executes a stored procedure asynchronously on the database
        /// </summary>
        /// <param name="bID">The ID of the Business the query is pertaining to for logging.</param>
        /// <param name="procName">The stored procedure to the executed.</param>
        /// <param name="parameters">The input parameters to be applied to the stored procedure.</param>
        /// <param name="outputParameters">The output parameters to be returned from the stored procedure.</param>
        /// <returns>DatabaseResult</returns>
        public static async Task<DatabaseResult> ExecStoredProc(short bID, string procName, Dictionary<string, object> parameters = null, Dictionary<string, object> outputParameters = null)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                SqlCommand command = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn,
                    CommandText = procName
                };

                if (parameters != null && parameters.Count > 0)
                {
                    foreach (KeyValuePair<string, object> kvp in parameters)
                        command.Parameters.AddWithValue(kvp.Key, kvp.Value);
                }

                if (outputParameters != null && outputParameters.Count > 0)
                {
                    foreach (KeyValuePair<string, object> kvp in outputParameters)
                    {
                        SqlParameter param = command.Parameters.AddWithValue(kvp.Key, kvp.Value);
                        param.Direction = ParameterDirection.InputOutput;
                    }
                    outputParameters.Clear();
                }

                try
                {
                    int rows = await command.ExecuteNonQueryAsync();

                    if (outputParameters != null)
                    {
                        foreach (SqlParameter param in command.Parameters)
                        {
                            if (param.Direction == ParameterDirection.InputOutput)
                                outputParameters.Add(param.ParameterName, param.Value);
                        }
                    }

                    return new DatabaseResult { Success = true, RowsAffected = rows };
                }
                catch (Exception err)
                {
                    LoggingService.Error("SQL", $"Error executing stored procedure {procName}.", exception: err, bID: bID);
                    return new DatabaseResult
                    {
                        Success = false,
                        Exception = err
                    };
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public static async Task<DatabaseResult> ExecQueryFromFile(short bid, string fileName, object model)
        {
            string sqlContent = await File.ReadAllTextAsync(fileName);
            DatabaseResult dbResult = await ExecQuery(bid, sqlContent, null);

            if (dbResult.Success)
                return new DatabaseResult { Success = true };

            return new DatabaseResult
            {
                Success = false,
                Exception = new Exception($"Error executing query in {fileName}", dbResult.Exception),
            };
        }

        private static DynamicParameters PrepareParameters(Dictionary<string, object> parameters)
        {
            if (parameters == null || parameters.Count == 0)
                return null;

            DynamicParameters result = new DynamicParameters();

            foreach (KeyValuePair<string, object> kvp in parameters)
                result.Add(kvp.Key, kvp.Value);

            return result;
        }
        /// <summary>
        /// Loads a list of items from the database
        /// </summary>
        /// <typeparam name="T">The class of the item to be loaded</typeparam>
        /// <param name="sql">The SQL query to be executed.</param>
        /// <param name="parameters">The input parameters to be applied to the stored procedure.</param>
        /// <param name="commandType">The <see cref="CommandType"/> to execute.</param>
        /// <returns></returns>
        private static async Task<IEnumerable<T>> DoLoad<T>(string sql, Dictionary<string, object> parameters, CommandType commandType)
        {
            DynamicParameters myParams = PrepareParameters(parameters);

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                try
                {
                    return await conn.QueryAsync<T>
                               (sql: sql,
                                commandType: commandType,
                                param: myParams
                                );
                }
                finally
                {
                    conn.Close();
                }
            }
        }
        /// <summary>
        /// Loads a list of items with a subitem from the database
        /// </summary>
        /// <typeparam name="T">The class of the item to be loaded</typeparam>
        /// <typeparam name="T2">The class of the subitem to be loaded</typeparam>
        /// <param name="sql">The SQL query to be executed.</param>
        /// <param name="map">The delegate function for the mapping of the item and subitems.</param>
        /// <param name="splitOn">The field in the results to starts the subitem fields.</param>
        /// <param name="parameters">The input parameters to be applied to the stored procedure.</param>
        /// <param name="commandType">The <see cref="CommandType"/> to execute.</param>
        /// <returns></returns>
        private static async Task<IEnumerable<T>> DoLoad<T, T2>(string sql, Func<T, T2, T> map, string splitOn, Dictionary<string, object> parameters, CommandType commandType)
            where T : class
            where T2 : class
        {
            DynamicParameters myParams = PrepareParameters(parameters);

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                try
                {
                    return await conn.QueryAsync<T, T2, T>
                               (sql: sql,
                                commandType: commandType,
                                param: parameters,
                                map: map,
                                splitOn: splitOn
                                );
                }
                finally
                {
                    conn.Close();
                }
            }
        }
        /// <summary>
        /// Loads a list of items with a set of subitems from the database
        /// </summary>
        /// <typeparam name="T">The class of the item to be loaded</typeparam>
        /// <typeparam name="T2">The class of the first subitem to be loaded</typeparam>
        /// <typeparam name="T3">The class of the second subitem to be loaded</typeparam>
        /// <param name="sql">The SQL query to be executed.</param>
        /// <param name="map">The delegate function for the mapping of the item and subitems.</param>
        /// <param name="splitOn">The fields in the results to starts the subitem fields.</param>
        /// <param name="parameters">The input parameters to be applied to the stored procedure.</param>
        /// <param name="commandType">The <see cref="CommandType"/> to execute.</param>
        /// <returns></returns>
        private static async Task<IEnumerable<T>> DoLoad<T, T2, T3>(string sql, Func<T, T2, T3, T> map, string splitOn, Dictionary<string, object> parameters, CommandType commandType)
            where T : class
            where T2 : class
            where T3 : class
        {
            DynamicParameters myParams = PrepareParameters(parameters);

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                try
                {
                    return await conn.QueryAsync<T, T2, T3, T>
                               (sql: sql,
                                commandType: commandType,
                                param: parameters,
                                map: map,
                                splitOn: splitOn
                                );
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        /// <summary>
        /// Loads a single item from the database
        /// </summary>
        /// <typeparam name="T">The class of the item to be loaded</typeparam>
        /// <param name="sql">The SQL query to be executed.</param>
        /// <param name="parameters">The input parameters to be applied to the stored procedure.</param>
        /// <param name="commandType">The <see cref="CommandType"/> to execute.</param>
        /// <returns></returns>
        public static async Task<T> Load<T>(string sql, Dictionary<string, object> parameters = null, CommandType commandType = CommandType.Text)
        {
            IEnumerable<T> results = await DoLoad<T>(sql, parameters, commandType);
            return results.FirstOrDefault();
        }
        /// <summary>
        /// Loads a single item with a subitem from the database
        /// </summary>
        /// <typeparam name="T">The class of the item to be loaded</typeparam>
        /// <typeparam name="T2">The class of the subitem to be loaded</typeparam>
        /// <param name="sql">The SQL query to be executed.</param>
        /// <param name="map">The delegate function for the mapping of the item and subitems.</param>
        /// <param name="splitOn">The field in the results to starts the subitem fields.</param>
        /// <param name="parameters">The input parameters to be applied to the stored procedure.</param>
        /// <param name="commandType">The <see cref="CommandType"/> to execute.</param>
        /// <returns></returns>
        public static async Task<T> Load<T, T2>(string sql, Func<T, T2, T> map, string splitOn, Dictionary<string, object> parameters = null, CommandType commandType = CommandType.Text)
            where T : class
            where T2 : class
        {
            IEnumerable<T> results = await DoLoad<T, T2>(sql, map, splitOn, parameters, commandType);
            return results.FirstOrDefault();
        }
        /// <summary>
        /// Loads a single item with a set of subitems from the database
        /// </summary>
        /// <typeparam name="T">The class of the item to be loaded</typeparam>
        /// <typeparam name="T2">The class of the first subitem to be loaded</typeparam>
        /// <typeparam name="T3">The class of the second subitem to be loaded</typeparam>
        /// <param name="sql">The SQL query to be executed.</param>
        /// <param name="map">The delegate function for the mapping of the item and subitems.</param>
        /// <param name="splitOn">The fields in the results to starts the subitem fields.</param>
        /// <param name="parameters">The input parameters to be applied to the stored procedure.</param>
        /// <param name="commandType">The <see cref="CommandType"/> to execute.</param>
        /// <returns></returns>
        public static async Task<T> Load<T, T2, T3>(string sql, Func<T, T2, T3, T> map, string splitOn, Dictionary<string, object> parameters = null, CommandType commandType = CommandType.Text)
            where T : class
            where T2 : class
            where T3 : class
        {
            IEnumerable<T> results = await DoLoad<T, T2, T3>(sql, map, splitOn, parameters, commandType);
            return results.FirstOrDefault();
        }
        /// <summary>
        /// Loads a list of items from the database
        /// </summary>
        /// <typeparam name="T">The class of the item to be loaded</typeparam>
        /// <param name="sql">The SQL query to be executed.</param>
        /// <param name="parameters">The input parameters to be applied to the stored procedure.</param>
        /// <param name="commandType">The <see cref="CommandType"/> to execute.</param>
        /// <returns></returns>
        public static async Task<List<T>> LoadList<T>(string sql, Dictionary<string, object> parameters = null, CommandType commandType = CommandType.Text)
        {
            IEnumerable<T> results = await DoLoad<T>(sql, parameters, commandType);
            return results.ToList();
        }
        /// <summary>
        /// Loads a list of items with a subitem from the database
        /// </summary>
        /// <typeparam name="T">The class of the item to be loaded</typeparam>
        /// <typeparam name="T2">The class of the subitem to be loaded</typeparam>
        /// <param name="sql">The SQL query to be executed.</param>
        /// <param name="map">The delegate function for the mapping of the item and subitems.</param>
        /// <param name="splitOn">The field in the results to starts the subitem fields.</param>
        /// <param name="parameters">The input parameters to be applied to the stored procedure.</param>
        /// <param name="commandType">The <see cref="CommandType"/> to execute.</param>
        /// <returns></returns>
        public static async Task<List<T>> LoadList<T, T2>(string sql, Func<T, T2, T> map, string splitOn, Dictionary<string, object> parameters = null, CommandType commandType = CommandType.Text)
            where T : class
            where T2 : class
        {
            IEnumerable<T> results = await DoLoad<T, T2>(sql, map, splitOn, parameters, commandType);
            return results.ToList();
        }
        /// <summary>
        /// Loads a list of items with a set of subitems from the database
        /// </summary>
        /// <typeparam name="T">The class of the item to be loaded</typeparam>
        /// <typeparam name="T2">The class of the first subitem to be loaded</typeparam>
        /// <typeparam name="T3">The class of the second subitem to be loaded</typeparam>
        /// <param name="sql">The SQL query to be executed.</param>
        /// <param name="map">The delegate function for the mapping of the item and subitems.</param>
        /// <param name="splitOn">The fields in the results to starts the subitem fields.</param>
        /// <param name="parameters">The input parameters to be applied to the stored procedure.</param>
        /// <param name="commandType">The <see cref="CommandType"/> to execute.</param>
        /// <returns></returns>
        public static async Task<List<T>> LoadList<T, T2, T3>(string sql, Func<T, T2, T3, T> map, string splitOn, Dictionary<string, object> parameters = null, CommandType commandType = CommandType.Text)
            where T : class
            where T2 : class
            where T3 : class
        {
            IEnumerable<T> results = await DoLoad<T, T2, T3>(sql, map, splitOn, parameters, commandType);
            return results.ToList();
        }
    }
}
