﻿using System;
using APSA.Web.Services;
using APSA.Web.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using APSA.Web.Repositories;
using APSA.Web.Settings;
using APSA.Web.Services.Interface;
using System.Collections.Generic;
using System.Threading.Tasks;
using APSA.Web.Libraries.XPS.Interface;
using APSA.Web.Libraries.XPS;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Microsoft.OpenApi.Models;

namespace APSA.Web
{
    public class Startup
    {
        private const string AppTitle = "APSA API";
        private static string Version = "v1";
        //private static string BaseDirectory => AppDomain.CurrentDomain.BaseDirectory;
        public IConfiguration Configuration { get; }
        private IDiagnosticsService _diagnosticsService;

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            

            SQLService.SetContentRootPath(environment.ContentRootPath);
            Task.Run(async () => await SQLService.RunMigration());

            IConfigurationBuilder builder = GetConfigurationBuilder(environment);
            Configuration = builder.Build();

            //Configuration = configuration;
            DatabaseService.Init(Configuration);
            StorageService.Init(Configuration);
            LoggingService.Init(configuration);
            LoggingService.Information("Startup", $"{AppTitle} is starting up.");
        }

        private void ConfigureDiagnosticsServiceAndStartupVersion(IServiceCollection services)
        {
            this._diagnosticsService = new DiagnosticsService();
            services.AddTransient<IDiagnosticsService>(provider => _diagnosticsService);
            
            Startup.Version = "v"+this._diagnosticsService.GetBuildVersion();
        }

        private IConfigurationBuilder GetConfigurationBuilder(IWebHostEnvironment env)
        {
            return new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddUserSecrets<Startup>()
                .AddEnvironmentVariables();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            this.ConfigureDiagnosticsServiceAndStartupVersion(services);

            services.AddCors();
            services.AddOptions();

            services
            .AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            })
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
            })
            .AddControllersAsServices();

            services.AddSwaggerGen(config =>
            {
                
                config.SwaggerDoc(Startup.Version, new OpenApiInfo() { Title = Startup.AppTitle, Version = Startup.Version});
                //config.IncludeXmlComments($@"{Startup.BaseDirectory}\APSA.XML");
                config.AddSecurityDefinition("APSA", new OpenApiSecurityScheme()
                {
                    In = ParameterLocation.Header,
                    Description = "Please insert the registration GUID.",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });

                //config.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                //{
                //    { "APSA", new List<string>() }
                //});
            });

            services.AddAuthentication(config =>
            {
                config.DefaultAuthenticateScheme = APSAAuthOptions.DefaultScheme;
                config.DefaultChallengeScheme = APSAAuthOptions.DefaultScheme;
            }).AddCustomAuth(options => {});

            services.Configure<DeleteExpiredRequestedImagesSettings>(Configuration.GetSection("DeleteExpiredRequestedImagesSettings"));
            services.AddTransient<IRequestedImagesRepository, RequestedImagesRepository>();

            services.AddTransient<IPackAndShipService, PackAndShipService>();
            services.AddTransient<IShippingService, ShippingService>();
            services.AddTransient<IXPSClient, XPSClient>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
                //app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseSwagger();
            
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/"+Startup.Version+"/swagger.json", "APSA "+Startup.Version);
                c.RoutePrefix = "swagger";

            });
        }
    }
}
