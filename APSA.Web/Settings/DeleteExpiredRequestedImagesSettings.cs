﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Settings
{
    public class DeleteExpiredRequestedImagesSettings
    {
        public int NumDays { get; set; }
        public string AzureConnectionString { get; set; }
        public string ImagesRootContainer { get; set; }
        
    }
}
