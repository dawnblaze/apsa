﻿namespace APSA.Web.Libraries.BinPacking.Models
{
    /// <summary>
    /// Container
    /// </summary>
    public class Bin
    {
        /// <summary>
        /// Container ID. It enables to identify a particular container in a packing result.
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// Container width.
        /// </summary>
        public string w { get; set; }
        /// <summary>
        /// Container height.
        /// </summary>
        public string h { get; set; }
        /// <summary>
        /// Container depth.
        /// </summary>
        public string d { get; set; }
        /// <summary>
        /// Maximum weight of items in a container. Value set to "0" (zero) means there is no weight limit
        /// </summary>
        public string max_wg { get; set; }
    }
}
