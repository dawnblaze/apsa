﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Libraries.BinPacking.Models
{
    public class RequestData
    {
        public string username { get; set; }
        public string api_key { get; set; }
        public List<Item> items { get; set; }
        public List<Bin> bins { get; set; }
        public OptionalParameters @params { get; set; }
    }
}
