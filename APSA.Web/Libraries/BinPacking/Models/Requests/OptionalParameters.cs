﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Libraries.BinPacking.Models
{
    public class OptionalParameters
    {
        /// <summary>
        /// Defines packing optimisation mode
        /// </summary>
        public string optimization_mode { get; set; }
        /// <summary>
        /// Max. width of generated images
        /// </summary>
        public string images_width { get; set; }
        /// <summary>
        /// Max. height of generated images
        /// </summary>
        public string images_height { get; set; }
        /// <summary>
        /// Background color
        /// </summary>
        public string images_background_color { get; set; }
        /// <summary>
        /// Container border color
        /// </summary>
        public string images_bin_border_color { get; set; }
        /// <summary>
        /// Container front border color (dashed)
        /// </summary>
        public string images_bin_dashed_line_color { get; set; }
        /// <summary>
        /// Container fill color
        /// </summary>
        public string images_bin_fill_color { get; set; }
        /// <summary>
        /// Item border color
        /// </summary>
        public string images_item_border_color { get; set; }
        /// <summary>
        /// Fill color of the last item on images showing packing "step by step"
        /// </summary>
        public string images_sbs_last_item_fill_color { get; set; }
        /// <summary>
        /// Border color of the last item on images showing packing "step by step"
        /// </summary>
        public string images_sbs_last_item_border_color { get; set; }
        /// <summary>
        /// Images format
        /// </summary>
        public string images_format { get; set; }
        /// <summary>
        /// Generate images showing packing "step by step"
        /// </summary>
        public string images_sbs { get; set; }
        /// <summary>
        /// Generate images showing final packing result.
        /// </summary>
        public string images_complete { get; set; }
        /// <summary>
        /// Generate images showing the placement of each item separately
        /// </summary>
        public string images_separated { get; set; }
        /// <summary>
        /// Return coordinates (x, y, z) for each item
        /// </summary>
        public string item_coordinates { get; set; }
        /// <summary>
        /// Return stats (eg. images generation time)
        /// </summary>
        public string stats { get; set; }
    }
}
