﻿namespace APSA.Web.Libraries.BinPacking.Models
{
    public class Item
    {
        /// <summary>
        /// Item ID. It enables to identify a particular item in a packing result
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// Item width.
        /// </summary>
        public string w { get; set; }
        /// <summary>
        /// Item height.
        /// </summary>
        public string h { get; set; }
        /// <summary>
        /// Item depth.
        /// </summary>
        public string d { get; set; }
        /// <summary>
        /// Item weight
        /// </summary>
        public string wg { get; set; }
        /// <summary>
        /// vertical rotation. The information if the item can be rotated vertically
        /// </summary>
        public string vr { get; set; }
        /// <summary>
        /// The amount of the same items to pack
        /// </summary>
        public string q { get; set; }
    }
}
