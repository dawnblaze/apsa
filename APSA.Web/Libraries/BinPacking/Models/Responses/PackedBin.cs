using Newtonsoft.Json;
using System.Collections.Generic;

namespace APSA.Web.Libraries.BinPacking.Models.Responses
{

    public class PackedBin
    {
        public BinData Bin_data {get; set;}
        
        public IList<Item> Items {get; set;}

    }

}