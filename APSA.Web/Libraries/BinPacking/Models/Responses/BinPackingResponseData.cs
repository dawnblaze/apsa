using Newtonsoft.Json;
using System.Collections.Generic;

namespace APSA.Web.Libraries.BinPacking.Models.Responses
{

    public class BinPackingResponseData
    {
        public string Id {get; set;}
        
        public IList<PackedBin> Bins_packed {get; set;}
        
        public IList<BinPackingError> Errors {get; set;}
        
        public int Status {get; set;}

        public IList<Item> Not_packed_items {get; set;}
    }

}

