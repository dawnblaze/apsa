namespace APSA.Web.Libraries.BinPacking.Models.Responses
{

    public class Item 
    {

        public string Id { get; set;}
        public float W { get; set; }
        public float H { get; set; }
        public float D { get; set; }
        public float Wg { get;set; }
        public float Q { get; set; }
        
    }

}