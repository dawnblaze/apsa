﻿using Newtonsoft.Json;

namespace APSA.Web.Libraries.BinPacking.Models.Responses
{
    public class BinData
    {
        
        public string Id { get; set; }
        public float W { get; set; }
        public float H { get; set; }
        public float D { get; set; }
        public float Used_space { get; set; }
        public float Weight { get; set; }
        public string Used_weight { get; set; }
        public string Stack_height { get; set; }
        public string Order_id { get; set; }
    }
}
