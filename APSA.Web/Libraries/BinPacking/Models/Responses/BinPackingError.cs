﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Libraries.BinPacking.Models.Responses
{
    public class BinPackingError
    {
        public string Level { get; set; }
        public string Message { get; set; }
    }
}
