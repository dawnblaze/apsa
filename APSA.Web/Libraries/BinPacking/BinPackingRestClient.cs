﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using APSA.Web.Libraries.BinPacking.Models;
using APSA.Web.Libraries.BinPacking.Models.Responses;
using APSA.Web.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace APSA.Web.Libraries.BinPacking
{
    public class BinPackingRestClient
    {
        private const string BaseUrl = "https://global-api.3dbinpacking.com/packer";
        private readonly string _singleBinPackingUrl = $"{BaseUrl}/pack";
        private readonly string _multiBinPackingUrl = $"{BaseUrl}/packIntoMany";

        /// <summary>
        /// Access the 3D Bin Packing API and process packing based on what packingType will be used.
        /// </summary>
        /// <param name="packingCredentials"></param>
        /// <param name="containers"></param>
        /// <param name="packShipRequest"></param>
        /// <param name="allowVertical">Permits the packing algorithm to rotate the item vertically (upright) to produce an optimal packing list</param>
        /// <param name="includeItemImage">Includes a URL for the images for each item by itself. (Called images_separated in 3D Bin Packing.)</param>
        /// <param name="includeStepByStepImage">Includes a URL to obtains the Step-By-Step packing images. </param>
        /// <param name="includeGroupImage">Includes a URL to obtains a packing image for the final state of the group.</param>
        /// <param name="packingType"> 1 - Single Bin Packing; 2 - Multi Bin Packing</param>
        /// <returns> PackingPlan </returns>
        public async Task<PackingPlan> BinPacking(
            PackingCredentials packingCredentials
            , List<ShippingContainer> containers
            , PackShipRequest packShipRequest
            , bool allowVertical
            , bool includeItemImage
            , bool includeStepByStepImage
            , bool includeGroupImage
            , int packingType)
        {
            string packingUrl;

            switch (packingType)
            {
                case 1:
                    packingUrl = _singleBinPackingUrl;
                    break;
                case 2:
                    packingUrl = _multiBinPackingUrl;
                    break;
                default:
                    packingUrl = _singleBinPackingUrl;
                    break;
            }

            var req = (HttpWebRequest)WebRequest.Create(packingUrl);

            var requestData = ConvertToRequestData(packingCredentials, containers, packShipRequest, allowVertical, includeItemImage,
                includeStepByStepImage, includeGroupImage);

            string jsonStr = JsonConvert.SerializeObject(requestData);

            byte[] postBytes = System.Text.Encoding.ASCII.GetBytes(jsonStr);

            {
                req.Method = "POST";
                req.ContentType = "application/json";
                req.ContentLength = postBytes.Length;

                System.IO.Stream postStream = await req.GetRequestStreamAsync();
                postStream.Write(postBytes, 0, postBytes.Length);
                postStream.Close();

                using (HttpWebResponse response = await req.GetResponseAsync() as HttpWebResponse)
                {
                    if (response == null) return null;

                    Encoding encoding = Encoding.GetEncoding("utf-8");

                    System.IO.StreamReader reader =
                        new System.IO.StreamReader(
                            response.GetResponseStream() ?? throw new InvalidOperationException(), encoding);
                    var data = reader.ReadToEnd();

                    //Use concrete BinPackingResponse type for deserialized response data.
                    BinPackingResponse binPackingResponse = JsonConvert.DeserializeObject<BinPackingResponse>(data);

                    //Start of updated section.
                    //Create list to map the returned packed containers or bins.
                    var packedContainers = new List<PackedContainer>();

                    if (containers != null && containers.Count > 0)
                    {
                        //Only add to the the list of packed containers bins that are actually packed.
                        foreach (var bin in binPackingResponse.Response.Bins_packed)
                        {
                            var matchedContainer = containers.Where(sc => sc.Name == bin.Bin_data.Id).First<ShippingContainer>();

                            if (matchedContainer != null)
                            {

                                var packedContainer = new PackedContainer
                                {
                                    ContainerWeight = bin.Bin_data.Weight,
                                    Depth = bin.Bin_data.D,
                                    FilledSpace = bin.Bin_data.Used_space,
                                    Height = bin.Bin_data.H,
                                    MaxWeight = matchedContainer.MaxWeight,
                                    Name = matchedContainer.Name,
                                    NameLabel = matchedContainer.NameLabel,
                                    Width = bin.Bin_data.W
                                };

                                //Create a list for the packed items of the current bin.
                                List<PackedItem> packedItemsForBin = new List<PackedItem>();

                                foreach (var binItem in bin.Items)
                                {
                                    //Get the details from the PackShipRequest.Items collection.
                                    ShippingItem shippingItem = packShipRequest.Items.Where(i => i.Name == binItem.Id).First<ShippingItem>();

                                    PackedItem packedItem = new PackedItem
                                    {
                                        Width = binItem.W,
                                        Height = binItem.H,
                                        Depth = binItem.D,
                                        Weight = binItem.Wg,
                                        ID = shippingItem.ID,
                                        InsuredValue = shippingItem.InsuredValue,
                                        DeclaredValue = shippingItem.DeclaredValue,
                                        ItemImageURL = "",
                                        Name = shippingItem.Name,
                                        Quantity = 1
                                    };

                                    packedItemsForBin.Add(packedItem);

                                }

                                //Set packed items list to packed container instance.
                                packedContainer.Items = packedItemsForBin;

                                //Add container to packed containers list.
                                packedContainers.Add(packedContainer);

                            }

                        }

                    }

                    List<ShippingItem> unPackedItemsFromResponse = new List<ShippingItem>();

                    foreach (var unPackedBinItem in binPackingResponse.Response.Not_packed_items)
                    {
                        var unPackedShippingItem = packShipRequest.Items.Where(i => i.Name == unPackedBinItem.Id).First<ShippingItem>();

                        unPackedShippingItem.Quantity = (int)unPackedBinItem.Q;

                        unPackedItemsFromResponse.Add(unPackedShippingItem);
                    }


                    bool hasPackingErrors = binPackingResponse.Response.Errors.Count > 0;

                    var packingPlan = new PackingPlan
                    {
                        Containers = packedContainers,
                        UnpackedItems = unPackedItemsFromResponse,
                        HasPackingErrors = hasPackingErrors,
                        ID = packShipRequest.ID,
                        Items = packShipRequest.Items,
                        Name = packShipRequest.Name,
                        OrderID = packShipRequest.OrderID,
                        Service = packShipRequest.Service,
                        Services = packShipRequest.Services
                    };


                    return packingPlan;
                }
            }
        }

        private RequestData ConvertToRequestData(
            PackingCredentials packingCredentials
            , List<ShippingContainer> containers
            , PackShipRequest packShipRequest
            , bool allowVertical
            , bool includeItemImage
            , bool includeStepByStepImage
            , bool includeGroupImage)
        {
            var requestData = new RequestData
            {
                username = packingCredentials.Login,
                api_key = packingCredentials.APIKey,
                items = new List<Models.Item>(packShipRequest.Items.Select(i => new Models.Item
                {
                    id = i.Name,
                    w = i.Width.ToString("0.00"),
                    h = i.Height.ToString("0.00"),
                    d = i.Depth.ToString("0.00"),
                    q = i.Quantity.ToString("0.00"),
                    wg = i.Weight.ToString("0.00"),
                    vr = allowVertical ? "1" : "0"
                })),
                bins = new List<Bin>(containers.Select(c => new Bin
                {
                    id = c.Name,
                    d = c.Depth.ToString("0.00"),
                    h = c.Height.ToString("0.00"),
                    w = c.Width.ToString("0.00"),
                    max_wg = c.MaxWeight.ToString("0.00")
                })),
                @params = new OptionalParameters
                {
                    images_complete = includeGroupImage ? "1" : "0",
                    images_separated = includeItemImage ? "1" : "0"
                }
            };

            return requestData;
        }

    } 
}
