﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using APSA.Web.Libraries.XPS.Models.Requests;
using APSA.Web.Libraries.XPS.Models.Responses;
using APSA.Web.Models;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using APSA.Web.Libraries.XPS.Interface;

namespace APSA.Web.Libraries.XPS
{
    public class XPSClient : IXPSClient
    {
        private const string BaseUrl = "https://xpsshipper.com/restapi/v1/customers";

        public async Task<ShippingQuote> QuoteShipment(PackShipRequest packShipRequest, Registration registration)
        {
            var shippingQuote = new ShippingQuote();

            string quoteUrl = $"{BaseUrl}/{registration.ShippingCredentials.Login}/quote";
            XPSQuoteShipment quoteRequest = ConvertToQuoteRequest(packShipRequest, registration);
            string requestJsonString = JsonConvert.SerializeObject(quoteRequest);

            HttpWebRequest xpsRequest = CreateXPSRequest(quoteUrl, requestJsonString, registration.ShippingCredentials, HttpMethod.Post);
            
            try
            {
                var httpResponse = (HttpWebResponse) await xpsRequest.GetResponseAsync();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream() ?? throw new InvalidOperationException()))
                {
                    string result = streamReader.ReadToEnd();
                    JObject xpsObjectResult = JObject.Parse(result);

                    // From PackShipRequest
                    shippingQuote.ID = packShipRequest.ID;
                    shippingQuote.OrderID = packShipRequest.OrderID;
                    shippingQuote.Name = packShipRequest.Name;
                    shippingQuote.Items = packShipRequest.Items;
                    shippingQuote.ShipmentInfo = packShipRequest.ShipmentInfo;
                    shippingQuote.Service = packShipRequest.Service;
                    shippingQuote.Services = packShipRequest.Services;

                    // From PackingPlan
                    shippingQuote.HasPackingErrors = false;
                    shippingQuote.Containers = null;
                    shippingQuote.UnpackedItems = null;
                    shippingQuote.StepByStepImageURL = "";
                    shippingQuote.GroupImageURL = "";

                    shippingQuote.DeclaredValue = shippingQuote.Items.Count != 0 ? (int)shippingQuote.Items.Sum(i => (i.DeclaredValue * i.Quantity)) : 0;
                    shippingQuote.InsuredValue = shippingQuote.Items.Count != 0 ? (int)shippingQuote.Items.Sum(i => (i.InsuredValue * i.Quantity)) : 0;
                    shippingQuote.Quote = new List<CarrierServiceQuote>();

                    if (shippingQuote.Service != null)
                    {
                        shippingQuote.Quote.Add(new CarrierServiceQuote
                        {
                            Carrier = shippingQuote.Service.Carrier,
                            Service = shippingQuote.Service.Service,
                            CarrierPackageCode = shippingQuote.Service.CarrierPackageCode,
                            SubTotalPrice = xpsObjectResult.SelectToken("baseAmount").ToObject<float>()
                        });
                    }

                    shippingQuote.Surcharges = new List<Surcharge>();

                    // Get the surcharges data from the XPS response
                    var surcharges = xpsObjectResult.SelectToken("surcharges").ToList();

                    if (surcharges.Count != 0)
                    {
                        // Assign the surcharges to shippingQuote.Surcharges
                        shippingQuote.Surcharges.AddRange(surcharges.Select(s => new Surcharge
                        {
                            description = (string) s.SelectToken("description"),
                            price = (string) s.SelectToken("amount")
                        }));
                    }

                    shippingQuote.Price = xpsObjectResult.SelectToken("totalAmount").ToObject<float>();
                    shippingQuote.Zone = xpsObjectResult.SelectToken("zone").ToObject<int>();
                    shippingQuote.HasShippingErrors = false;
                    shippingQuote.ErrorMessage = "";
                }
            }
            catch (Exception e)
            {
                shippingQuote.HasShippingErrors = true;
                shippingQuote.ErrorMessage = e.Message;
            }

            return shippingQuote;
        }

        public async Task<ShippingPlan> BookShipment(PackShipRequest packShipRequest, Registration registration)
        {
            var shippingPlan = new ShippingPlan();

            string shipUrl = $"{BaseUrl}/{registration.ShippingCredentials.Login}/shipments";
            XPSBookShipment shipRequest = ConvertToShipRequest(packShipRequest, registration);
            string requestJsonString = JsonConvert.SerializeObject(shipRequest);
            //requestJsonString = "{\"carrierCode\": \"ups\",\"serviceCode\": \"ups_ground\",\"packageTypeCode\": \"ups_custom_package\",\"shipmentDate\": \"2019-07-13\",\"shipmentReference\": \"1234\",\"contentDescription\": \"test\",\"sender\": {\"name\": \"Shipping Department\",\"company\": \"COREBRIDGE DEVELOPMENT\",\"address1\": \"US-201b Capital St\",\"address2\": \"\",\"city\": \"Augusta\",\"state\": \"ME\",\"zip\": \"04330\",\"country\": \"US\",\"phone\": \"8015042351\"},\"receiver\": {\"name\": \"Alice Jensen\",\"company\": \"\",\"address1\": \"54 Green St.\",\"address2\": \"\",\"city\": \"Salt Lake City\",\"state\": \"UT\",\"zip\": \"84106\",\"country\": \"US\",\"phone\": \"8013920046\"},\"residential\": true,\"signatureOptionCode\": \"NO_SIGNATURE_REQUIRED\",\"weightUnit\": \"lb\",\"dimUnit\": \"in\",\"currency\": \"USD\",\"customsCurrency\": \"USD\",\"pieces\": [{\"weight\": \"2\",\"length\": \"5\",\"width\": \"5\",\"height\": \"5\",\"insuranceAmount\": \"12.15\",\"declaredValue\": null}],\"billing\": {\"party\": \"sender\"}}";

            HttpWebRequest xpsRequest = CreateXPSRequest(shipUrl, requestJsonString, registration.ShippingCredentials, HttpMethod.Post);

            try
            {
                var httpResponse = (HttpWebResponse)await xpsRequest.GetResponseAsync();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream() ?? throw new InvalidOperationException()))
                {
                    string result = streamReader.ReadToEnd();
                    JObject xpsObjectResult = JObject.Parse(result);

                    // From PackShipRequest
                    shippingPlan.ID = packShipRequest.ID;
                    shippingPlan.OrderID = packShipRequest.OrderID;
                    shippingPlan.Name = packShipRequest.Name;
                    shippingPlan.Items = packShipRequest.Items;
                    shippingPlan.ShipmentInfo = packShipRequest.ShipmentInfo;
                    shippingPlan.Service = packShipRequest.Service;
                    shippingPlan.Services = packShipRequest.Services;

                    // From PackingPlan
                    shippingPlan.HasPackingErrors = false;
                    shippingPlan.Containers = null;
                    shippingPlan.UnpackedItems = null;
                    shippingPlan.StepByStepImageURL = "";
                    shippingPlan.GroupImageURL = "";

                    shippingPlan.DeclaredValue = shippingPlan.Items.Count != 0 ? (int)shippingPlan.Items.Sum(i => (i.DeclaredValue * i.Quantity)) : 0;
                    shippingPlan.InsuredValue = shippingPlan.Items.Count != 0 ? (int)shippingPlan.Items.Sum(i => (i.InsuredValue * i.Quantity)) : 0;
                    shippingPlan.SubTotalPrice = null;
                    shippingPlan.Surcharges = new List<Surcharge>();

                    // Get the surcharges data from the XPS response
                    //var surcharges = xpsObjectResult.SelectToken("surcharges").ToList();

                    //if (surcharges.Count != 0)
                    //{
                    //    // Assign the surcharges to shippingQuote.Surcharges
                    //    shippingPlan.Surcharges.AddRange(surcharges.Select(s => new Surcharge
                    //    {
                    //        description = (string)s.SelectToken("description"),
                    //        price = (string)s.SelectToken("amount")
                    //    }));
                    //}

                    //shippingPlan.Price = xpsObjectResult.SelectToken("totalAmount").ToObject<float>();
                    shippingPlan.Zone = xpsObjectResult.SelectToken("zone").ToObject<int>();
                    shippingPlan.HasShippingErrors = false;
                    shippingPlan.ErrorMessage = "";

                    shippingPlan.ReferenceID = new Guid(); // A referenceID for this packing configuration. 
                    shippingPlan.ReferenceNumber = xpsObjectResult.SelectToken("bookNumber").ToObject<string>();
                    shippingPlan.TrackingNumber = xpsObjectResult.SelectToken("trackingNumber").ToObject<string>();
                    shippingPlan.PrePaidBalance = xpsObjectResult.SelectToken("prepayBalance").ToObject<float>();
                }
            }
            catch (Exception e)
            {
                shippingPlan.HasShippingErrors = true;
                shippingPlan.ErrorMessage = e.Message;
            }

            return shippingPlan;
        }

        public async Task<List<XPSCarrier>> GetCarriers(bool includeServices, ShippingCredentials shippingCredentials)
        {
            var carriers = new List<XPSCarrier>();

            string getCarriersUrl = $"{BaseUrl}/{shippingCredentials.Login}/services";

            HttpWebRequest xpsRequest = CreateXPSRequest(getCarriersUrl, String.Empty, shippingCredentials, HttpMethod.Get);

            try
            {
                var httpResponse = (HttpWebResponse)await xpsRequest.GetResponseAsync();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream() ?? throw new InvalidOperationException()))
                {
                    string result = streamReader.ReadToEnd();
                    JObject xpsObjectResult = JObject.Parse(result);

                    carriers = xpsObjectResult.SelectToken("services").Select(s => new XPSCarrier
                    {
                        carrier = (string) s.SelectToken("carrierLabel"),
                        name = (string) s.SelectToken("serviceLabel")
                    }).ToList();
                }
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(data))
                        {
                            string text = reader.ReadToEnd();
                            Console.WriteLine(text);
                        }
                    }
                }
            }

            return carriers;
        }

        public async Task<Response<DeleteResponse>> VoidShipment(int referenceId, ShippingCredentials shippingCredentials)
        {
            Response<DeleteResponse> response;
            string voidUrl = $"{BaseUrl}/{shippingCredentials.Login}/shipments/{referenceId}";
            string requestJsonString = JsonConvert.SerializeObject(new XPSVoidShipment { voided = true });
            HttpWebRequest xpsRequest = CreateXPSRequest(voidUrl, requestJsonString, shippingCredentials, HttpMethod.Post);

            try
            {
                var httpResponse = (HttpWebResponse)await xpsRequest.GetResponseAsync();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream() ?? throw new InvalidOperationException()))
                {
                    string result = streamReader.ReadToEnd();
                    JObject xpsObjectResult = JObject.Parse(result);

                    response = Response<DeleteResponse>.Success(new DeleteResponse { NotFound = false });
                }
            }
            catch (WebException e)
            {
                using (WebResponse resp = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)resp;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = resp.GetResponseStream())
                    {
                        using (var reader = new StreamReader(data))
                        {
                            string text = reader.ReadToEnd();
                            Console.WriteLine(text);
                            JObject xpsObjectResult = JObject.Parse(text);
                            response = Response<DeleteResponse>.Error(
                                (string)xpsObjectResult.SelectToken("errorCategory") + ": " + (string)xpsObjectResult.SelectToken("error"));
                        }
                    }
                }
            }

            return response;
        }


        public async Task<List<XPSService>> GetServices(ShippingCredentials shippingCredentials)
        {
            var services = new List<XPSService>();

            string getServicesUrl = $"{BaseUrl}/{shippingCredentials.Login}/integratedQuotingOptions";

            HttpWebRequest xpsRequest = CreateXPSRequest(getServicesUrl, String.Empty, shippingCredentials, HttpMethod.Get);

            try
            {
                var httpResponse = (HttpWebResponse)await xpsRequest.GetResponseAsync();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream() ?? throw new InvalidOperationException()))
                {
                    string result = streamReader.ReadToEnd();
                    JObject xpsObjectResult = JObject.Parse(result);

                    services = xpsObjectResult.SelectToken("integratedQuotingOptions").Select(s => new XPSService
                    {
                        carrierCode = (string)s.SelectToken("carrierCode"),
                        serviceCode = (string)s.SelectToken("serviceCode"),
                        name = (string)s.SelectToken("name")
                    }).ToList();
                }
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(data))
                        {
                            string text = reader.ReadToEnd();
                            Console.WriteLine(text);
                        }
                    }
                }
            }

            return services;
        }

        private static HttpWebRequest CreateXPSRequest(string url, string requestJsonString, ShippingCredentials shippingCredentials, HttpMethod method)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/json";
            request.Method = method.ToString();
            request.Headers["Authorization"] = shippingCredentials.APIKey;

            if (string.IsNullOrEmpty(requestJsonString)) return request;

            using (var writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(requestJsonString);
            }

            return request;
        }

        private static XPSBookShipment ConvertToShipRequest(PackShipRequest packShipRequest, Registration registration)
        {
            var items = new List<Piece>();

            items.AddRange(packShipRequest.Items.Select(i => new Piece
            {
                weight = i.Weight.ToString("0.00"),
                length = i.Depth.ToString("0.00"),
                width = i.Width.ToString("0.00"),
                height = i.Height.ToString("0.00"),
                insuranceAmount = i.InsuredValue.ToString("0.00"),
                declaredValue = i.DeclaredValue.ToString("0.00")
            }));

            var shipRequest = new XPSBookShipment
            {
                carrierCode = packShipRequest.Service.Carrier,
                serviceCode = packShipRequest.Service.Service,
                packageTypeCode = packShipRequest.Service.CarrierPackageCode,
                shipmentDate = packShipRequest.ShipmentInfo.ShipmentDate.ToString("yyyy-MM-dd"),
                shipmentReference = packShipRequest.ShipmentInfo.ShipmentReference,
                contentDescription = packShipRequest.ShipmentInfo.Description,
                sender = new ShipPeerInformation
                {
                    name = packShipRequest.ShipmentInfo.ShipFrom.Name,
                    company = packShipRequest.ShipmentInfo.ShipFrom.Company,
                    address1 = packShipRequest.ShipmentInfo.ShipFrom.Address.Street,
                    address2 = packShipRequest.ShipmentInfo.ShipFrom.Address.Street2,
                    city = packShipRequest.ShipmentInfo.ShipFrom.Address.City,
                    state = packShipRequest.ShipmentInfo.ShipFrom.State,
                    zip = packShipRequest.ShipmentInfo.ShipFrom.Address.PostalCode,
                    country = packShipRequest.ShipmentInfo.ShipFrom.Address.Country,
                    phone = packShipRequest.ShipmentInfo.ShipFrom.Phone
                },
                receiver = new ShipPeerInformation
                {
                    name = packShipRequest.ShipmentInfo.ShipTo.Name,
                    company = packShipRequest.ShipmentInfo.ShipTo.Company,
                    address1 = packShipRequest.ShipmentInfo.ShipTo.Address.Street,
                    address2 = packShipRequest.ShipmentInfo.ShipTo.Address.Street2,
                    city = packShipRequest.ShipmentInfo.ShipTo.Address.City,
                    state = packShipRequest.ShipmentInfo.ShipTo.State,
                    zip = packShipRequest.ShipmentInfo.ShipTo.Address.PostalCode,
                    country = packShipRequest.ShipmentInfo.ShipTo.Address.Country,
                    phone = packShipRequest.ShipmentInfo.ShipTo.Phone
                },
                residential = true,
                signatureOptionCode = packShipRequest.ShipmentInfo.SignatureOption,
                weightUnit = registration.UnitOfWeight,
                dimUnit = registration.UnitOfLength,
                currency = registration.Currency, // Currency from ClientRegistration
                customsCurrency = registration.Currency,
                pieces = items,
                billing = new BaseBillingInformation
                {
                    party = "sender"
                }
            };

            return shipRequest;
        }

        private static XPSQuoteShipment ConvertToQuoteRequest(PackShipRequest packShipRequest, Registration registration)
        {
            //Just need to change this part to use the packed bins for pieces instead of items.
            var items = new List<Piece>();

            items.AddRange(packShipRequest.Items.Select(i => new Piece
            {
                weight = i.Weight.ToString("0.00"),
                length = i.Depth.ToString("0.00"),
                width = i.Width.ToString("0.00"),
                height = i.Height.ToString("0.00"),
                insuranceAmount = i.InsuredValue.ToString("0.00"),
                declaredValue = i.DeclaredValue.ToString("0.00")
            }));

            var quoteRequest = new XPSQuoteShipment
            {
                carrierCode = packShipRequest.Service.Carrier,
                serviceCode = packShipRequest.Service.Service,
                packageTypeCode = packShipRequest.Service.CarrierPackageCode,
                sender = new BasePeerInformation
                {
                    country = packShipRequest.ShipmentInfo.ShipFrom.Address.Country,
                    zip = packShipRequest.ShipmentInfo.ShipFrom.Address.PostalCode
                },

                

                receiver = new QuoteReceiver
                {
                    city = packShipRequest.ShipmentInfo.ShipTo.Address.City,
                    country = packShipRequest.ShipmentInfo.ShipTo.Address.Country,
                    zip = packShipRequest.ShipmentInfo.ShipTo.Address.PostalCode
                },
                residential = false,
                signatureOptionCode = packShipRequest.ShipmentInfo.SignatureOption,
                contentDescription = packShipRequest.ShipmentInfo.Description,
                weightUnit = "lb",//Temporarily hardcoded for now. registration.UnitOfWeight,
                dimUnit = "in", //Temporarily hardcoded for now. registration.UnitOfLength,
                currency = registration.Currency,
                pieces = items,
                billing = new BaseBillingInformation
                {
                    party = "sender"
                }
            };

            return quoteRequest;
        }

        public async Task<ShippingQuote> QuoteShipment(PackingPlan packingPlan, Registration registration)
        {
            //Return immediately if either method parameter is null.
            if (packingPlan == null || registration == null) return default(ShippingQuote);

            var shippingQuote = new ShippingQuote();
            

            string quoteUrl = $"{BaseUrl}/{registration.ShippingCredentials.Login}/quote";
            XPSQuoteShipment quoteRequest = this.ConvertPackingPlanToQuoteRequest(packingPlan, registration);
            string requestJsonString = JsonConvert.SerializeObject(quoteRequest);
            HttpWebRequest xpsRequest = CreateXPSRequest(quoteUrl, requestJsonString, registration.ShippingCredentials, HttpMethod.Post);

            try
            {
                var httpResponse = (HttpWebResponse)await xpsRequest.GetResponseAsync();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream() ?? throw new InvalidOperationException()))
                {
                    string result = streamReader.ReadToEnd();
                    JObject xpsObjectResult = JObject.Parse(result);

                    //Cast to PackShipRequest for now to use legacy code referencing PackShipRequest specific properties.
                    PackShipRequest packShipRequest = (PackShipRequest)packingPlan;

                    // From PackShipRequest
                    shippingQuote.ID = packShipRequest.ID;
                    shippingQuote.OrderID = packShipRequest.OrderID;
                    shippingQuote.Name = packShipRequest.Name;
                    shippingQuote.Items = packShipRequest.Items;
                    shippingQuote.ShipmentInfo = packShipRequest.ShipmentInfo;
                    shippingQuote.Service = packShipRequest.Service;
                    shippingQuote.Services = packShipRequest.Services;

                    // From PackingPlan
                    shippingQuote.HasPackingErrors = packingPlan.HasPackingErrors;
                    shippingQuote.Containers = packingPlan.Containers;
                    shippingQuote.UnpackedItems = packingPlan.UnpackedItems;
                    shippingQuote.StepByStepImageURL = packingPlan.StepByStepImageURL;
                    shippingQuote.GroupImageURL = "";

                    shippingQuote.DeclaredValue = shippingQuote.Items.Count != 0 ? (int)shippingQuote.Items.Sum(i => (i.DeclaredValue * i.Quantity)) : 0;
                    shippingQuote.InsuredValue = shippingQuote.Items.Count != 0 ? (int)shippingQuote.Items.Sum(i => (i.InsuredValue * i.Quantity)) : 0;
                    shippingQuote.Quote = new List<CarrierServiceQuote>();

                    if (shippingQuote.Service != null)
                    {
                        shippingQuote.Quote.Add(new CarrierServiceQuote
                        {
                            Carrier = shippingQuote.Service.Carrier,
                            Service = quoteRequest.serviceCode, //Must use the modified serviceCode due to logic changing service code when using fedex_ground and residential is true.
                            CarrierPackageCode = shippingQuote.Service.CarrierPackageCode,
                            SubTotalPrice = xpsObjectResult.SelectToken("baseAmount").ToObject<float>()
                        });
                    }

                    shippingQuote.Surcharges = new List<Surcharge>();

                    // Get the surcharges data from the XPS response
                    var surcharges = xpsObjectResult.SelectToken("surcharges").ToList();

                    if (surcharges.Count != 0)
                    {
                        // Assign the surcharges to shippingQuote.Surcharges
                        shippingQuote.Surcharges.AddRange(surcharges.Select(s => new Surcharge
                        {
                            description = (string)s.SelectToken("description"),
                            price = (string)s.SelectToken("amount")
                        }));
                    }

                    shippingQuote.Price = xpsObjectResult.SelectToken("totalAmount").ToObject<float>();

                    try
                    {

                        shippingQuote.Zone = xpsObjectResult.SelectToken("zone").ToObject<int>();

                    }
                    catch
                    {
                        shippingQuote.Zone = default(int);
                    }

                    
                    shippingQuote.HasShippingErrors = false;
                    shippingQuote.ErrorMessage = "";
                }
            }
            catch (Exception e)
            {
                shippingQuote.HasShippingErrors = true;
                shippingQuote.ErrorMessage = e.Message;

            }

            return shippingQuote;
        }

        public XPSQuoteShipment ConvertPackingPlanToQuoteRequest(PackingPlan packingPlan, Registration registration)
        {
            //Return immediately if either method parameter is null.
            if (packingPlan == null || registration == null) return default(XPSQuoteShipment);
            

            //Just need to change this part to use the packed bins for pieces instead of items.
            var items = new List<Piece>();

            items.AddRange(packingPlan.Containers.Select(i => new Piece
            {
                weight = i.Weight.ToString("0.00"),
                length = i.Depth.ToString("0.00"),
                width = i.Width.ToString("0.00"),
                height = i.Height.ToString("0.00"),
                insuranceAmount = i.InsuredValue.ToString("0.00"),
                declaredValue = i.DeclaredValue.ToString("0.00")
            }));

            PackShipRequest packShipRequest = (PackShipRequest)packingPlan;

            var quoteRequest = new XPSQuoteShipment
            {
                carrierCode = packShipRequest.Service.Carrier,
                serviceCode = packShipRequest.Service.Service,
                packageTypeCode = packShipRequest.Service.CarrierPackageCode,
                sender = new BasePeerInformation
                {
                    country = packShipRequest.ShipmentInfo.ShipFrom.Address.Country,
                    zip = packShipRequest.ShipmentInfo.ShipFrom.Address.PostalCode
                },



                receiver = new QuoteReceiver
                {
                    city = packShipRequest.ShipmentInfo.ShipTo.Address.City,
                    country = packShipRequest.ShipmentInfo.ShipTo.Address.Country,
                    zip = packShipRequest.ShipmentInfo.ShipTo.Address.PostalCode
                },
                residential = string.IsNullOrWhiteSpace(packShipRequest.ShipmentInfo.ShipTo.Company) ? true : false,
                signatureOptionCode = packShipRequest.ShipmentInfo.SignatureOption, 
                contentDescription = packShipRequest.ShipmentInfo.Description, //Value must NOT be null.
                weightUnit = "lb",//Temporarily hardcoded for now. registration.UnitOfWeight,
                dimUnit = "in", //Temporarily hardcoded for now. registration.UnitOfLength,
                currency = registration.Currency,
                pieces = items,
                billing = new BaseBillingInformation
                {
                    party = "sender"
                }
            };

            if(string.IsNullOrWhiteSpace(packShipRequest.ShipmentInfo.ShipTo.Company) && quoteRequest.carrierCode == "fedex" && quoteRequest.serviceCode == "fedex_ground"){
                
                quoteRequest.serviceCode = "fedex_ground_home_delivery";
            }

            return quoteRequest;
        }
    }
}
