﻿using APSA.Web.Libraries.XPS.Models.Requests;
using APSA.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Libraries.XPS.Interface
{
    public interface IXPSClient
    {
        Task<ShippingQuote> QuoteShipment(PackingPlan packingPlan, Registration registration);
        XPSQuoteShipment ConvertPackingPlanToQuoteRequest(PackingPlan packingPlan, Registration registration);
    }
}
