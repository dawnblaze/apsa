﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Libraries.XPS.Models.Requests
{
    public class BaseRequest
    {
        /// <summary>
        /// The carrier code
        /// </summary>
        public string carrierCode { get; set; }
        /// <summary>
        /// The service code
        /// </summary>
        public string serviceCode { get; set; }
        /// <summary>
        /// The package type code
        /// </summary>
        public string packageTypeCode { get; set; }
        /// <summary>
        /// Set to true if the receiver address is residential
        /// </summary>
        public bool residential { get; set; }
        /// <summary>
        /// Signature option code for shipment. Null for no signature
        /// </summary>
        public string signatureOptionCode { get; set; }
        /// <summary>
        /// "lb" for pounds, "oz" for ounces, "kg" for kilograms, or "g" for grams
        /// </summary>

        public string contentDescription { get; set;}
        public string weightUnit { get; set; }
        /// <summary>
        /// Either "in" for inches or "cm" for centimeters or null if packageType has preset dimensions
        /// </summary>
        public string dimUnit { get; set; }
        /// <summary>
        /// The currency of the provided insuranceAmount and declaredValue fields. (USD, GBP, CAD, EUR)
        /// </summary>
        public string currency { get; set; }
        public List<Piece> pieces { get; set; }
    }

    public class BasePeerInformation
    {
        /// <summary>
        /// ISO two-character country code
        /// </summary>
        public string country { get; set; }
        /// <summary>
        /// Zip or postal code
        /// </summary>
        public string zip { get; set; }
    }

    public class QuoteReceiver : BasePeerInformation
    {
        /// <summary>
        /// Destination City/Town
        /// </summary>
        public string city { get; set; }
    }

    public class Piece
    {
        /// <summary>
        /// Numeric weight as a JSON string
        /// </summary>
        public string weight { get; set; }
        /// <summary>
        /// Numeric length as a JSON string (set to null if packageType has preset dimensions)
        /// </summary>
        public string length { get; set; }

        /// <summary>
        /// Numeric width as a JSON string (set to null if packageType has preset dimensions)
        /// </summary>
        public string width { get; set; }
        /// <summary>
        /// Numeric height as a JSON string (set to null if packageType has preset dimensions)
        /// </summary>
        public string height { get; set; }
        /// <summary>
        /// The value of the piece to be covered by insurance. Must be null for no insurance
        /// </summary>
        public string insuranceAmount { get; set; }
        /// <summary>
        /// The declared value of the piece for international shipments. Must be null for domestic shipments
        /// </summary>
        public string declaredValue { get; set; }
    }

    public class BaseBillingInformation
    {
        /// <summary>
        /// Billing party: "sender" for Sender, "receiver" for Receiver, "third_party" for Third Party
        /// </summary>
        public string party { get; set; }
    }
}
