﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Libraries.XPS.Models.Requests
{
    public class XPSVoidShipment
    {
        public bool voided { get; set; }
    }
}
