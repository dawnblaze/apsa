﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Libraries.XPS.Models.Requests
{
    public class XPSBookShipment : BaseRequest
    {
        public string shipmentDate { get; set; }
        public string shipmentReference { get; set; }
        public ShipPeerInformation sender { get; set; }
        public ShipPeerInformation receiver { get; set; }
        public BaseBillingInformation billing { get; set; }
        public string customsCurrency { get; set; }
    }

    public class ShipPeerInformation : BasePeerInformation
    {
        public string name { get; set; }
        public string company { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string phone { get; set; }
    }

    public class ShipBillingInformation : BaseBillingInformation
    {
        public string account { get; set; }
        public string country { get; set; }
        public string zip { get; set; }
    }
}
