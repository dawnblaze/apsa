﻿using System.Collections.Generic;

namespace APSA.Web.Libraries.XPS.Models.Requests
{
    public class XPSQuoteShipment : BaseRequest
    {
        public BasePeerInformation sender { get; set; }
        public QuoteReceiver receiver { get; set; }
        public BaseBillingInformation billing { get; set; }
    }
}
