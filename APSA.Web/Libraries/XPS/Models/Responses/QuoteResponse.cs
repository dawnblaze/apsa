﻿using System.Collections.Generic;

namespace APSA.Web.Libraries.XPS.Models.Responses
{
    public class QuoteResponse
    {
        /// <summary>
        /// 3-character currency code
        /// </summary>
        public string currency { get; set; }
        /// <summary>
        /// Total quote amount (includes base charge and all surcharges)
        /// </summary>
        public string totalAmount { get; set; }
        /// <summary>
        /// Base charge amount
        /// </summary>
        public string baseAmount { get; set; }
        public List<Surcharge> surcharges { get; set; }
        /// <summary>
        /// The zone used to compute shipping cost
        /// </summary>
        public string zone { get; set; }
    }

    public class Surcharge
    {
        /// <summary>
        /// Human readable description of surcharge
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// Surcharge amount
        /// </summary>
        public string price { get; set; }
    }
}
