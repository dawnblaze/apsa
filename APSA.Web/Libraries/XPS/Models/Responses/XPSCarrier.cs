﻿namespace APSA.Web.Libraries.XPS.Models.Responses
{
    public class XPSCarrier
    {
        public string carrier { get; set; }
        public string name { get; set; }
    }

    public class XPSService
    {
        public string carrierCode { get; set; }
        public string serviceCode { get; set; }
        public string name { get; set; }
    }
}
