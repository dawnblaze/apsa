﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using System.Net;
using System.IO;
using System;

namespace APSA.Web
{
    public class Program
    {
        /// <summary>
        /// </summary>
        public static void Main(string[] args)
        {
            Console.Title = "Endor.APSA";
            IConfigurationBuilder hostingBuilder = new ConfigurationBuilder()
                .AddEnvironmentVariables();

            var host = new WebHostBuilder();
            var environment = host.GetSetting("environment");
            hostingBuilder.SetBasePath(Directory.GetCurrentDirectory());
            hostingBuilder.AddJsonFile("appsettings.json");
            hostingBuilder.AddJsonFile($"appsettings.{environment}.json", optional: true);

            if (environment == "Development")
            {
                hostingBuilder.AddUserSecrets<Startup>();
            }

            var envConfiguration = hostingBuilder.Build();

            host.UseKestrel(options =>
            {
                if (environment == "Development")
                {
                    AddHttpsWithCert(options, envConfiguration);
                }
            })
                .UseIISIntegration()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>()
                //.UseApplicationInsights()
                .Build()
                .Run();
        }

        /// <summary>
        /// </summary>
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();


        private static void AddHttpsWithCert(KestrelServerOptions options, IConfigurationRoot envConfiguration)
        {
            string uriString = envConfiguration["ASPNETCORE_URLS"];
            if (string.IsNullOrEmpty(uriString))
            {
                uriString = "https://0.0.0.0:10777";
            }
            Uri hostUri = new Uri(uriString);
            if (Environment.OSVersion.ToString().Contains("Unix"))
            {
                string wildCardCertPath = "/usr/local/share/ca-certificates/wildcard.localcyriousdevelopment.com.v3.GOES_IN_PERSONAL.pfx";
                string wildCardCertPassword = envConfiguration["wildCardCertPassword"];
                options.Listen(IPAddress.Any, hostUri.Port, (a => a.UseHttps(wildCardCertPath, wildCardCertPassword)));
            }
            else
            {
                X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                X509Certificate2Collection certs = store.Certificates.Find(X509FindType.FindByThumbprint, envConfiguration["Endor:certThumbprint"], true);
                if (certs != null && certs.Count > 0)
                {
                    X509Certificate2 cert = certs[0];

                    options.Listen(IPAddress.Any, hostUri.Port, (a => a.UseHttps(cert))); //options.UseHttps(cert);
                }
            }
        }
    }
}
