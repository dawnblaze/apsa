﻿using APSA.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Interfaces
{
    public interface IBusinessKeyRepository
    {
        Task<Response<BusinessKeyResponse>> Add(BusinessKeyRequest input);
        Task<Response<DeleteResponse>> Delete(Guid key);
        Task<short?> GetBusinessID(Guid key);
        
    }
}
