﻿using System.Collections.Generic;
using System.Linq;

namespace APSA.Web.Models
{
    /// <summary>
    /// Types of applications supported by APE
    /// </summary>
    public enum AppType
    {
        /// <summary>
        /// Control
        /// </summary>
        Control = 10,
        /// <summary>
        /// Control Cloud Payment
        /// </summary>
        ControlCloudPayment = 11,
        /// <summary>
        /// CBMS v2
        /// </summary>
        CBMSv2 = 20,
        /// <summary>
        /// CBMS v2 Customer Portal
        /// </summary>
        CBMSv2CustomerPortal = 21,
        /// <summary>
        /// Endor
        /// </summary>
        Endor = 30,
        /// <summary>
        /// Endor Customer Portal
        /// </summary>
        EndorCustomerPortal = 31,
        /// <summary>
        /// Ecommerce
        /// </summary>
        Ecommerce = 32
    }
    /// <summary>
    /// Helper class for handling the <see cref="AppType"/> enum.
    /// </summary>
    public static class AppTypeHelper
    {
        internal static Dictionary<AppType, string> names =
            new Dictionary<AppType, string>
            {
                { AppType.Control, "Control" },
                { AppType.ControlCloudPayment, "Control Cloud Payment" },
                { AppType.CBMSv2, "CBMS v2" },
                { AppType.CBMSv2CustomerPortal, "CBMS v2 Customer Portal" },
                { AppType.Endor, "Endor" },
                { AppType.EndorCustomerPortal, "Endor Customer Portal" },
                { AppType.Ecommerce, "Ecommerce" },
            };
        /// <summary>
        /// Returns the <see cref="AppType"/> from its proper name.
        /// </summary>
        /// <param name="name">Proper name of the <see cref="AppType"/></param>
        /// <returns>AppType</returns>
        public static AppType FromName(string name)
        {
            KeyValuePair<AppType, string> myPair = names.FirstOrDefault(x => x.Value == name);

            return myPair.Key;
        }
        /// <summary>
        /// Returns the <see cref="AppType"/> from its proper name.
        /// </summary>
        /// <param name="name">Proper name of the <see cref="AppType"/></param>
        /// <param name="value">Out parameter to retrun the the AppType</param>
        /// <returns>True when the name is valid, otherwise False</returns>
        public static bool TryFromName(string name, out AppType value)
        {
            value = AppType.CBMSv2;
            if (!names.ContainsValue(name))
                return false;

            KeyValuePair<AppType, string> myPair = names.FirstOrDefault(x => x.Value == name);

            value = myPair.Key;
            return true;
        }
        /// <summary>
        /// Returns the <see cref="AppType"/> from its ID.
        /// </summary>
        /// <param name="id">ID of the <see cref="AppType"/></param>
        /// <returns>AppType</returns>
        public static AppType FromID(int id)
        {
            return (AppType)id;
        }
    }
    /// <summary>
    /// Extension Methods for the AppType enum
    /// </summary>
    public static class AppTypeMethods
    {
        /// <summary>
        /// Returns the proper name of an <see cref="AppType"/>.
        /// </summary>
        /// <param name="appType"></param>
        /// <returns>string</returns>
        public static string Name(this AppType appType)
        {
            return AppTypeHelper.names.GetValueOrDefault(appType, appType.ToString());
        }
    }
}
