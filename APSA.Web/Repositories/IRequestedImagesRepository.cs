﻿using APSA.Web.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Repositories
{
    public interface IRequestedImagesRepository
    {
        Task<DeleteExpiredRequestedImagesResponse> DeleteExpiredImagesAsync();
        int GetNumDaysSettingBeforeDeletion();
        string GetImagesRootContainer();
    }
}
