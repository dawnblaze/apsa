﻿using APSA.Web.Models.Responses;
using APSA.Web.Settings;
using Microsoft.Extensions.Options;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APSA.Web.Repositories
{
    public class RequestedImagesRepository : IRequestedImagesRepository
    {

        private DeleteExpiredRequestedImagesSettings _settings;
        private const string IMAGES_TEMP_PATH_PREFIX = "apsa/images/temp/";

        public RequestedImagesRepository(IOptions<DeleteExpiredRequestedImagesSettings> settings)
        {
            _settings = settings.Value;
        }

        private async Task<bool> DeleteCloudBlockBlobItem(CloudBlockBlob item)
        { 
            bool deleteSuccessful = false;
            //The num days is fetched from json settings and turned into a negative integer.
            if (!Equals(item, null) && DateTime.UtcNow.AddDays(-Math.Abs(this.GetNumDaysSettingBeforeDeletion())) > item.Properties.LastModified.Value.DateTime)
            {
    
                //DeleteSnapshotsOption.IncludeSnapshots required.
                deleteSuccessful = await item.DeleteIfExistsAsync(DeleteSnapshotsOption.IncludeSnapshots, null, null, null);
                
            }

            return deleteSuccessful;

        }

        public async Task<DeleteExpiredRequestedImagesResponse> DeleteExpiredImagesAsync()
        {

            string connectionString = this._settings.AzureConnectionString;
            string imagesRootContainer = this.GetImagesRootContainer();
            CloudStorageAccount storageAccount;
            CloudBlobClient blobClient;
            CloudBlobContainer blobContainer;
            BlobContinuationToken blobContinuationToken = null;
            BlobResultSegment blobResultSegment = null;
            int numDays = this.GetNumDaysSettingBeforeDeletion();

            DeleteExpiredRequestedImagesResponse response = new DeleteExpiredRequestedImagesResponse();
            response.NumDaysOldSetting = this._settings.NumDays;


            if (CloudStorageAccount.TryParse(connectionString, out storageAccount))
            {

                blobClient = storageAccount.CreateCloudBlobClient();
                
                //Start from the prefix set from the settings.
                blobContainer = blobClient.GetContainerReference(this.GetImagesRootContainer());

                bool blobContainerExists = await blobContainer.ExistsAsync();

                if(blobContainerExists)
                {
                    
                    do
                    {

                        blobResultSegment = await blobContainer.ListBlobsSegmentedAsync(prefix: IMAGES_TEMP_PATH_PREFIX, currentToken:blobContinuationToken, useFlatBlobListing:true, operationContext:null, options:null, maxResults:null, blobListingDetails:BlobListingDetails.All);

                        blobContinuationToken = blobResultSegment.ContinuationToken;
 
                        if (blobResultSegment.Results.Count<IListBlobItem>() > 0)
                        {

                            
                            foreach(var i in blobResultSegment.Results)
                            {

                                response.BlobsFound.Add(i.Uri.ToString());

                                bool deleteSuccessful = await this.DeleteCloudBlockBlobItem((CloudBlockBlob)i);

                                if (deleteSuccessful)
                                {
                                    response.DeletedBlobs.Add(i.Uri.ToString());

                                }
                                
                            }
                            
                            

                        }

                    } while (!Equals(blobContinuationToken, null));


                }
            }
            else
            {
                response.ErrorMessage += "Error on: CloudStorageAccount.TryParse(connectionString, out storageAccount)";

            }

            return response;
            
        }

        

        public string GetImagesRootContainer()
        {
            return this._settings.ImagesRootContainer;
        }

        public int GetNumDaysSettingBeforeDeletion()
        {
            
            if(!Equals(this._settings, null))
            {
               
                return this._settings.NumDays;
                
            }

            //Make sure nothing is deleted if _settings is null.
            return 365;

        }
    }
}
