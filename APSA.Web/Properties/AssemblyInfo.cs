﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


[assembly: BuildVersion("1.0.1905.2201")]

[AttributeUsage(AttributeTargets.Assembly)]

public sealed class BuildVersionAttribute : Attribute
{
    public string VersionString { get; set; }

    public BuildVersionAttribute(string versionString)
    {
        this.VersionString = versionString;
    }
}

namespace APSA.Web.Properties
{
    public class AssemblyInfo
    {
    }
}
